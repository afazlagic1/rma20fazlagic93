package ba.unsa.etf.rma.rma20fazlagic93.chart;

public interface ITransactionGraphInteractor {
    double getTotalMonthPayment(int month, int year);
    double getTotalMonthIncome(int month, int year);
    double getTotalAmount(int month, int year);
    double getTotalDayAmount(int date, int month, int year);
    double getTotalDayPayment(int date, int month, int year);
    double getTotalDayIncome(int date, int month, int year);
    double getTotalWeekAmount(int date, int month, int year);
    double getTotalWeekPayment(int date, int month, int year);
    double getTotalWeekIncome(int date, int month, int year);
}
