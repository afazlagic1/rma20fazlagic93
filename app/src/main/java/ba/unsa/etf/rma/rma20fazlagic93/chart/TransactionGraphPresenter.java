package ba.unsa.etf.rma.rma20fazlagic93.chart;

import android.util.Log;

import java.util.ArrayList;
import java.util.Calendar;

import ba.unsa.etf.rma.rma20fazlagic93.data.Transaction;
import ba.unsa.etf.rma.rma20fazlagic93.data.TransactionsModel;

public class TransactionGraphPresenter implements ITransactionGraphPresenter, ChartDataInteractor.OnDoneTotalWeek, ChartDataInteractor.OnDoneTotalDay, ChartDataInteractor.OnDoneTotalMonth {
    private ISetGraphView view;
    private Calendar calendar = Calendar.getInstance();

    private IChartDataInteractor chartDataInteractor = new ChartDataInteractor((ChartDataInteractor.OnDoneTotalMonth)this);

    public TransactionGraphPresenter(ISetGraphView view) {
        this.view = view;
    }

    public IChartDataInteractor getChartDataInteractor() {
        return chartDataInteractor;
    }

    @Override
    public void onDoneTotalMonth(ArrayList<Transaction> payment, ArrayList<Transaction> income, ArrayList<Transaction> amount) {
        ArrayList<Double> p = new ArrayList<>();
        ArrayList<Double> i = new ArrayList<>();
        ArrayList<Double> a = new ArrayList<>();
        for(int j = 0; j < 12; j++) {
            Double x = TransactionsModel.totalMonthPayment(payment, j, Calendar.getInstance().get(Calendar.YEAR));
            Double y = TransactionsModel.totalMonthIncome(income, j, Calendar.getInstance().get(Calendar.YEAR));
            p.add(x);
            i.add(y);
            a.add(p.get(j) + i.get(j));
        }
        view.setMonth(p, i, a);
    }

    @Override
    public void onDoneTotaDay(ArrayList<Transaction> transakcije) {
        ArrayList<Double> p = new ArrayList<>();
        ArrayList<Double> i = new ArrayList<>();
        ArrayList<Double> a = new ArrayList<>();
        for(int j = 1; j <= calendar.getActualMaximum(Calendar.DATE); j++) {
            Double x = TransactionsModel.totalDayPayment(transakcije, j, calendar.get(Calendar.MONTH), calendar.get(Calendar.YEAR));
            Double y = TransactionsModel.totalDayIncome(transakcije, j, calendar.get(Calendar.MONTH), calendar.get(Calendar.YEAR));
            p.add(x);
            i.add(y);
            a.add(p.get(j-1) + i.get(j-1));
        }
        view.setDay(p,i,a);
    }

    private int weeksInMonth() {
        Calendar calendar1 = Calendar.getInstance();
        calendar1.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), 1);
        calendar1.add(Calendar.MONTH, 1);
        calendar1.add(Calendar.DATE, -1);
        return calendar1.get(Calendar.WEEK_OF_MONTH);
    }

    @Override
    public void onDoneTotalWeek(ArrayList<Transaction> transakcije) {
        ArrayList<Double> p = new ArrayList<>();
        ArrayList<Double> i = new ArrayList<>();
        ArrayList<Double> a = new ArrayList<>();
        for(int j = 1; j <= weeksInMonth(); j++) {
            Double x = TransactionsModel.totalWeekPayment(transakcije, j, calendar.get(Calendar.MONTH), calendar.get(Calendar.YEAR));
            Double y = TransactionsModel.totalWeekIncome(transakcije, j, calendar.get(Calendar.MONTH), calendar.get(Calendar.YEAR));
            p.add(x);
            i.add(y);
            a.add(p.get(j-1) + i.get(j-1));
        }
        view.setWeek(p,i,a);
    }

    @Override
    public void putChartMonth() {
        chartDataInteractor = new ChartDataInteractor((ChartDataInteractor.OnDoneTotalMonth) this);
        getChartDataInteractor().set("month");
    }

    @Override
    public void putChartDay() {
        chartDataInteractor = new ChartDataInteractor((ChartDataInteractor.OnDoneTotalDay) this);
        getChartDataInteractor().set("day");
    }

    @Override
    public void putChartWeek() {
        chartDataInteractor = new ChartDataInteractor((ChartDataInteractor.OnDoneTotalWeek) this);
        getChartDataInteractor().set("week");
    }
}
