package ba.unsa.etf.rma.rma20fazlagic93.account;

import android.content.Context;

import ba.unsa.etf.rma.rma20fazlagic93.data.Account;

public interface IAccountPresenter {
    double getMonthLimit();
    double getTotalLimit();
    double getBudget();
    void setMonthLimit(double x, Context context);
    void setTotalLimit(double x, Context context);
    void setBudget(double x, Context context);
    void setMonthLimitAndSetTotalLimit(double monthLimit, double TotalLimit, Context context);
    Account getAccountFromDB(Context applicationContext);
}
