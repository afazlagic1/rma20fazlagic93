package ba.unsa.etf.rma.rma20fazlagic93.list;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import ba.unsa.etf.rma.rma20fazlagic93.R;
import ba.unsa.etf.rma.rma20fazlagic93.data.Transaction;
import ba.unsa.etf.rma.rma20fazlagic93.data.Type;

public class TransactionListAdapter extends ArrayAdapter<Transaction> {
    private int resource;
    private Context context;
    public TransactionListAdapter(@NonNull Context context, int resource, @NonNull List<Transaction> objects) {
        super(context, resource, objects);
        this.resource = resource;
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LinearLayout newView;
        if(convertView == null) {
            newView = new LinearLayout(getContext());
            String inflater = Context.LAYOUT_INFLATER_SERVICE;
            LayoutInflater li;
            li = (LayoutInflater) getContext().getSystemService(inflater);
            li.inflate(resource, newView, true);
        }
        else {
            newView = (LinearLayout) convertView;
        }
        Transaction objekat = getItem(position);

        TextView title = newView.findViewById(R.id.title);
        TextView iznos = newView.findViewById(R.id.iznos);
        ImageView ikonica = newView.findViewById(R.id.ikonica);

        title.setText(objekat.getTitle());
        iznos.setText(String.valueOf(objekat.getAmount()));
        if(objekat != null)
            if(objekat.getType().equals(Type.INDIVIDUALINCOME))
                ikonica.setImageResource(R.drawable.individual_income);
            else if(objekat.getType().equals(Type.INDIVIDUALPAYMENT))
                ikonica.setImageResource(R.drawable.individual_payment);
            else if(objekat.getType().equals(Type.REGULARINCOME))
                ikonica.setImageResource(R.drawable.regular_income);
            else if(objekat.getType().equals(Type.REGULARPAYMENT))
                ikonica.setImageResource(R.drawable.regular_payment);
            else if(objekat.getType().equals(Type.PURCHASE))
                ikonica.setImageResource(R.drawable.purchase_);

        return newView;
    }

    public Transaction getTransaction(int position) {
        return getItem(position);
    }

    public void setTransactions(ArrayList<Transaction> transactions) {
        this.addAll(transactions);
    }

}
