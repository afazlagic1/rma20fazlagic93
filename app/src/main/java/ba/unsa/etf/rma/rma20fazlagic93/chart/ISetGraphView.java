package ba.unsa.etf.rma.rma20fazlagic93.chart;

import java.util.ArrayList;

public interface ISetGraphView {
    void setMonth(ArrayList<Double> payment, ArrayList<Double> income, ArrayList<Double> amount);
    void setDay(ArrayList<Double> payment, ArrayList<Double> income, ArrayList<Double> amount);
    void setWeek(ArrayList<Double> payment, ArrayList<Double> income, ArrayList<Double> amount);
}
