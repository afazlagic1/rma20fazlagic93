package ba.unsa.etf.rma.rma20fazlagic93.list;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

import ba.unsa.etf.rma.rma20fazlagic93.data.Account;
import ba.unsa.etf.rma.rma20fazlagic93.util.ConnectivityBroadcastReceiver;
import ba.unsa.etf.rma.rma20fazlagic93.account.AccountPresenter;
import ba.unsa.etf.rma.rma20fazlagic93.account.BudgetFragment;
import ba.unsa.etf.rma.rma20fazlagic93.account.IAccountPresenter;
import ba.unsa.etf.rma.rma20fazlagic93.account.IAccountView;
import ba.unsa.etf.rma.rma20fazlagic93.chart.GraphsFragment;
import ba.unsa.etf.rma.rma20fazlagic93.util.OnSwipeTouchListener;
import ba.unsa.etf.rma.rma20fazlagic93.R;
import ba.unsa.etf.rma.rma20fazlagic93.data.Transaction;
import ba.unsa.etf.rma.rma20fazlagic93.data.TransactionsModel;
import ba.unsa.etf.rma.rma20fazlagic93.data.Type;
import ba.unsa.etf.rma.rma20fazlagic93.detail.TransactionDetailFragment;

public class MainActivity extends AppCompatActivity implements Serializable, TransactionListFragment.OnClick, TransactionDetailFragment.OnDelete, TransactionListFragment.OnAddClick, TransactionDetailFragment.OnSave, IAccountView, IUpdateTransactionList {
    private int swipeCount = 0;
    private Transaction pickedTransaction = null;
    private IAccountPresenter accountPresenter = new AccountPresenter(this,this);
    private ITransactionListPresenter transactionListPresenter = new TransactionListPresenter(this, this);
    private ConnectivityBroadcastReceiver receiver = new ConnectivityBroadcastReceiver(this);
    private IntentFilter filter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");

    @Override
    protected void onDestroy() {
        if(isFinishing()) {
            if(TransactionsModel.lastLoadedTransactions.size() != 0)
                TransactionsModel.lastLoadedTransactionsHolder = TransactionsModel.lastLoadedTransactions;
            TransactionsModel.lastLoadedTransactions = new ArrayList<>();
            if(ConnectivityBroadcastReceiver.connection == false) {
                ArrayList<Transaction> uBazi = null;
                try {
                    uBazi = transactionListPresenter.getTransactionsFromDB(this);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                boolean inDatabase = false;
                for (int i = 0; i < TransactionsModel.transactionsDeleted.size(); i++) {
                    inDatabase = false;
                    Log.d("deletedID", String.valueOf(TransactionsModel.transactionsDeleted.get(i).getId()));
                    if(TransactionsModel.transactionsDeleted.get(i).getId() == -1)
                        continue;
                    for (int k = 0; k < uBazi.size(); k++) {
                        Log.d("uBaziID", String.valueOf(uBazi.get(k).getId()));
                        if(uBazi.get(k).getId().equals(TransactionsModel.transactionsDeleted.get(i).getId())) {
                            inDatabase = true;
                            break;
                        }
                    }
                    if(!inDatabase)
                        transactionListPresenter.addTransaction(TransactionsModel.transactionsDeleted.get(i), this);
                }
            }
        }
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        registerReceiver(receiver, filter);
    }

    @Override
    public void onPause() {
        unregisterReceiver(receiver);
        super.onPause();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toast.makeText(this, "loading... (please wait)", Toast.LENGTH_LONG).show();
        FragmentManager fragmentManager = getSupportFragmentManager();
        Configuration configuration = getResources().getConfiguration();
        if(configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            Bundle bundle = new Bundle();
            TransactionDetailFragment transactionDetailFragment = null;
            if(transactionDetailFragment == null) {
                transactionDetailFragment = new TransactionDetailFragment();
                if(pickedTransaction == null)
                    bundle.putSerializable("transaction", new Transaction(0, new Date(), 0.0, "Title", Type.INDIVIDUALINCOME, null, 0, null, 0));
                else
                    bundle.putSerializable("transaction", pickedTransaction);
                bundle.putDouble("totalLimit", getAccountPresenter().getTotalLimit());
                bundle.putDouble("monthLimit", getAccountPresenter().getMonthLimit());
                bundle.putDouble("budget", getAccountPresenter().getBudget());
                transactionDetailFragment.setArguments(bundle);
                fragmentManager.beginTransaction().replace(R.id.transactions_detail, transactionDetailFragment, "detailTag").commit();
            }
        }
        Fragment transactionListFragment = null;
        if(transactionListFragment == null) {
            transactionListFragment = new TransactionListFragment();
            fragmentManager.beginTransaction().replace(R.id.transactions_list, transactionListFragment).commit();
        }
        else {
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }

        LinearLayout ll = findViewById(R.id.linLayout);
        if(configuration.orientation == Configuration.ORIENTATION_PORTRAIT)
            ll.setOnTouchListener(new OnSwipeTouchListener(this) {
                @Override
                public void onSwipeLeft() {
                    swipeCount = swipeCount + 1;
                    if(swipeCount == 1) {
                        BudgetFragment budgetFragment = new BudgetFragment();
                        getSupportFragmentManager().beginTransaction().replace(R.id.transactions_list, budgetFragment, "b").addToBackStack(null).commit();
                    }
                    else if(swipeCount == 2) {
                        GraphsFragment graphsFragment = new GraphsFragment();
                        getSupportFragmentManager().beginTransaction().replace(R.id.transactions_list, graphsFragment).addToBackStack(null).commit();
                    }
                    else if(swipeCount == 3) {
                        swipeCount = 0;
                        getSupportFragmentManager().popBackStack();
                        getSupportFragmentManager().popBackStack();
                    }
                }

                @Override
                public void onSwipeRight() {
                    if(swipeCount == 0) {
                        swipeCount = 2;
                        BudgetFragment budgetFragment = new BudgetFragment();
                        getSupportFragmentManager().beginTransaction().replace(R.id.transactions_list, budgetFragment).addToBackStack(null).commit();
                        GraphsFragment graphsFragment = new GraphsFragment();
                        getSupportFragmentManager().beginTransaction().replace(R.id.transactions_list, graphsFragment).addToBackStack(null).commit();
                    }
                    else if(swipeCount == 1) {
                        swipeCount = swipeCount - 1;
                        getSupportFragmentManager().popBackStack();
                        getSupportFragmentManager().popBackStack();
                    }
                    else if(swipeCount == 2) {
                        swipeCount = swipeCount - 1;
                        getSupportFragmentManager().popBackStack();
                    }
                }
            });
    }

    @Override
    public void onItemClick(Transaction transaction, double totalMonthAmount) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("transaction", transaction);
        bundle.putDouble("totalLimit", getAccountPresenter().getTotalLimit());
        bundle.putDouble("monthLimit", getAccountPresenter().getMonthLimit());
        bundle.putDouble("budget", getAccountPresenter().getBudget());
        bundle.putDouble("totalMonthAmount", totalMonthAmount);
        Configuration configuration = getResources().getConfiguration();
        TransactionDetailFragment transactionDetailFragment = new TransactionDetailFragment();
        transactionDetailFragment.setArguments(bundle);

        if(configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
            pickedTransaction = transaction;
            if(getSupportFragmentManager().getBackStackEntryCount() > 0)
                getSupportFragmentManager().popBackStack();
            getSupportFragmentManager().beginTransaction().replace(R.id.transactions_list, transactionDetailFragment, "portrait_detail").addToBackStack(null).commit();
        }
        else {
            getSupportFragmentManager().beginTransaction().replace(R.id.transactions_detail, transactionDetailFragment).commit();
            }
        }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        pickedTransaction = null;
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("pickedTransaction", pickedTransaction);
    }

    @Override
    public void onDelete(Transaction transaction, double amount) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("deleteThisTransaction", transaction);
        bundle.putDouble("amountToDelete", amount);
        Configuration configuration = getResources().getConfiguration();
        TransactionListFragment transactionListFragment = new TransactionListFragment();
        transactionListFragment.setArguments(bundle);

        if(configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
            if(getSupportFragmentManager().getBackStackEntryCount() > 0)
                getSupportFragmentManager().popBackStack();
            getSupportFragmentManager().beginTransaction().replace(R.id.transactions_list, transactionListFragment, "delete_portrait").addToBackStack(null).commit();
        }
        else {
            getSupportFragmentManager().beginTransaction().replace(R.id.transactions_list, transactionListFragment).commit();
        }
    }

    @Override
    public void onDeleteOrUndoDelete(Transaction transaction, double amount, boolean undo) {
        if(undo) {
            transactionListPresenter.undoDeleteTransactionDB(transaction, this);
        }
        else {
            transactionListPresenter.removeTransaction(transaction, this);
        }
    }

    @Override
    public void onAddClickMethod(Transaction transaction, double totalMonthAmount) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("transaction", transaction);
        bundle.putDouble("totalLimit", getAccountPresenter().getTotalLimit());
        bundle.putDouble("monthLimit", getAccountPresenter().getMonthLimit());
        bundle.putDouble("budget", getAccountPresenter().getBudget());
        bundle.putDouble("totalMonthAmount", totalMonthAmount);
        TransactionDetailFragment transactionDetailFragment = new TransactionDetailFragment();
        transactionDetailFragment.setArguments(bundle);
        Configuration configuration = getResources().getConfiguration();
        if(configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
            if(getSupportFragmentManager().getBackStackEntryCount() > 0)
                getSupportFragmentManager().popBackStack();
           getSupportFragmentManager().beginTransaction().replace(R.id.transactions_list, transactionDetailFragment).addToBackStack(null).commit();
        }
        else {
            getSupportFragmentManager().beginTransaction().replace(R.id.transactions_detail, transactionDetailFragment).commit();
        }
    }

    @Override
    public void onSave(Transaction transaction, boolean newTransaction, double formerAmount, Type formerType, Date formerDate, Date formerEndDate, int formerInterval) {
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            //ne treba refresh
            if(newTransaction == false)
                updateTransaction(transaction, formerAmount, formerType, formerDate, formerEndDate, formerInterval);
        }
        else {
            if(newTransaction == false)
                updateTransaction(transaction, formerAmount, formerType, formerDate, formerEndDate, formerInterval);
            Bundle bundle = new Bundle();
            bundle.putSerializable("novaT", transaction);
            bundle.putBoolean("nova", newTransaction);
            TransactionListFragment transactionListFragment = new TransactionListFragment();
            transactionListFragment.setArguments(bundle);
            getSupportFragmentManager().beginTransaction().replace(R.id.transactions_list, transactionListFragment).commit();
        }
    }

    private IAccountPresenter getAccountPresenter() {
        return accountPresenter;
    }

    @Override
    public void setBudgetView(Double budget) {
        //
    }

    @Override
    public void setTotalLimitView(Double totalLimit) {
        //
    }

    @Override
    public void setMonthLimitView(Double monthLimit) {
        //
    }

    @Override
    public void updateTransaction(Transaction transaction, double formerAmount, Type formerType, Date formerDate, Date formerEndDate, int formerInterval) {
        transactionListPresenter.updateTransaction(transaction, formerAmount, formerType, formerDate, formerEndDate, formerInterval, this);
    }
}