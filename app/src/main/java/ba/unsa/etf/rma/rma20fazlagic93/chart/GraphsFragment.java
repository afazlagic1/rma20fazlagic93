package ba.unsa.etf.rma.rma20fazlagic93.chart;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import ba.unsa.etf.rma.rma20fazlagic93.R;

public class GraphsFragment extends Fragment implements ISetGraphView{

    private BarChart potrosnja;
    private BarChart iznos;
    private BarChart stanje;
    private Spinner spinner;
    private ITransactionGraphPresenter transactionGraphPresenter = new TransactionGraphPresenter(this);
    private Calendar calendar = Calendar.getInstance();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_graphs, container, false);

        if(getActivity() != null) {
        spinner = view.findViewById(R.id.DWMspinner);
        ArrayAdapter<CharSequence> arrayAdapter = ArrayAdapter.createFromResource(getActivity(), R.array.graphs, android.R.layout.simple_spinner_item);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(arrayAdapter);
        }

        potrosnja = view.findViewById(R.id.potrosnja);
        iznos = view.findViewById(R.id.iznos);
        stanje = view.findViewById(R.id.stanje);

        /*
        paymentChartMonth();
        incomeChartMonth();
        stanjeChartMonth();*/

        getTransactionGraphPresenter().putChartMonth();

        spinner.setOnItemSelectedListener(criteriaChangedListener());
        return view;
    }

    private AdapterView.OnItemSelectedListener criteriaChangedListener() {
        return new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position == 0) {
                   /* paymentChartMonth();
                    incomeChartMonth();
                    stanjeChartMonth();*/
                   getTransactionGraphPresenter().putChartMonth();
                }
                else if(position == 1) {
                   /* paymentChartDay();
                    incomeChartDay();
                    stanjeChartDay();*/
                    getTransactionGraphPresenter().putChartDay();
                }
                else if(position == 2) {
                   /* paymentChartWeek();
                    incomeChartWeek();
                    stanjeChartWeek();*/
                    getTransactionGraphPresenter().putChartWeek();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        };
    }

    private ITransactionGraphPresenter getTransactionGraphPresenter() {
        return transactionGraphPresenter;
    }

    @Override
    public void setMonth(ArrayList<Double> payment, ArrayList<Double> income, ArrayList<Double> amount) {
        List<BarEntry> paymentMonthlyArrayList = new ArrayList<>();
        for(int i = 0; i < 12; i++)
            paymentMonthlyArrayList.add(new BarEntry(i + 1, (int)Double.parseDouble(String.valueOf(payment.get(i))))); //double amount
        BarDataSet potrosnjaDataSet = new BarDataSet(paymentMonthlyArrayList, "Potrošnja");
        potrosnjaDataSet.setColor(Color.RED);
        List<IBarDataSet> potrosnjaDataSets = new ArrayList<>();
        potrosnjaDataSets.add(potrosnjaDataSet);
        BarData data = new BarData(potrosnjaDataSets);
        potrosnja.setData(data);
        potrosnja.invalidate();
        potrosnja.getDescription().setText("kriterij: mjesec");

        List<BarEntry> incomeMonthlyArrayList = new ArrayList<>();
        for(int i = 0; i < 12; i++)
            incomeMonthlyArrayList.add(new BarEntry(i + 1, (int)Double.parseDouble(String.valueOf(income.get(i)))));
        BarDataSet iznosDataSet = new BarDataSet(incomeMonthlyArrayList, "Zarada");
        iznosDataSet.setColor(Color.BLUE);
        List<IBarDataSet> iznosDataSets = new ArrayList<>();
        iznosDataSets.add(iznosDataSet);
        BarData data1 = new BarData(iznosDataSets);
        iznos.setData(data1);
        iznos.invalidate();
        iznos.getDescription().setText("kriterij: mjesec");

        List<BarEntry> stanjeMonthlyArrayList = new ArrayList<>();
        for(int i = 0; i < 12; i++)
            stanjeMonthlyArrayList.add(new BarEntry(i + 1, (int)Double.parseDouble(String.valueOf(amount.get(i)))));
        BarDataSet stanjeDataSet = new BarDataSet(stanjeMonthlyArrayList, "Stanje");
        List<IBarDataSet> stanjeDataSets = new ArrayList<>();
        stanjeDataSets.add(stanjeDataSet);
        BarData data2 = new BarData(stanjeDataSets);
        stanje.setData(data2);
        stanje.invalidate();
        stanje.getDescription().setText("kriterij: mjesec");
    }

    @Override
    public void setDay(ArrayList<Double> payment, ArrayList<Double> income, ArrayList<Double> amount) {
        {
            List<BarEntry> paymentArrayList = new ArrayList<>();
            for (int i = 0; i < payment.size(); i++)
                paymentArrayList.add(new BarEntry(i + 1, (int) Double.parseDouble(String.valueOf(payment.get(i)))));
            BarDataSet dataSet = new BarDataSet(paymentArrayList, "Potrošnja");
            dataSet.setColor(Color.RED);
            List<IBarDataSet> dataSets = new ArrayList<>();
            dataSets.add(dataSet);
            BarData data = new BarData(dataSets);
            potrosnja.setData(data);
            potrosnja.invalidate();
            potrosnja.getDescription().setText("kriterij: dan");
        }
        {
            List<BarEntry> barEntryList = new ArrayList<>();
            for (int i = 0; i < income.size(); i++)
                barEntryList.add(new BarEntry(i + 1, (int) Double.parseDouble(String.valueOf(income.get(i)))));
            BarDataSet barDataSet = new BarDataSet(barEntryList, "Zarada");
            barDataSet.setColor(Color.BLUE);
            List<IBarDataSet> dataSets = new ArrayList<>();
            dataSets.add(barDataSet);
            BarData data = new BarData(dataSets);
            iznos.setData(data);
            iznos.invalidate();
            iznos.getDescription().setText("kriterij: dan");
        }
        {
            List<BarEntry> barEntryList = new ArrayList<>();
            for (int i = 0; i < amount.size(); i++)
                barEntryList.add(new BarEntry(i + 1, (int) Double.parseDouble(String.valueOf(amount.get(i)))));
            BarDataSet barDataSet = new BarDataSet(barEntryList, "Stanje");
            List<IBarDataSet> dataSets = new ArrayList<>();
            dataSets.add(barDataSet);
            BarData data = new BarData(dataSets);
            stanje.setData(data);
            stanje.invalidate();
            stanje.getDescription().setText("kriterij: dan");
        }
    }

    @Override
    public void setWeek(ArrayList<Double> payment, ArrayList<Double> income, ArrayList<Double> amount) {
        {
            List<BarEntry> paymentArrayList = new ArrayList<>();
            for(int i = 0; i < payment.size(); i++)
                paymentArrayList.add(new BarEntry(i+1, (int) Double.parseDouble(String.valueOf(payment.get(i)))));
            BarDataSet dataSet = new BarDataSet(paymentArrayList, "Potrošnja");
            dataSet.setColor(Color.RED);
            List<IBarDataSet> dataSets = new ArrayList<>();
            dataSets.add(dataSet);
            BarData data = new BarData(dataSets);
            potrosnja.setData(data);
            potrosnja.invalidate();
            potrosnja.getDescription().setText("kriterij: sedmica");
        }
        {
            List<BarEntry> barEntryList = new ArrayList<>();
            for(int i = 0; i < income.size(); i++)
                barEntryList.add(new BarEntry(i+1, (int) Double.parseDouble(String.valueOf(income.get(i)))));
            BarDataSet barDataSet = new BarDataSet(barEntryList, "Zarada");
            barDataSet.setColor(Color.BLUE);
            List<IBarDataSet> dataSets = new ArrayList<>();
            dataSets.add(barDataSet);
            BarData data = new BarData(dataSets);
            iznos.setData(data);
            iznos.invalidate();
            iznos.getDescription().setText("kriterij: sedmica");
        }
        {
            List<BarEntry> barEntryList = new ArrayList<>();
            for(int i = 0; i < amount.size(); i++)
                barEntryList.add(new BarEntry(i+1, (int) Double.parseDouble(String.valueOf(amount.get(i)))));
            BarDataSet barDataSet = new BarDataSet(barEntryList, "Stanje");
            List<IBarDataSet> dataSets = new ArrayList<>();
            dataSets.add(barDataSet);
            BarData data = new BarData(dataSets);
            stanje.setData(data);
            stanje.invalidate();
            stanje.getDescription().setText("kriterij: sedmica");
        }
    }
}
