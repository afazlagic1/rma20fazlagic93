package ba.unsa.etf.rma.rma20fazlagic93.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.util.Log;
import android.widget.Toast;

import java.text.ParseException;
import java.util.ArrayList;

import ba.unsa.etf.rma.rma20fazlagic93.account.AccountPresenter;
import ba.unsa.etf.rma.rma20fazlagic93.data.Account;
import ba.unsa.etf.rma.rma20fazlagic93.data.Transaction;
import ba.unsa.etf.rma.rma20fazlagic93.data.TransactionsModel;
import ba.unsa.etf.rma.rma20fazlagic93.list.MainActivity;
import ba.unsa.etf.rma.rma20fazlagic93.list.TransactionListPresenter;

public class ConnectivityBroadcastReceiver extends BroadcastReceiver {
    public static boolean connection = false;
    MainActivity view;

    public ConnectivityBroadcastReceiver(MainActivity view) {
        this.view = view;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm.getActiveNetworkInfo() == null) {
            Toast toast = Toast.makeText(context, "Disconnected", Toast.LENGTH_SHORT);
            toast.show();
            connection = false;
        }
        else {
            Toast toast = Toast.makeText(context, "Connected", Toast.LENGTH_SHORT);
            toast.show();
            //update web servisa sa podacima iz baze i brisanje podataka iz baze
            connection = true;
            TransactionListPresenter transactionListPresenter = new TransactionListPresenter();
            try {
                ArrayList<Transaction> transactions = transactionListPresenter.getTransactionsFromDB(context.getApplicationContext());
                ArrayList<Transaction> transactionsToDeleteDB = transactionListPresenter.getDeletedTransactionsFromDB(context.getApplicationContext());
                Log.d("DBsizee", String.valueOf(transactions.size()));
                for(int i = 0; i < transactions.size(); i++) {
                    if (transactions.get(i).getId() == -1) {
                        Log.d("idNew", String.valueOf(transactions.get(i).getId()));
                        Log.d("internalIdNew", String.valueOf(transactions.get(i).getInternalId()));
                        boolean deleted = false;
                        for(int j = 0; j < TransactionsModel.transactionsDeleted.size(); j++)
                            if(TransactionsModel.transactionsDeleted.get(j).getInternalId() == transactions.get(i).getInternalId()) {
                                deleted = true;
                                break;
                            }
                        if(!deleted)
                            transactionListPresenter.addTransaction(transactions.get(i), context.getApplicationContext());
                    }
                    else {
                        for (int j = 0; j < TransactionsModel.lastLoadedTransactionsHolder.size(); j++)
                            if (TransactionsModel.lastLoadedTransactionsHolder.get(j).getId().equals(transactions.get(i).getId()))
                                transactionListPresenter.updateTransaction(transactions.get(i), TransactionsModel.lastLoadedTransactionsHolder.get(j).getAmount(), TransactionsModel.lastLoadedTransactionsHolder.get(j).getType(), TransactionsModel.lastLoadedTransactionsHolder.get(j).getDate(), TransactionsModel.lastLoadedTransactionsHolder.get(j).getEndDate(), TransactionsModel.lastLoadedTransactionsHolder.get(j).getTransactionInterval(), context.getApplicationContext());
                    }
                }
                transactionListPresenter.deleteTransactionsFromDB(context.getApplicationContext());
                for(int i = 0; i < transactionsToDeleteDB.size(); i++) {
                    transactionListPresenter.removeTransaction(transactionsToDeleteDB.get(i), context.getApplicationContext());
                }
                transactionListPresenter.deleteTransactionsDeletedFromDB(context.getApplicationContext());
                AccountPresenter accountPresenter = new AccountPresenter();
                Account account = accountPresenter.getAccountFromDB(context.getApplicationContext());
                accountPresenter.setMonthLimitAndSetTotalLimit(account.getMonthLimit(), account.getTotalLimit(), context.getApplicationContext());

                TransactionsModel.transactionsDeleted = new ArrayList<>();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }
}