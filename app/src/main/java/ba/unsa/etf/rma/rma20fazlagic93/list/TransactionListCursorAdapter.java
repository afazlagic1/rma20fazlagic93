package ba.unsa.etf.rma.rma20fazlagic93.list;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.widget.ImageView;
import android.widget.ResourceCursorAdapter;
import android.widget.TextView;

import ba.unsa.etf.rma.rma20fazlagic93.R;
import ba.unsa.etf.rma.rma20fazlagic93.util.TransactionDBOpenHelper;

public class TransactionListCursorAdapter extends ResourceCursorAdapter {
    public TextView title;
    public TextView iznos;
    public ImageView ikonica;

    public TransactionListCursorAdapter(Context context, int layout, Cursor c, boolean autoRequery) {
        super(context, layout, c, autoRequery);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        title = view.findViewById(R.id.title);
        iznos = view.findViewById(R.id.iznos);
        ikonica = view.findViewById(R.id.ikonica);
        title.setText(cursor.getString(cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_TITLE)));
        iznos.setText(cursor.getString(cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_AMOUNT)));
        String tip = cursor.getString(cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_TYPE));
        if(tip.equalsIgnoreCase("individual income"))
            ikonica.setImageResource(R.drawable.individual_income);
        else if(tip.equalsIgnoreCase("individual payment"))
            ikonica.setImageResource(R.drawable.individual_payment);
        else if(tip.equalsIgnoreCase("regular income"))
            ikonica.setImageResource(R.drawable.regular_income);
        else if(tip.equalsIgnoreCase("regular payment"))
            ikonica.setImageResource(R.drawable.regular_payment);
        else if(tip.equalsIgnoreCase("purchase"))
            ikonica.setImageResource(R.drawable.purchase_);
    }
}
