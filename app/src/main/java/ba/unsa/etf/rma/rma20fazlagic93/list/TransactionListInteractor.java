package ba.unsa.etf.rma.rma20fazlagic93.list;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import ba.unsa.etf.rma.rma20fazlagic93.data.Type;
import ba.unsa.etf.rma.rma20fazlagic93.util.TransactionDBOpenHelper;
import ba.unsa.etf.rma.rma20fazlagic93.data.Transaction;

public class TransactionListInteractor implements ITransactionListInteractor {
    private String path = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com/account/";
    private ArrayList<Transaction> transactionArrayList = new ArrayList<>();
    private Transaction transactionNow;

    public interface onTransactionLoadDone {
        void onDone(ArrayList<Transaction> transactions);
    }

    private onTransactionLoadDone caller;

    TransactionListInteractor(onTransactionLoadDone caller) {
        this.caller = caller;
    }

    @Override
    public void remove(final int id, final Transaction t, final Context context) {
        Runnable removeTransaction = new Runnable() {
            @Override
            public void run() {
                HttpURLConnection con;
                try {
                    URL url1  = new URL(path + "c78da961-b241-4175-84c3-34c2b6b88f2f" + "/transactions/" + id);
                    con = (HttpURLConnection) url1.openConnection();
                    con.setReadTimeout(2000);
                    con.setConnectTimeout(4000);
                    con.setRequestMethod("DELETE");
                    con.setDoInput(true);
                    con.setDoOutput(true);
                    con.setRequestProperty("Content-Type", "application/json");

                    InputStream in = new BufferedInputStream(con.getInputStream());
                    String rezultat = convertStreamToString(in);
                    JSONObject jo = new JSONObject(rezultat);
                    if(!jo.isNull("success")) {
                        Log.d("success", jo.getString("success"));
                        updateBudget(t, "delete");
                    }
                    if(!jo.isNull("error"))
                        Log.d("error", jo.getString("error"));

                } catch (IOException | JSONException e) {
                    e.printStackTrace(); //treba staviti u baze transakciju koja se briše



                    ContentResolver cr = context.getApplicationContext().getContentResolver();
                    Uri deletedtransactionsURI = Uri.parse("content://rma.provider.deletedtransactions/elements");
                    ContentValues contentValues = new ContentValues();
                    if(t.getId() == -1 || t.getId() == null || t.getId() == 0)
                        contentValues.put(TransactionDBOpenHelper.TRANSACTION_ID, -1);
                    else
                        contentValues.put(TransactionDBOpenHelper.TRANSACTION_ID, t.getId());
                    contentValues.put(TransactionDBOpenHelper.TRANSACTION_TITLE, t.getTitle());
                    SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                    String dateWithFormat = inputFormat.format(t.getDate());
                    contentValues.put(TransactionDBOpenHelper.TRANSACTION_DATE, dateWithFormat);
                    if(t.getEndDate() != null) {
                        String endDateWithFormat = inputFormat.format(t.getEndDate());
                        contentValues.put(TransactionDBOpenHelper.TRANSACTION_ENDDATE, endDateWithFormat);
                    }
                    contentValues.put(TransactionDBOpenHelper.TRANSACTION_AMOUNT, t.getAmount());
                    if(t.getType().equals(Type.INDIVIDUALINCOME))
                        contentValues.put(TransactionDBOpenHelper.TRANSACTION_TYPE, "individual income");
                    else if(t.getType().equals(Type.INDIVIDUALPAYMENT))
                        contentValues.put(TransactionDBOpenHelper.TRANSACTION_TYPE, "individual payment");
                    else if(t.getType().equals(Type.PURCHASE))
                        contentValues.put(TransactionDBOpenHelper.TRANSACTION_TYPE, "purchase");
                    else if(t.getType().equals(Type.REGULARPAYMENT))
                        contentValues.put(TransactionDBOpenHelper.TRANSACTION_TYPE, "regular payment");
                    else if(t.getType().equals(Type.REGULARINCOME))
                        contentValues.put(TransactionDBOpenHelper.TRANSACTION_TYPE, "regular income");
                    if(t.getItemDescription() != null)
                        contentValues.put(TransactionDBOpenHelper.TRANSACTION_ITEMDESCRIPTION, t.getItemDescription());
                    contentValues.put(TransactionDBOpenHelper.TRANSACTION_TRANSACTION_INTERVAL, t.getTransactionInterval());
                    contentValues.put(TransactionDBOpenHelper.TRANSACTION_ACCOUNT_ID, t.getAccountId());
                    cr.insert(deletedtransactionsURI, contentValues);
                }
            }
        };
        if(t.getId() != -1 && t.getId() != null && t.getId() != 0) {
            Thread thread = new Thread(removeTransaction);
            thread.start();
        }
        else {
            ContentResolver cr = context.getApplicationContext().getContentResolver();
            Uri deletedtransactionsURI = Uri.parse("content://rma.provider.deletedtransactions/elements");
            ContentValues contentValues = new ContentValues();
            if(t.getId() == -1 || t.getId() == null || t.getId() == 0)
                contentValues.put(TransactionDBOpenHelper.TRANSACTION_ID, -1);
            else
                contentValues.put(TransactionDBOpenHelper.TRANSACTION_ID, t.getId());
            contentValues.put(TransactionDBOpenHelper.TRANSACTION_TITLE, t.getTitle());
            SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            String dateWithFormat = inputFormat.format(t.getDate());
            contentValues.put(TransactionDBOpenHelper.TRANSACTION_DATE, dateWithFormat);
            if(t.getEndDate() != null) {
                String endDateWithFormat = inputFormat.format(t.getEndDate());
                contentValues.put(TransactionDBOpenHelper.TRANSACTION_ENDDATE, endDateWithFormat);
            }
            contentValues.put(TransactionDBOpenHelper.TRANSACTION_AMOUNT, t.getAmount());
            if(t.getType().equals(Type.INDIVIDUALINCOME))
                contentValues.put(TransactionDBOpenHelper.TRANSACTION_TYPE, "individual income");
            else if(t.getType().equals(Type.INDIVIDUALPAYMENT))
                contentValues.put(TransactionDBOpenHelper.TRANSACTION_TYPE, "individual payment");
            else if(t.getType().equals(Type.PURCHASE))
                contentValues.put(TransactionDBOpenHelper.TRANSACTION_TYPE, "purchase");
            else if(t.getType().equals(Type.REGULARPAYMENT))
                contentValues.put(TransactionDBOpenHelper.TRANSACTION_TYPE, "regular payment");
            else if(t.getType().equals(Type.REGULARINCOME))
                contentValues.put(TransactionDBOpenHelper.TRANSACTION_TYPE, "regular income");
            if(t.getItemDescription() != null)
                contentValues.put(TransactionDBOpenHelper.TRANSACTION_ITEMDESCRIPTION, t.getItemDescription());
            contentValues.put(TransactionDBOpenHelper.TRANSACTION_TRANSACTION_INTERVAL, t.getTransactionInterval());
            contentValues.put(TransactionDBOpenHelper.TRANSACTION_ACCOUNT_ID, t.getAccountId());
            cr.insert(deletedtransactionsURI, contentValues);
        }
    }

    private void updateBudget(final Transaction t, final String s) {
        final Double[] budget = new Double[1];

        final Runnable accountBudgetPostThread = new Runnable() {
            @Override
            public void run() {
                HttpURLConnection con = null;
                try {
                    String response = null;
                    URL url1 = new URL(path + "c78da961-b241-4175-84c3-34c2b6b88f2f");
                    con = (HttpURLConnection) url1.openConnection();
                    con.setReadTimeout(2000);
                    con.setConnectTimeout(4000);
                    con.setRequestMethod("POST");
                    con.setDoInput(true);
                    con.setDoOutput(true);
                    con.setRequestProperty("Content-Type", "application/json");

                    JSONObject jsonObject = new JSONObject();
                    if(t.getType().equals(Type.INDIVIDUALPAYMENT) || t.getType().equals(Type.PURCHASE) || t.getType().equals(Type.INDIVIDUALINCOME))
                        if(s.equals("delete"))
                            jsonObject.put("budget", budget[0] - t.getAmount());
                        else if(s.equals("add"))
                            jsonObject.put("budget", budget[0] + t.getAmount());
                    else {
                        double amountToRemove = 0;
                        Calendar c1 = Calendar.getInstance();
                        Calendar c2 = Calendar.getInstance();
                        c1.setTime(t.getDate());
                        c2.setTime(t.getEndDate());
                        while(c1.before(c2)) {
                            amountToRemove = amountToRemove + t.getAmount();
                            if(t.getTransactionInterval() == 0)
                                break;
                            c1.add(Calendar.DATE, t.getTransactionInterval());
                        }
                        if(s.equals("delete"))
                            jsonObject.put("budget", budget[0] - t.getAmount());
                        else if(s.equals("add"))
                            jsonObject.put("budget", budget[0] + t.getAmount());
                    }

                    DataOutputStream dataOutputStream = new DataOutputStream(con.getOutputStream());
                    dataOutputStream.writeBytes(jsonObject.toString());
                    dataOutputStream.flush();
                    dataOutputStream.close();

                    con.connect();

                    Log.d("uspjesan post za budget", String.valueOf(con.getResponseCode()));

                    if (con.getResponseCode() == 200) {

                    }

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        Runnable accountBudgetGetThread = new Runnable() {
            @Override
            public void run() {
                String url1 = path + "c78da961-b241-4175-84c3-34c2b6b88f2f";
                try {
                    URL url = new URL(url1);
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                    String rezultat = convertStreamToString(in);
                    JSONObject jo = new JSONObject(rezultat);
                    if(jo.isNull("budget"))
                        budget[0] = 0.;
                    else
                        budget[0] = jo.getDouble("budget");
                    Thread thread = new Thread(accountBudgetPostThread);
                    thread.start();
                }
                catch (MalformedURLException e) {
                    e.printStackTrace();
                    Log.d("MalformedURLException", "e");
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.d("IOException", "e");
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("JSONException", "e");
                }
            }
        };

        Thread getB = new Thread(accountBudgetGetThread);
        getB.start();
    }

    @Override
    public void add(final Transaction t, final Context context) {
        Runnable addThread = new Runnable() {
            @Override
            public void run() {
                HttpURLConnection con;
                try {
                    URL url1 = new URL(path + "c78da961-b241-4175-84c3-34c2b6b88f2f" + "/transactions");
                    con = (HttpURLConnection) url1.openConnection();
                    con.setReadTimeout(2000);
                    con.setConnectTimeout(4000);
                    con.setRequestMethod("POST");
                    con.setDoInput(true);
                    con.setDoOutput(true);
                    con.setRequestProperty("Content-Type", "application/json");

                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("title", t.getTitle());
                    SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                    String dateWithFormat = inputFormat.format(t.getDate());
                    jsonObject.put("date", dateWithFormat);
                    if(t.getEndDate() != null) {
                        String endDateWithFormat = inputFormat.format(t.getEndDate());
                        jsonObject.put("endDate", endDateWithFormat);
                    }
                    else
                        jsonObject.put("endDate", JSONObject.NULL);
                    if(t.getType() != null)
                        if (t.getType().equals(Type.REGULARPAYMENT)) {
                            jsonObject.put("typeId", 1);
                            jsonObject.put("amount", t.getAmount()*(-1));
                            jsonObject.put("transactionInterval", t.getTransactionInterval());
                        } else if (t.getType().equals(Type.INDIVIDUALPAYMENT)) {
                            jsonObject.put("typeId", 5);
                            jsonObject.put("amount", t.getAmount()*(-1));
                            jsonObject.put("transactionInterval", JSONObject.NULL);
                        } else if (t.getType().equals(Type.INDIVIDUALINCOME)) {
                            jsonObject.put("typeId", 4);
                            jsonObject.put("amount", t.getAmount());
                            jsonObject.put("transactionInterval", JSONObject.NULL);
                        }
                        else if (t.getType().equals(Type.REGULARINCOME)) {
                            jsonObject.put("typeId", 2);
                            jsonObject.put("amount", t.getAmount());
                            jsonObject.put("transactionInterval", t.getTransactionInterval());
                        }
                        else if (t.getType().equals(Type.PURCHASE)) {
                            jsonObject.put("typeId", 3);
                            jsonObject.put("amount", t.getAmount()*(-1));
                            jsonObject.put("transactionInterval", JSONObject.NULL);
                        }
                    if(t.getItemDescription() == null)
                        jsonObject.put("itemDescription", JSONObject.NULL);
                    else
                        jsonObject.put("itemDescription", t.getItemDescription());

                    DataOutputStream dataOutputStream = new DataOutputStream(con.getOutputStream());
                    dataOutputStream.writeBytes(jsonObject.toString());
                    dataOutputStream.flush();
                    dataOutputStream.close();

                    con.connect();

                    Log.d("uspjesan postt", String.valueOf(con.getResponseCode()));

                    if(con.getResponseCode() == 200) {
                        updateBudget(t, "add");
                    }

                } catch (ProtocolException | JSONException e) {
                    e.printStackTrace();
                    Log.d("Exception", "Protocol ili JSON");
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                    Log.d("Exception", "Malformed");
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.d("Exception", "IO"); //treba ubaciti u bazu kada se desi ovaj Exception
                    ContentResolver cr = context.getApplicationContext().getContentResolver();
                    Uri transactionsURI = Uri.parse("content://rma.provider.transactions/elements");
                    ContentValues contentValues = new ContentValues();
                    if(t.getId() == -1 || t.getId() == null || t.getId() == 0)
                        contentValues.put(TransactionDBOpenHelper.TRANSACTION_ID, -1);
                    else
                        contentValues.put(TransactionDBOpenHelper.TRANSACTION_ID, t.getId());
                    contentValues.put(TransactionDBOpenHelper.TRANSACTION_TITLE, t.getTitle());
                    SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                    String dateWithFormat = inputFormat.format(t.getDate());
                    contentValues.put(TransactionDBOpenHelper.TRANSACTION_DATE, dateWithFormat);
                    if(t.getEndDate() != null) {
                        String endDateWithFormat = inputFormat.format(t.getEndDate());
                        contentValues.put(TransactionDBOpenHelper.TRANSACTION_ENDDATE, endDateWithFormat);
                    }
                    contentValues.put(TransactionDBOpenHelper.TRANSACTION_AMOUNT, t.getAmount());
                    if(t.getType().equals(Type.INDIVIDUALINCOME))
                        contentValues.put(TransactionDBOpenHelper.TRANSACTION_TYPE, "individual income");
                    else if(t.getType().equals(Type.INDIVIDUALPAYMENT))
                        contentValues.put(TransactionDBOpenHelper.TRANSACTION_TYPE, "individual payment");
                    else if(t.getType().equals(Type.PURCHASE))
                        contentValues.put(TransactionDBOpenHelper.TRANSACTION_TYPE, "purchase");
                    else if(t.getType().equals(Type.REGULARPAYMENT))
                        contentValues.put(TransactionDBOpenHelper.TRANSACTION_TYPE, "regular payment");
                    else if(t.getType().equals(Type.REGULARINCOME))
                        contentValues.put(TransactionDBOpenHelper.TRANSACTION_TYPE, "regular income");
                    if(t.getItemDescription() != null)
                        contentValues.put(TransactionDBOpenHelper.TRANSACTION_ITEMDESCRIPTION, t.getItemDescription());
                    contentValues.put(TransactionDBOpenHelper.TRANSACTION_TRANSACTION_INTERVAL, t.getTransactionInterval());
                    contentValues.put(TransactionDBOpenHelper.TRANSACTION_ACCOUNT_ID, t.getAccountId());
                    cr.insert(transactionsURI, contentValues);
                }
            }
        };
        Thread thread = new Thread(addThread);
        thread.start();
    }

    @Override
    public void getTransactions(String what, String sort, String month, String year) {
        Log.d("pozvana", "notype");
        getTransactionsTask(what, sort, month, year);
    }

    @Override
    public void getTransactions(String what, String typeId, String sort, String month, String year) {
        Log.d("pozvana", "type");
        getTransactionsTask(what, typeId, sort, month, year);
    }

    @Override
    public void updateTransaction(final Transaction t, final double formerAmount, final Type formerType, final Date formerDate, final Date formerEndDate, final int formerInterval, final Context context) {
        final Runnable updateThread = new Runnable() {
            @Override
            public void run() {
                HttpURLConnection con = null;
                try {
                    String response = null;
                    URL url1 = new URL(path + "c78da961-b241-4175-84c3-34c2b6b88f2f" + "/transactions/" + t.getId());
                    con = (HttpURLConnection) url1.openConnection();
                    con.setReadTimeout(2000);
                    con.setConnectTimeout(4000);
                    con.setRequestMethod("POST");
                    con.setDoInput(true);
                    con.setDoOutput(true);
                    con.setRequestProperty("Content-Type", "application/json");

                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("title", t.getTitle());
                    SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                    String dateWithFormat = inputFormat.format(t.getDate());
                    Log.d("dateFormatiran", dateWithFormat);
                    jsonObject.put("date", dateWithFormat);
                    if(t.getEndDate() != null) {
                        String endDateWithFormat = inputFormat.format(t.getEndDate());
                        jsonObject.put("endDate", endDateWithFormat);
                    }
                    else
                        jsonObject.put("endDate", JSONObject.NULL);
                    Log.d("regularincome", url1.toString());
                    if(t.getType() != null) {
                        Log.d("tip", t.getType().toString());
                        if (t.getType().equals(Type.REGULARPAYMENT)) {
                            jsonObject.put("TransactionTypeId", 1);
                            jsonObject.put("amount", t.getAmount()*(-1));
                            jsonObject.put("transactionInterval", t.getTransactionInterval());
                        } else if (t.getType().equals(Type.INDIVIDUALPAYMENT)) {
                            jsonObject.put("TransactionTypeId", 5);
                            jsonObject.put("amount", t.getAmount()*(-1));
                            jsonObject.put("transactionInterval", JSONObject.NULL);
                        } else if (t.getType().equals(Type.INDIVIDUALINCOME)) {
                            jsonObject.put("TransactionTypeId", 4);
                            jsonObject.put("amount", t.getAmount());
                            jsonObject.put("transactionInterval", JSONObject.NULL);
                        }
                        else if (t.getType().equals(Type.REGULARINCOME)) {
                            jsonObject.put("TransactionTypeId", 2);
                            jsonObject.put("amount", t.getAmount());
                            jsonObject.put("transactionInterval", t.getTransactionInterval());
                        }
                        else if (t.getType().equals(Type.PURCHASE)) {
                            jsonObject.put("TransactionTypeId", 3);
                            jsonObject.put("amount", t.getAmount()*(-1));
                            jsonObject.put("transactionInterval", JSONObject.NULL);
                        }
                    }
                    if(t.getItemDescription() == null)
                        jsonObject.put("itemDescription", JSONObject.NULL);
                    else
                        jsonObject.put("itemDescription", t.getItemDescription());

                    Log.d("json", jsonObject.toString());

                    DataOutputStream dataOutputStream = new DataOutputStream(con.getOutputStream());
                    dataOutputStream.writeBytes(jsonObject.toString());
                    dataOutputStream.flush();
                    dataOutputStream.close();

                    con.connect();

                    Log.d("uspjesan post", String.valueOf(con.getResponseCode()));

                    if(con.getResponseCode() == 200) {
                        updateBudget(t, "add");
                        Transaction helpToDelete = new Transaction(t.getId(), formerDate, formerAmount, t.getTitle(), formerType, t.getItemDescription(), formerInterval, formerEndDate, t.getAccountId());
                        updateBudget(helpToDelete, "delete");
                    }
                } catch (MalformedURLException | ProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) { //ovdje treba ubaciti u bazu izmijenjenu transakciju, prvo izbrisati staru izmjenu
                    e.printStackTrace();
                    Log.d("id-transakcije-je",String.valueOf(t.getId()));

                    ContentResolver contentResolver = context.getApplicationContext().getContentResolver();
                    Uri adresa = Uri.parse("content://rma.provider.transactions/elements");
                    String where = TransactionDBOpenHelper.TRANSACTION_ID + "=?";
                    String whereArgs[] = new String[]{String.valueOf(t.getId())};
                    contentResolver.delete(adresa, where, whereArgs);

                    ContentResolver cr = context.getApplicationContext().getContentResolver();
                    Uri transactionsURI = Uri.parse("content://rma.provider.transactions/elements");
                    ContentValues contentValues = new ContentValues();
                    if(t.getId() != -1)
                        contentValues.put(TransactionDBOpenHelper.TRANSACTION_ID, t.getId());
                    else {
                        contentValues.put(TransactionDBOpenHelper.TRANSACTION_ID, -1);
                    }
                    contentValues.put(TransactionDBOpenHelper.TRANSACTION_TITLE, t.getTitle());
                    SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                    String dateWithFormat = inputFormat.format(t.getDate());
                    contentValues.put(TransactionDBOpenHelper.TRANSACTION_DATE, dateWithFormat);
                    if(t.getEndDate() != null) {
                        String endDateWithFormat = inputFormat.format(t.getEndDate());
                        contentValues.put(TransactionDBOpenHelper.TRANSACTION_ENDDATE, endDateWithFormat);
                    }
                    contentValues.put(TransactionDBOpenHelper.TRANSACTION_AMOUNT, t.getAmount());
                    if(t.getType().equals(Type.INDIVIDUALINCOME))
                        contentValues.put(TransactionDBOpenHelper.TRANSACTION_TYPE, "individual income");
                    else if(t.getType().equals(Type.INDIVIDUALPAYMENT))
                        contentValues.put(TransactionDBOpenHelper.TRANSACTION_TYPE, "individual payment");
                    else if(t.getType().equals(Type.PURCHASE))
                        contentValues.put(TransactionDBOpenHelper.TRANSACTION_TYPE, "purchase");
                    else if(t.getType().equals(Type.REGULARPAYMENT))
                        contentValues.put(TransactionDBOpenHelper.TRANSACTION_TYPE, "regular payment");
                    else if(t.getType().equals(Type.REGULARINCOME))
                        contentValues.put(TransactionDBOpenHelper.TRANSACTION_TYPE, "regular income");
                    if(t.getItemDescription() != null)
                        contentValues.put(TransactionDBOpenHelper.TRANSACTION_ITEMDESCRIPTION, t.getItemDescription());
                    contentValues.put(TransactionDBOpenHelper.TRANSACTION_TRANSACTION_INTERVAL, t.getTransactionInterval());
                    contentValues.put(TransactionDBOpenHelper.TRANSACTION_ACCOUNT_ID, t.getAccountId());
                    cr.insert(transactionsURI, contentValues);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        };
        if(t.getId() != -1 && t.getId() != null && t.getId() != 0) {
            Thread thread = new Thread(updateThread);
            thread.start();
        }
        else {
            ContentResolver contentResolver = context.getApplicationContext().getContentResolver();
            Uri adresa = Uri.parse("content://rma.provider.transactions/elements");
            String where = TransactionDBOpenHelper.TRANSACTION_INTERNAL_ID + "=?";
            String whereArgs[] = new String[]{String.valueOf(t.getInternalId())};
            Log.d("internal_Id", String.valueOf(t.getInternalId()));
            contentResolver.delete(adresa, where, whereArgs);

            ContentResolver cr = context.getApplicationContext().getContentResolver();
            Uri transactionsURI = Uri.parse("content://rma.provider.transactions/elements");
            ContentValues contentValues = new ContentValues();
            if(t.getId() != -1)
                contentValues.put(TransactionDBOpenHelper.TRANSACTION_ID, t.getId());
            else {
                contentValues.put(TransactionDBOpenHelper.TRANSACTION_ID, -1);
            }
            contentValues.put(TransactionDBOpenHelper.TRANSACTION_TITLE, t.getTitle());
            SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            String dateWithFormat = inputFormat.format(t.getDate());
            contentValues.put(TransactionDBOpenHelper.TRANSACTION_DATE, dateWithFormat);
            if(t.getEndDate() != null) {
                String endDateWithFormat = inputFormat.format(t.getEndDate());
                contentValues.put(TransactionDBOpenHelper.TRANSACTION_ENDDATE, endDateWithFormat);
            }
            contentValues.put(TransactionDBOpenHelper.TRANSACTION_AMOUNT, t.getAmount());
            if(t.getType().equals(Type.INDIVIDUALINCOME))
                contentValues.put(TransactionDBOpenHelper.TRANSACTION_TYPE, "individual income");
            else if(t.getType().equals(Type.INDIVIDUALPAYMENT))
                contentValues.put(TransactionDBOpenHelper.TRANSACTION_TYPE, "individual payment");
            else if(t.getType().equals(Type.PURCHASE))
                contentValues.put(TransactionDBOpenHelper.TRANSACTION_TYPE, "purchase");
            else if(t.getType().equals(Type.REGULARPAYMENT))
                contentValues.put(TransactionDBOpenHelper.TRANSACTION_TYPE, "regular payment");
            else if(t.getType().equals(Type.REGULARINCOME))
                contentValues.put(TransactionDBOpenHelper.TRANSACTION_TYPE, "regular income");
            if(t.getItemDescription() != null)
                contentValues.put(TransactionDBOpenHelper.TRANSACTION_ITEMDESCRIPTION, t.getItemDescription());
            contentValues.put(TransactionDBOpenHelper.TRANSACTION_TRANSACTION_INTERVAL, t.getTransactionInterval());
            contentValues.put(TransactionDBOpenHelper.TRANSACTION_ACCOUNT_ID, t.getAccountId());
            cr.insert(transactionsURI, contentValues);
        }
    }

    @Override
    public Cursor getTransactionCursor(Context applicationContext, String month, String year) {
        ContentResolver contentResolver = applicationContext.getApplicationContext().getContentResolver();
        String[] kolone = new String[]{
                TransactionDBOpenHelper.TRANSACTION_INTERNAL_ID,
                TransactionDBOpenHelper.TRANSACTION_TITLE,
                TransactionDBOpenHelper.TRANSACTION_DATE,
                TransactionDBOpenHelper.TRANSACTION_ENDDATE,
                TransactionDBOpenHelper.TRANSACTION_AMOUNT,
                TransactionDBOpenHelper.TRANSACTION_TYPE,
                TransactionDBOpenHelper.TRANSACTION_ITEMDESCRIPTION,
                TransactionDBOpenHelper.TRANSACTION_TRANSACTION_INTERVAL,
                TransactionDBOpenHelper.TRANSACTION_ACCOUNT_ID
        };
        Uri adresa = Uri.parse("content://rma.provider.transactions/elements");
        String where = TransactionDBOpenHelper.TRANSACTION_DATE + " like ?";
        String whereArgs[] = new String[]{year + "-" + month + "-%"};
        String order = null;
        Cursor cur = contentResolver.query(adresa,kolone,where,whereArgs,order);
        return cur;
    }

    @Override
    public Transaction getTransactionFromDB(int internalId, Context context) throws ParseException {
        Transaction transaction = null;
        ContentResolver cr = context.getApplicationContext().getContentResolver();
        String[] kolone = null;
        Uri adresa = ContentUris.withAppendedId(Uri.parse("content://rma.provider.transactions/elements"),internalId);
        String where = null;
        String whereArgs[] = null;
        String order = null;
        Cursor cursor = cr.query(adresa,kolone,where,whereArgs,order);
        if (cursor != null) {
            cursor.moveToFirst();
            int idPos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_ID);
            int idIn = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_INTERNAL_ID);
            int titlePos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_TITLE);
            int datePos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_DATE);
            int endDatePos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_ENDDATE);
            int amountPos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_AMOUNT);
            int itemDescriptionPos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_ITEMDESCRIPTION);
            int intervalPos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_TRANSACTION_INTERVAL);
            int accountIdPos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_ACCOUNT_ID);
            SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            Calendar c = Calendar.getInstance();
            Date date = inputFormat.parse(cursor.getString(datePos));
            Date endDate = null;
            if(cursor.getString(endDatePos) != null)
                endDate = inputFormat.parse(cursor.getString(endDatePos));
            Type type = null;
            String tip = cursor.getString(cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_TYPE));
            if(tip.equalsIgnoreCase("individual income"))
                type = Type.INDIVIDUALINCOME;
            else if(tip.equalsIgnoreCase("individual payment"))
                type = Type.INDIVIDUALPAYMENT;
            else if(tip.equalsIgnoreCase("regular income"))
                type = Type.REGULARINCOME;
            else if(tip.equalsIgnoreCase("regular payment"))
                type = Type.REGULARPAYMENT;
            else if(tip.equalsIgnoreCase("purchase"))
                type = Type.PURCHASE;
            else
                type = Type.INDIVIDUALINCOME;
            transaction = new Transaction(cursor.getInt(idPos), date, cursor.getDouble(amountPos), cursor.getString(titlePos), type, cursor.getString(itemDescriptionPos), cursor.getInt(intervalPos), endDate, cursor.getInt(accountIdPos), internalId);
        }
        return transaction;
    }

    @Override
    public ArrayList<Transaction> getTransactionsFromDB(Context context) throws ParseException {
        ArrayList<Transaction> transactionsDB = new ArrayList<>();
        ContentResolver contentResolver = context.getApplicationContext().getContentResolver();
        String[] kolone = new String[]{
                TransactionDBOpenHelper.TRANSACTION_ID,
                TransactionDBOpenHelper.TRANSACTION_INTERNAL_ID,
                TransactionDBOpenHelper.TRANSACTION_TITLE,
                TransactionDBOpenHelper.TRANSACTION_DATE,
                TransactionDBOpenHelper.TRANSACTION_ENDDATE,
                TransactionDBOpenHelper.TRANSACTION_AMOUNT,
                TransactionDBOpenHelper.TRANSACTION_TYPE,
                TransactionDBOpenHelper.TRANSACTION_ITEMDESCRIPTION,
                TransactionDBOpenHelper.TRANSACTION_TRANSACTION_INTERVAL,
                TransactionDBOpenHelper.TRANSACTION_ACCOUNT_ID
        };
        Uri adresa = Uri.parse("content://rma.provider.transactions/elements");
        String where = null;
        String whereArgs[] = null;
        String order = null;
        Cursor cursor = contentResolver.query(adresa,kolone,where,whereArgs,order);
        if(cursor != null) {
            Log.d("okejDB", "ok");
            cursor.moveToFirst();
            for (int i = 0; i < cursor.getCount(); i++) {
                int idPos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_ID);
                int idIn = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_INTERNAL_ID);
                int titlePos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_TITLE);
                int datePos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_DATE);
                int endDatePos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_ENDDATE);
                int amountPos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_AMOUNT);
                int itemDescriptionPos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_ITEMDESCRIPTION);
                int intervalPos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_TRANSACTION_INTERVAL);
                int accountIdPos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_ACCOUNT_ID);
                SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                Calendar c = Calendar.getInstance();
                Date date = inputFormat.parse(cursor.getString(datePos));
                Date endDate = null;
                if(cursor.getString(endDatePos) != null)
                    endDate = inputFormat.parse(cursor.getString(endDatePos));
                Type type = null;
                String tip = cursor.getString(cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_TYPE));
                if(tip.equalsIgnoreCase("individual income"))
                    type = Type.INDIVIDUALINCOME;
                else if(tip.equalsIgnoreCase("individual payment"))
                    type = Type.INDIVIDUALPAYMENT;
                else if(tip.equalsIgnoreCase("regular income"))
                    type = Type.REGULARINCOME;
                else if(tip.equalsIgnoreCase("regular payment"))
                    type = Type.REGULARPAYMENT;
                else if(tip.equalsIgnoreCase("purchase"))
                    type = Type.PURCHASE;
                else
                    type = Type.INDIVIDUALINCOME;
                Transaction transaction = new Transaction(cursor.getInt(idPos), date, cursor.getDouble(amountPos), cursor.getString(titlePos), type, cursor.getString(itemDescriptionPos), cursor.getInt(intervalPos), endDate, cursor.getInt(accountIdPos), cursor.getInt(idIn));
                transactionsDB.add(transaction);
                Log.d("transakcija " + String.valueOf(i), transaction.getTitle() + ", id " + String.valueOf(transaction.getId()));
                cursor.moveToNext();
            }
            Log.d("sizeDB", String.valueOf(transactionsDB.size()));
        }
        cursor.close();
        return transactionsDB;
    }

    @Override
    public void deleteTransactionsFromDB(Context applicationContext) {
        ContentResolver cr = applicationContext.getApplicationContext().getContentResolver();
        Uri adresa = Uri.parse("content://rma.provider.transactions/elements");
        String where = null;
        String whereArgs[] = null;
        cr.delete(adresa, where, whereArgs);
    }

    @Override
    public void undoDeleteTransactionDB(Transaction undoDeleteThisTransaction, Context applicationContext) {
        ContentResolver cr = applicationContext.getApplicationContext().getContentResolver();
        Uri adresa = Uri.parse("content://rma.provider.deletedtransactions");
        String where = null;
        String whereArgs[] = null;
        if(undoDeleteThisTransaction.getId() != -1 && undoDeleteThisTransaction.getId() != 0 && undoDeleteThisTransaction.getId() != null) {
            where = TransactionDBOpenHelper.TRANSACTION_ID + "=?";
            whereArgs = new String[] {String.valueOf(undoDeleteThisTransaction.getId())};
            cr.delete(adresa, where, whereArgs);
        }
        else {
            where = TransactionDBOpenHelper.TRANSACTION_INTERNAL_ID +"=?";
            whereArgs = new String[]{String.valueOf(undoDeleteThisTransaction.getInternalId())};
            cr.delete(adresa, where, whereArgs);
        }
    }

    @Override
    public ArrayList<Transaction> getDeletedTransactionsFromDB(Context applicationContext) throws ParseException {
        ArrayList<Transaction> transactionsDB = new ArrayList<>();
        ContentResolver cr = applicationContext.getApplicationContext().getContentResolver();
        String[] kolone = new String[]{
                TransactionDBOpenHelper.TRANSACTION_ID,
                TransactionDBOpenHelper.TRANSACTION_INTERNAL_ID,
                TransactionDBOpenHelper.TRANSACTION_TITLE,
                TransactionDBOpenHelper.TRANSACTION_DATE,
                TransactionDBOpenHelper.TRANSACTION_ENDDATE,
                TransactionDBOpenHelper.TRANSACTION_AMOUNT,
                TransactionDBOpenHelper.TRANSACTION_TYPE,
                TransactionDBOpenHelper.TRANSACTION_ITEMDESCRIPTION,
                TransactionDBOpenHelper.TRANSACTION_TRANSACTION_INTERVAL,
                TransactionDBOpenHelper.TRANSACTION_ACCOUNT_ID
        };
        Uri adresa = Uri.parse("content://rma.provider.deletedtransactions/elements");
        String where = null;
        String whereArgs[] = null;
        String order = null;
        Cursor cursor = cr.query(adresa,kolone,where,whereArgs,order);
        if(cursor != null) {
                cursor.moveToFirst();
                for(int i = 0; i < cursor.getCount(); i++) {
                    int idPos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_ID);
                    int idIn = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_INTERNAL_ID);
                    int titlePos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_TITLE);
                    int datePos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_DATE);
                    int endDatePos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_ENDDATE);
                    int amountPos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_AMOUNT);
                    int itemDescriptionPos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_ITEMDESCRIPTION);
                    int intervalPos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_TRANSACTION_INTERVAL);
                    int accountIdPos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_ACCOUNT_ID);
                    SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                    Calendar c = Calendar.getInstance();
                    Date date = inputFormat.parse(cursor.getString(datePos));
                    Date endDate = null;
                    if(cursor.getString(endDatePos) != null)
                        endDate = inputFormat.parse(cursor.getString(endDatePos));
                    Type type = null;
                    String tip = cursor.getString(cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_TYPE));
                    if(tip.equalsIgnoreCase("individual income"))
                        type = Type.INDIVIDUALINCOME;
                    else if(tip.equalsIgnoreCase("individual payment"))
                        type = Type.INDIVIDUALPAYMENT;
                    else if(tip.equalsIgnoreCase("regular income"))
                        type = Type.REGULARINCOME;
                    else if(tip.equalsIgnoreCase("regular payment"))
                        type = Type.REGULARPAYMENT;
                    else if(tip.equalsIgnoreCase("purchase"))
                        type = Type.PURCHASE;
                    else
                        type = Type.INDIVIDUALINCOME;
                    Transaction transaction = new Transaction(cursor.getInt(idPos), date, cursor.getDouble(amountPos), cursor.getString(titlePos), type, cursor.getString(itemDescriptionPos), cursor.getInt(intervalPos), endDate, cursor.getInt(accountIdPos), cursor.getInt(idIn));
                    transactionsDB.add(transaction);
                    Log.d("transakcijaobrisana " + String.valueOf(i), transaction.getTitle() + ", id " + String.valueOf(transaction.getId()));
                    cursor.moveToNext();
                }
            Log.d("sizeDB", String.valueOf(transactionsDB.size()));
            }
        cursor.close();
        return transactionsDB;
    }

    @Override
    public void deleteTransactionsDeletedFromDB(Context applicationContext) {
        ContentResolver cr = applicationContext.getApplicationContext().getContentResolver();
        Uri adresa = Uri.parse("content://rma.provider.deletedtransactions/elements");
        String where = null;
        String whereArgs[] = null;
        cr.delete(adresa, where, whereArgs);
    }

    public void getTransactionsTask(final String... strings) {
        transactionArrayList = new ArrayList<>();
        String query1 = null;
        try {
            query1 = URLEncoder.encode(strings[0], "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        final String query = query1;
        Runnable getThread = new Runnable() {
            @Override
            public void run() {
                if (!query.equals("filter-sort")) {
                    int k = 0;
                    while (true) {
                        String url1 = path + "c78da961-b241-4175-84c3-34c2b6b88f2f" + "/transactions" + "?page=" + k;
                        try {
                            URL url = new URL(url1);
                            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                            String rezultat = convertStreamToString(in);
                            JSONObject jo = new JSONObject(rezultat);
                            JSONArray transactions = jo.getJSONArray("transactions");

                            if (transactions.length() == 0) break;

                            for (int i = 0; i < transactions.length(); i++) {
                                JSONObject transaction = transactions.getJSONObject(i);
                                Integer id = transaction.getInt("id");
                                String date;
                                Date date1;
                                if (!transaction.isNull("date")) {
                                    date = transaction.getString("date");
                                    SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                                    date1 = inputFormat.parse(date);
                                } else {
                                    continue;
                                }
                                String title = transaction.getString("title");
                                if (!(title.length() > 3 && title.length() < 15))
                                    continue;
                                Double amount = transaction.getDouble("amount");
                                Integer transactionTypeId;
                                if (!transaction.isNull("TransactionTypeId"))
                                    transactionTypeId = transaction.getInt("TransactionTypeId");
                                else
                                    continue;
                                String itemDescription;
                                if (transaction.isNull("itemDescription")) {
                                    if (transactionTypeId != 2 && transactionTypeId != 4)
                                        itemDescription = "";
                                    else
                                        itemDescription = null;
                                } else {
                                    itemDescription = transaction.getString("itemDescription");
                                    if (transactionTypeId == 2 && itemDescription != "")
                                        continue;
                                    else if (transactionTypeId == 4 && itemDescription != "")
                                        continue;
                                }
                                Integer transactionInterval;
                                if (transaction.isNull("transactionInterval")) {
                                    transactionInterval = 0;
                                    if (transactionTypeId == 1)
                                        continue;
                                    else if (transactionTypeId == 2)
                                        continue;
                                } else {
                                    transactionInterval = transaction.getInt("transactionInterval");
                                    if (transactionTypeId != 1 && transactionTypeId != 2 && transactionInterval != 0)
                                        continue;
                                }
                                String endDate;
                                Date date2 = null;
                                if (!transaction.isNull("endDate")) {
                                    endDate = transaction.getString("endDate");
                                    SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                                    date2 = inputFormat.parse(endDate);
                                    if (transactionTypeId != 1 && transactionTypeId != 2)
                                        continue;
                                } else {
                                    date2 = null;
                                    if (transactionTypeId == 1)
                                        continue;
                                    if (transactionTypeId == 2)
                                        continue;
                                }
                                String createdAt = transaction.getString("createdAt");
                                String updatedAt = transaction.getString("updatedAt");
                                Integer accountId = transaction.getInt("AccountId");
                                transactionNow = new Transaction(id, date1, amount, title, null, itemDescription, transactionInterval, date2, accountId);
                                if (transactionNow.getTitle().length() > 3 && transactionNow.getTitle().length() < 15)
                                    transactionArrayList.add(transactionNow);
                                addType(transactionTypeId);
                            }
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                            Log.d("MalformedURLException", "e");
                            break;
                        } catch (IOException e) {
                            e.printStackTrace();
                            Log.d("IOException", "e");
                            break;
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("JSONException", "e");
                            break;
                        } catch (ParseException e) {
                            Log.i("1", "ne valja date formatter");
                            e.printStackTrace();
                            break;
                        }
                        k++;
                    }
                } else if (query.equals("filter-sort")) {
                    int k = 0;
                    String typeID, sort, month, year;
                    sort = null;
                    month = null;
                    year = null;
                    typeID = "1";
                    if (strings.length == 5) {
                        try {
                            typeID = URLEncoder.encode(strings[1], "utf-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                        try {
                            sort = URLEncoder.encode(strings[2], "utf-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                        try {
                            month = URLEncoder.encode(strings[3], "utf-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                        try {
                            year = URLEncoder.encode(strings[4], "utf-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    } else {
                        try {
                            sort = URLEncoder.encode(strings[1], "utf-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                        try {
                            month = URLEncoder.encode(strings[2], "utf-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                        try {
                            year = URLEncoder.encode(strings[3], "utf-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                    while (true) {
                        String url1 = "";
                        if (strings.length == 5)
                            url1 = path + "c78da961-b241-4175-84c3-34c2b6b88f2f" + "/transactions/filter" + "?page=" + k + "&typeId=" + typeID + "&sort=" + sort + "&month=" + month + "&year=" + year;
                        else
                            url1 = path + "c78da961-b241-4175-84c3-34c2b6b88f2f" + "/transactions/filter" + "?page=" + k + "&sort=" + sort + "&month=" + month + "&year=" + year;
                        try {
                            Log.d("put", url1);
                            URL url = new URL(url1);
                            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                            String rezultat = convertStreamToString(in);
                            JSONObject jo = new JSONObject(rezultat);
                            JSONArray transactions = jo.getJSONArray("transactions");
                            if (transactions.length() == 0) break;
                            for (int i = 0; i < transactions.length(); i++) {
                                JSONObject transaction = transactions.getJSONObject(i);
                                Integer id = transaction.getInt("id");
                                String date;
                                Date date1;
                                if (!transaction.isNull("date")) {
                                    date = transaction.getString("date");
                                    SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                                    date1 = inputFormat.parse(date);
                                } else {
                                    continue;
                                }
                                String title = transaction.getString("title");
                                if (!(title.length() > 3 && title.length() < 15))
                                    continue;
                                Double amount = transaction.getDouble("amount");
                                Integer transactionTypeId;
                                if (!transaction.isNull("TransactionTypeId"))
                                    transactionTypeId = transaction.getInt("TransactionTypeId");
                                else
                                    continue;
                                String itemDescription;
                                if (transaction.isNull("itemDescription")) {
                                    if (transactionTypeId != 2 && transactionTypeId != 4)
                                        itemDescription = "";
                                    else
                                        itemDescription = null;
                                } else {
                                    itemDescription = transaction.getString("itemDescription");
                                    if (transactionTypeId == 2 && itemDescription != "")
                                        continue;
                                    else if (transactionTypeId == 4 && itemDescription != "")
                                        continue;
                                }
                                Integer transactionInterval;
                                if (transaction.isNull("transactionInterval")) {
                                    transactionInterval = 0;
                                    if (transactionTypeId == 1)
                                        continue;
                                    else if (transactionTypeId == 2)
                                        continue;
                                } else {
                                    transactionInterval = transaction.getInt("transactionInterval");
                                    if (transactionTypeId != 1 && transactionTypeId != 2 && transactionInterval != 0)
                                        continue;
                                }
                                String endDate;
                                Date date2 = null;
                                if (!transaction.isNull("endDate")) {
                                    endDate = transaction.getString("endDate");
                                    SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                                    date2 = inputFormat.parse(endDate);
                                    if (transactionTypeId != 1 && transactionTypeId != 2)
                                        continue;
                                } else {
                                    date2 = null;
                                    if (transactionTypeId == 1)
                                        continue;
                                    if (transactionTypeId == 2)
                                        continue;
                                }
                                String createdAt = transaction.getString("createdAt");
                                String updatedAt = transaction.getString("updatedAt");
                                Integer accountId = transaction.getInt("AccountId");
                                transactionNow = new Transaction(id, date1, amount, title, null, itemDescription, transactionInterval, date2, accountId);
                                if (transactionNow.getTitle().length() > 3 && transactionNow.getTitle().length() < 15 && transactionTypeId != 1 && transactionTypeId != 2)
                                    transactionArrayList.add(transactionNow);
                                addType(transactionTypeId);
                            }
                            //TransactionsModel.transactionsWereLoaded = true;
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                            Log.d("MalformedURLException", "e");
                            break;
                        } catch (IOException e) {
                            e.printStackTrace();
                            Log.d("IOException", "e");
                            transactionArrayList = null;
                            break;
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("JSONException", "e");
                            break;
                        } catch (ParseException e) {
                            Log.i("1", "ne valja date formatter");
                            e.printStackTrace();
                            break;
                        }
                        k++;
                    }
                    if (strings.length == 4)
                        addRegularTransactions(Integer.parseInt(month), Integer.valueOf(year), null);
                    else if (strings.length == 5 && (typeID == "1" || typeID == "2"))
                        addRegularTransactions(Integer.parseInt(month), Integer.valueOf(year), Integer.valueOf(typeID));
                }
            }
        };
            Thread thread = new Thread(getThread);
            thread.start();
            while(thread.isAlive())
                Log.d("loading", "true");
            if(caller != null)
                caller.onDone(transactionArrayList);
    }

    private void addRegularTransactions(Integer month, Integer year, Integer typeID) {
        int k = 0;
        int m = 1;
        if(typeID != null)
            m = typeID;
        while (true) {
            String url1 = path + "c78da961-b241-4175-84c3-34c2b6b88f2f" + "/transactions" + "/filter" + "?page=" + k + "&typeId=" + m;
            Log.d("putRegularne", url1);
            try {
                URL url = new URL(url1);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                String rezultat = convertStreamToString(in);
                JSONObject jo = new JSONObject(rezultat);
                JSONArray transactions = jo.getJSONArray("transactions");

                if(typeID == null) {
                    if (transactions.length() == 0 && m == 2) break;
                    else if (transactions.length() == 0 && m == 1) {
                        m = 2;
                        k = 0;
                        continue;
                    }
                }
                else
                if(transactions.length() == 0) break;


                for (int i = 0; i < transactions.length(); i++) {
                    JSONObject transaction = transactions.getJSONObject(i);
                    Integer id = transaction.getInt("id");
                    String date;
                    Date date1;
                    if (!transaction.isNull("date")) {
                        date = transaction.getString("date");
                        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                        date1 = inputFormat.parse(date);
                    } else {
                        continue;
                    }
                    String title = transaction.getString("title");
                    if(!(title.length() > 3 && title.length() < 15))
                        continue;
                    Double amount = transaction.getDouble("amount");
                    Integer transactionTypeId;
                    if (!transaction.isNull("TransactionTypeId"))
                        transactionTypeId = transaction.getInt("TransactionTypeId");
                    else
                        continue;
                    String itemDescription;
                    if (transaction.isNull("itemDescription")) {
                        if(transactionTypeId != 2 && transactionTypeId != 4)
                            itemDescription = "";
                        else
                            itemDescription = null;
                    } else {
                        itemDescription = transaction.getString("itemDescription");
                        if(transactionTypeId == 2 && itemDescription != "")
                            continue;
                        else if(transactionTypeId == 4 && itemDescription != "")
                            continue;
                    }
                    Integer transactionInterval;
                    if (transaction.isNull("transactionInterval")) {
                        transactionInterval = 0;
                        if(transactionTypeId == 1)
                            continue;
                        else if(transactionTypeId == 2)
                            continue;
                    } else {
                        transactionInterval = transaction.getInt("transactionInterval");
                        if(transactionTypeId != 1 && transactionTypeId != 2 && transactionInterval != 0)
                            continue;
                    }
                    String endDate;
                    Date date2 = null;
                    if (!transaction.isNull("endDate")) {
                        endDate = transaction.getString("endDate");
                        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                        date2 = inputFormat.parse(endDate);
                        if(transactionTypeId != 1 && transactionTypeId != 2)
                            continue;
                    } else {
                        date2 = null;
                        if(transactionTypeId == 1)
                            continue;
                        if(transactionTypeId == 2)
                            continue;
                    }
                    String createdAt = transaction.getString("createdAt");
                    String updatedAt = transaction.getString("updatedAt");
                    Integer accountId = transaction.getInt("AccountId");

                    Calendar c1 = Calendar.getInstance();
                    c1.setTime(date1);
                    Calendar c2 = Calendar.getInstance();
                    c2.setTime(date2);
                    Calendar helpCalendar = Calendar.getInstance();
                    helpCalendar.set(year, month - 1, 1);
                    Calendar showedCalendar = Calendar.getInstance();
                    showedCalendar.set(year, month - 1, helpCalendar.getActualMaximum(Calendar.DATE));

                    transactionNow = new Transaction(id, date1, amount, title, null, itemDescription, transactionInterval, date2, accountId);
                    addType(transactionTypeId);
                    while(c1.before(c2) && c1.before(showedCalendar)) {
                        if(transactionInterval == 0)
                            break;
                        if(transactionNow.getTitle().length() > 3 && transactionNow.getTitle().length() < 15  && c1.get(Calendar.MONTH) == month - 1) {
                            transactionArrayList.add(transactionNow);
                            Log.d("date",String.valueOf(c1.get(Calendar.DATE)) + transactionNow.getTitle());
                        }
                        c1.add(Calendar.DATE, transactionInterval);
                    }
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
                Log.d("MalformedURLException", "e");
                break;
            } catch (IOException e) {
                e.printStackTrace();
                Log.d("IOException", "e");
                break;
            } catch (JSONException e) {
                e.printStackTrace();
                Log.d("JSONException", "e");
                break;
            } catch (ParseException e) {
                Log.i("1", "ne valja date formatter");
                e.printStackTrace();
                break;
            }
            k++;
        }
    }

        public void addType(Integer... params) {
            Integer typeID = params[0];
            String url1 = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com/transactionTypes";
            try {
                URL url = new URL(url1);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                String rezultat = convertStreamToString(in);
                JSONObject jo = new JSONObject(rezultat);
                Integer count = jo.getInt("count");
                JSONArray types = jo.getJSONArray("rows");
                for (int i = 0; i < count; i++) {
                    JSONObject type = types.getJSONObject(i);
                    if (typeID.equals(type.getInt("id"))) {
                        if (type.getString("name").equals("Regular payment")) {
                            transactionNow.setType(Type.REGULARPAYMENT);
                            transactionNow.setAmount(transactionNow.getAmount() * (-1));
                        } else if (type.getString("name").equals("Individual payment")) {
                            transactionNow.setType(Type.INDIVIDUALPAYMENT);
                            transactionNow.setAmount(transactionNow.getAmount() * (-1));
                        } else if (type.getString("name").equals("Individual income"))
                            transactionNow.setType(Type.INDIVIDUALINCOME);
                        else if (type.getString("name").equals("Regular income")) {
                            transactionNow.setType(Type.REGULARINCOME);
                        }
                        else if (type.getString("name").equals("Purchase")) {
                            transactionNow.setType(Type.PURCHASE);
                            transactionNow.setAmount(transactionNow.getAmount() * (-1));
                        }
                    }
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    public String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new
                InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
        } finally {
            try {
                is.close();
            } catch (IOException e) {
            }
        }
        return sb.toString();
    }
}
