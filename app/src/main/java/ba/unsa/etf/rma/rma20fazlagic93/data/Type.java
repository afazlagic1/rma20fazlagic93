package ba.unsa.etf.rma.rma20fazlagic93.data;

public enum Type {
    INDIVIDUALPAYMENT,
    REGULARPAYMENT,
    PURCHASE,
    INDIVIDUALINCOME,
    REGULARINCOME
}
