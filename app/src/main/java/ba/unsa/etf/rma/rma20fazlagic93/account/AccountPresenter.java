package ba.unsa.etf.rma.rma20fazlagic93.account;

import android.content.Context;

import ba.unsa.etf.rma.rma20fazlagic93.data.Account;
import ba.unsa.etf.rma.rma20fazlagic93.data.TransactionsModel;
import ba.unsa.etf.rma.rma20fazlagic93.util.ConnectivityBroadcastReceiver;

public class AccountPresenter implements IAccountPresenter, AccountInteractor.onGetAccountInfoDone, AccountInteractor.onPostAccountDone {
    private Context context;
    private IAccountView view;
    private IAccountInteractor accountInteractor = new AccountInteractor(this);

    public AccountPresenter() {
    }

    @Override
    public void onGetBudgetDone(Double info) {
        if(view != null) {
            if (ConnectivityBroadcastReceiver.connection && info != null)
                if (info != null)
                    view.setBudgetView(info);
                else if (TransactionsModel.budgetWas != null)
                    view.setBudgetView(TransactionsModel.budgetWas);
        }
    }

    @Override
    public void onGetMonthLimitDone(Double info) {
        if(view != null) {
            if (ConnectivityBroadcastReceiver.connection && info != null)
                view.setMonthLimitView(info);
            else if (TransactionsModel.monthLimitWas != null)
                view.setMonthLimitView(TransactionsModel.monthLimitWas);
        }
    }

    @Override
    public void onGetTotalLimitDone(Double info) {
        if(view != null) {
            if (ConnectivityBroadcastReceiver.connection && info != null)
                view.setTotalLimitView(info);
            else if (TransactionsModel.totalLimitWas != null)
                view.setTotalLimitView(TransactionsModel.totalLimitWas);
        }
    }

    public AccountPresenter(Context context, IAccountView view) {
        this.context = context;
        this.view = view;
    }

    private IAccountInteractor getAccountInteractor() {
        return accountInteractor;
    }

    @Override
    public double getMonthLimit() {
        if(ConnectivityBroadcastReceiver.connection)
            return getAccountInteractor().getMonthLimit();
        if(TransactionsModel.monthLimitWas != null)
            return TransactionsModel.monthLimitWas;
        return getAccountInteractor().getMonthLimit();
    }

    @Override
    public double getTotalLimit() {
        if(ConnectivityBroadcastReceiver.connection)
            return getAccountInteractor().getTotalLimit();
        if(TransactionsModel.totalLimitWas != null)
            return TransactionsModel.totalLimitWas;
        return getAccountInteractor().getTotalLimit();
    }

    @Override
    public double getBudget() {
        if(ConnectivityBroadcastReceiver.connection)
            return getAccountInteractor().getBudget();
        if(TransactionsModel.budgetWas != null)
            return TransactionsModel.budgetWas;
        return getAccountInteractor().getBudget();
    }

    @Override
    public void setMonthLimit(double x, Context context) {
        getAccountInteractor().setMonthLimit(x, context.getApplicationContext());
    }

    @Override
    public void setTotalLimit(double x, Context context) {
        getAccountInteractor().setTotalLimit(x, context.getApplicationContext());
    }

    @Override
    public void setBudget(double x, Context context) {
        getAccountInteractor().setBudget(x, context.getApplicationContext());
    }

    @Override
    public void setMonthLimitAndSetTotalLimit(double monthLimit, double totalLimit, Context context) {
        getAccountInteractor().setMonthLimitAndSetTotalLimit(this, monthLimit, totalLimit, context.getApplicationContext());
    }

    @Override
    public Account getAccountFromDB(Context applicationContext) {
        return getAccountInteractor().getAccountFromDB(applicationContext.getApplicationContext());
    }

    @Override
    public void onPostInfoChanged(Double monthLimit, Double totalLimit) {
        if(view != null) {
            if (ConnectivityBroadcastReceiver.connection && monthLimit != null && totalLimit != null) {
                view.setMonthLimitView(monthLimit);
                view.setTotalLimitView(totalLimit);
            } else if (TransactionsModel.totalLimitWas != null && TransactionsModel.monthLimitWas != null) {
                view.setMonthLimitView(TransactionsModel.monthLimitWas);
                view.setTotalLimitView(TransactionsModel.totalLimitWas);
            }
        }
    }

}
