package ba.unsa.etf.rma.rma20fazlagic93.data;

import java.io.Serializable;
import java.util.Date;

public class Transaction implements Serializable {  //implementira Serializable kako bi mogao objekat biti proslijeđen kao intent
    private Integer id;
    private Date date;
    private double amount;
    private String title; //duži od 3 i kraći od 15 znakova
    private Type type;
    private String itemDescription; //vjerovatno string, null za INCOME transakcije
    private int transactionInterval; //samo za REGULARINCOME i REGULARPAYMENT
    private Date endDate; //samo za REGULAR transakcije
    private Integer accountId;
    private int internalId = -2;

    public Transaction(Integer id, Date date, double amount, String title, Type type, String itemDescription, int transactionInterval, Date endDate, Integer accountId) {
        if(title.length() > 15) throw new IllegalArgumentException("Duzi naziv od 15 znakova!");
        if(title.length() < 3) throw new IllegalArgumentException("Kraći naziv od 3 znaka!");
        this.date = date;
        this.amount = amount;
        this.title = title;
        this.type = type;
        this.itemDescription = itemDescription;
        this.transactionInterval = transactionInterval;
        this.endDate = endDate;
        this.id = id;
        this.accountId = accountId;
    }

    public Transaction(int id, Date date, double amount, String title, Type type, String itemDescription, int transactionInterval, Date endDate, Integer accountId, int internalId) {
        if(title.length() > 15) throw new IllegalArgumentException("Duzi naziv od 15 znakova!");
        if(title.length() < 3) throw new IllegalArgumentException("Kraći naziv od 3 znaka!");
        this.id = id;
        this.date = date;
        this.amount = amount;
        this.title = title;
        this.type = type;
        this.itemDescription = itemDescription;
        this.transactionInterval = transactionInterval;
        this.endDate = endDate;
        this.accountId = accountId;
        this.internalId = internalId;
    }

    public int getInternalId() {
        return internalId;
    }

    public void setInternalId(int internalId) {
        this.internalId = internalId;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public double getAmount() {
        return amount;
    }

    public String getTitle() {
        return title;
    }

    public Type getType() {
        return type;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public int getTransactionInterval() {
        return transactionInterval;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public void setTransactionInterval(int transactionInterval) {
        this.transactionInterval = transactionInterval;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
