package ba.unsa.etf.rma.rma20fazlagic93.data;

public class Account {
    public double budget = 2000000.00;
    public double totalLimit = 1000000.00;
    public double monthLimit = 10000.00;

    public double getBudget() {
        return budget;
    }

    public void setBudget(double budget) {
        this.budget = budget;
    }

    public double getTotalLimit() {
        return totalLimit;
    }

    public void setTotalLimit(double totalLimit) {
        this.totalLimit = totalLimit;
    }

    public double getMonthLimit() {
        return monthLimit;
    }

    public void setMonthLimit(double monthLimit) {
        this.monthLimit = monthLimit;
    }
}
