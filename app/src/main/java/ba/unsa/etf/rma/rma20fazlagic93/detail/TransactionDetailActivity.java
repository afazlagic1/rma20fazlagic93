package ba.unsa.etf.rma.rma20fazlagic93.detail;
//nepotrebna klasa u ovoj spirali
/*
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import ba.unsa.etf.rma.rma20fazlagic93.list.FilterBySpinnerAdapter;
import ba.unsa.etf.rma.rma20fazlagic93.R;
import ba.unsa.etf.rma.rma20fazlagic93.data.Transaction;
import ba.unsa.etf.rma.rma20fazlagic93.data.TransactionsModel;
import ba.unsa.etf.rma.rma20fazlagic93.data.Type;

public class TransactionDetailActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    private ImageView detailImage;
    private EditText detailTitle;
    private Spinner detailType;
    private EditText detailAmount;
    private TextView detailDate;
    private TextView detailEndDate;
    private EditText detailInterval;
    private EditText detailItemDescription;
    private Button btnSave;
    private Button btnDelete;

    private ArrayList<Type> types2;
    private FilterBySpinnerAdapter filterBySpinnerAdapter;

    private Long s1;
    private Long s2;
    private boolean newTransaction = false;
    private int size;

    private DatePickerDialog datePickerDialog = null;
    private DatePickerDialog dateEndPickerDialog = null;
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMMM, YYYY, dd");

    private Transaction transaction;
    private Type type;
    private String title;
    private String itemDescription;
    private Double amount;
    private Integer transactionInterval;
    private Double monthLimit, totalLimit, budget, totalMonthAmount;
    private Date newDate, newEndDate;
    private Type newType;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.transaction_detail_activity);

        detailAmount = findViewById(R.id.detailAmount);
        detailDate = findViewById(R.id.viewDetailDate);
        detailEndDate = findViewById(R.id.viewDetailEndDate);
        detailImage = findViewById(R.id.detailImage);
        detailInterval = findViewById(R.id.detailInterval);
        detailItemDescription = findViewById(R.id.detailItemDescription);
        detailTitle = findViewById(R.id.detailTitle);
        detailType = findViewById(R.id.detailType);
        btnDelete = findViewById(R.id.btnDelete);
        btnSave = findViewById(R.id.btnSave);

        int pos = 0;
        Intent incomingIntent = getIntent();
        s1 = incomingIntent.getLongExtra("date", -1);
        amount = incomingIntent.getDoubleExtra("amount", 0);
        title = incomingIntent.getStringExtra("title");
        if(title.equals("Title"))
            newTransaction = true;
        type = (Type) incomingIntent.getSerializableExtra("type");
        itemDescription = incomingIntent.getStringExtra("itemDescription");
        transactionInterval = incomingIntent.getIntExtra("transactionInterval", 0);
        s2 = incomingIntent.getLongExtra("endDate", -1);
        totalLimit = incomingIntent.getDoubleExtra("totalLimit", 0);
        monthLimit = incomingIntent.getDoubleExtra("monthLimit", 0);
        budget = incomingIntent.getDoubleExtra("budget", 0);
        totalMonthAmount = incomingIntent.getDoubleExtra("totalMonthAmount", 0);
        transaction = (Transaction) incomingIntent.getSerializableExtra("transaction");
        size = incomingIntent.getIntExtra("size", 0);
        newDate = new Date(s1);
        newEndDate = new Date(s2);
        newType = type;

        if(type != null)
        if(type.equals(Type.INDIVIDUALINCOME)) {
            detailImage.setImageResource(R.drawable.individual_income);
            pos = 0;
        }
        else if(type.equals(Type.INDIVIDUALPAYMENT)) {
            detailImage.setImageResource(R.drawable.individual_payment);
            pos = 1;
        }
        else if(type.equals(Type.REGULARINCOME)) {
            detailImage.setImageResource(R.drawable.regular_income);
            pos = 2;
        }
        else if(type.equals(Type.REGULARPAYMENT)) {
            detailImage.setImageResource(R.drawable.regular_payment);
            pos = 3;
        }
        else if(type.equals(Type.PURCHASE)) {
            detailImage.setImageResource(R.drawable.purchase_);
            pos = 4;
        }

        if(type != null && (type.equals(Type.INDIVIDUALINCOME) || type.equals(Type.REGULARINCOME)))
            detailItemDescription.setEnabled(false);
        detailAmount.setText(String.valueOf(amount));
        detailTitle.setText(title);
        detailInterval.setText(String.valueOf(transactionInterval));
        if(type != null && !(type.equals(Type.REGULARPAYMENT) || type.equals(Type.REGULARINCOME))) {
            detailInterval.setEnabled(false);
        }
        detailItemDescription.setText(itemDescription);
        initListFilterBy();

        filterBySpinnerAdapter = new FilterBySpinnerAdapter(this, types2);
        detailType.setAdapter(filterBySpinnerAdapter);
        detailType.setSelection(pos);

        setColors();

        //listeneri
        detailDate.setOnClickListener(dateListener());
        detailEndDate.setOnClickListener(endDateListener());
        btnSave.setOnClickListener(saveListener());
        if(!newTransaction)
            btnDelete.setOnClickListener(deleteListener());
        detailType.setOnItemSelectedListener(typeChangedListener());
        detailTitle.addTextChangedListener(titleChangedListener());
        if(detailItemDescription.isEnabled())
            detailItemDescription.addTextChangedListener(detailItemDescriptionChangedListener());
        if(detailInterval.isEnabled())
            detailInterval.addTextChangedListener(detailIntervalChangedListener());
        detailAmount.addTextChangedListener(detailAmountChangedListener());
    }

    private TextWatcher detailAmountChangedListener() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!Double.valueOf(s.toString()).equals(amount))
                    detailAmount.setBackgroundColor(getResources().getColor(R.color.colorGreen));
                else
                    detailAmount.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };
    }

    private TextWatcher detailIntervalChangedListener() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!s.toString().equals(transactionInterval))
                    detailInterval.setBackgroundColor(getResources().getColor(R.color.colorGreen));
                else
                    detailInterval.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };
    }

    private TextWatcher detailItemDescriptionChangedListener() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!s.toString().equals(itemDescription))
                    detailItemDescription.setBackgroundColor(getResources().getColor(R.color.colorGreen));
                else
                    detailItemDescription.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };
    }

    private TextWatcher titleChangedListener() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!s.toString().equals(title))
                    detailTitle.setBackgroundColor(getResources().getColor(R.color.colorGreen));
                else
                    detailTitle.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };
    }

    private AdapterView.OnItemSelectedListener typeChangedListener() {
        return new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(!type.equals(parent.getItemAtPosition(position))) {
                    detailType.setBackgroundColor(getResources().getColor(R.color.colorGreen));
                }
                else
                    detailType.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                newType = (Type) parent.getItemAtPosition(position);
                if(newType.equals(Type.INDIVIDUALINCOME) || newType.equals(Type.REGULARINCOME)) {
                    detailItemDescription.setText("");
                    detailItemDescription.setEnabled(false);
                }
                else
                    detailItemDescription.setEnabled(true);
                if(!(newType.equals(Type.REGULARPAYMENT) || newType.equals(Type.REGULARINCOME))) {
                    detailInterval.setText("");
                    detailInterval.setEnabled(false);
                }
                else {
                    detailInterval.setEnabled(true);
                }
                if((newType.equals(Type.REGULARPAYMENT) || newType.equals(Type.REGULARINCOME)) && newEndDate.getTime() == -1)
                    newEndDate = new Date();
                else if((newType.equals(Type.PURCHASE) || newType.equals(Type.INDIVIDUALPAYMENT) || newType.equals(Type.INDIVIDUALINCOME)) && newEndDate.getTime() != -1)
                    newEndDate.setTime(-1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        };
    }

    private void initListFilterBy() {
        types2 = new ArrayList<>();
        types2.add(Type.INDIVIDUALINCOME);
        types2.add(Type.INDIVIDUALPAYMENT);
        types2.add(Type.REGULARINCOME);
        types2.add(Type.REGULARPAYMENT);
        types2.add(Type.PURCHASE);
    }

    private View.OnClickListener dateListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog(newDate.getTime());
            }
        };
    }

    private View.OnClickListener endDateListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDateEndPickerDialog(newEndDate.getTime());
            }
        };
    }

    private void showDatePickerDialog(Long m) {
        Calendar calendar = Calendar.getInstance();
        if(!(String.valueOf(m).equals("-1"))) {
        Date date = new Date(m);
        calendar.setTime(date);
        datePickerDialog = new DatePickerDialog(this, (DatePickerDialog.OnDateSetListener) this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
        }
    }
    private void showDateEndPickerDialog(Long m) {
        Calendar calendar = Calendar.getInstance();
        if(!(String.valueOf(m).equals("-1"))) {
            Date date = new Date(m);
            calendar.setTime(date);
            dateEndPickerDialog = new DatePickerDialog(this, (DatePickerDialog.OnDateSetListener) this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
            dateEndPickerDialog.show();
        }
    }

    private View.OnClickListener deleteListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alert = new AlertDialog.Builder(TransactionDetailActivity.this);
                alert.setTitle("Delete alert!");
                alert.setMessage("Are you sure you want to delete this transaction?");
                alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent answerIntent = new Intent();
                        answerIntent.putExtra("obrisiOvuTransakciju", transaction);
                        answerIntent.putExtra("izbrisiIznos", amount);
                        setResult(RESULT_OK, answerIntent);
                        finish();
                    }
                });
                alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alert.create().show();
            }
        };
    }

    private View.OnClickListener saveListener() {
        return new View.OnClickListener() {
            boolean change = true;
            @Override
            public void onClick(View v) {
                double newAmount;
                if(!detailAmount.getText().toString().isEmpty())
                    try {
                        newAmount = Double.parseDouble(detailAmount.getText().toString());
                        if( (int) (newAmount + totalLimit) < 0 || (int) (newAmount + budget) < 0 || (int) (newAmount + monthLimit + totalMonthAmount) < 0) {
                            AlertDialog.Builder alert = new AlertDialog.Builder(TransactionDetailActivity.this);
                            alert.setTitle("Limit alert!");
                            alert.setMessage("Are you sure you want to save considering the limit?");
                            alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    change = true;
                                }
                            });
                            alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    change = false;
                                }
                            });
                            alert.create().show();
                        }
                        if(change) {
                        setColors();
                        for(int i = 0; i < size; i++)
                            if(TransactionsModel.transactions.get(i).getTitle().equals(transaction.getTitle())) {
                                if(detailTitle.getText().toString().length() <= 15 && detailTitle.getText().toString().length() >=3 && !detailTitle.getText().toString().equals("Title")) {
                                    if( ( (newType.equals(Type.INDIVIDUALINCOME) || newType.equals(Type.REGULARINCOME)) && newAmount >= 0) || ( (newType.equals(Type.REGULARPAYMENT) || newType.equals(Type.INDIVIDUALPAYMENT) || newType.equals(Type.PURCHASE)) && newAmount <= 0) ) {
                                        if(newType.equals(Type.INDIVIDUALINCOME) || newType.equals(Type.INDIVIDUALPAYMENT) || newType.equals(Type.PURCHASE)
                                                || ( newEndDate != null && (newType.equals(Type.REGULARINCOME) || newType.equals(Type.REGULARPAYMENT)) && newDate.before(newEndDate)) ) {
                                        if(newType.equals(Type.REGULARPAYMENT) || newType.equals(Type.REGULARINCOME))
                                            TransactionsModel.transactions.get(i).setEndDate(newEndDate);
                                    else
                                            TransactionsModel.transactions.get(i).setEndDate(null);
                                    TransactionsModel.transactions.get(i).setTitle(detailTitle.getText().toString());
                                    TransactionsModel.transactions.get(i).setAmount(newAmount);
                                    TransactionsModel.transactions.get(i).setDate(newDate);
                                    if(newType != null && !(newType.equals(Type.INDIVIDUALINCOME) || newType.equals(Type.REGULARINCOME)))
                                        TransactionsModel.transactions.get(i).setItemDescription(detailItemDescription.getText().toString());
                                    else
                                        TransactionsModel.transactions.get(i).setItemDescription(null);
                                    TransactionsModel.transactions.get(i).setType(newType);
                                    if(newType != null)
                                        if(newType.equals(Type.INDIVIDUALINCOME)) {
                                            detailImage.setImageResource(R.drawable.individual_income);
                                        }
                                        else if(newType.equals(Type.INDIVIDUALPAYMENT)) {
                                            detailImage.setImageResource(R.drawable.individual_payment);
                                        }
                                        else if(newType.equals(Type.REGULARINCOME)) {
                                            detailImage.setImageResource(R.drawable.regular_income);
                                        }
                                        else if(newType.equals(Type.REGULARPAYMENT)) {
                                            detailImage.setImageResource(R.drawable.regular_payment);
                                        }
                                        else if(newType.equals(Type.PURCHASE)) {
                                            detailImage.setImageResource(R.drawable.purchase_);
                                        }
                                    if(newType != null && (newType.equals(Type.REGULARPAYMENT) || newType.equals(Type.REGULARINCOME)))
                                        if(detailInterval.getText().toString() != "")
                                            TransactionsModel.transactions.get(i).setTransactionInterval(Integer.valueOf(detailInterval.getText().toString()));
                                        else
                                            TransactionsModel.transactions.get(i).setTransactionInterval(0);
                                    else
                                        TransactionsModel.transactions.get(i).setTransactionInterval(0);

                                    if(newTransaction == true) {
                                        Intent answerIntent = new Intent();
                                        answerIntent.putExtra("dodajOvuTransakciju", transaction);
                                        answerIntent.putExtra("uracunajIznos", newAmount);
                                        setResult(RESULT_OK, answerIntent);
                                        finish();
                                        }
                                    }
                                    else {
                                        Toast.makeText(getApplicationContext(), "Invalid dates!", Toast.LENGTH_LONG).show();
                                    }
                                }
                                    else {
                                        Toast.makeText(getApplicationContext(), "Invalid sign of amount for this type of transaction!", Toast.LENGTH_LONG).show();
                                    }
                                }
                                else {
                                    Toast.makeText(getApplicationContext(), "Title invalid!", Toast.LENGTH_LONG).show();
                                }
                            }
                        }
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
            }
        };
    }

    private void setColors() {
                detailAmount.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                detailDate.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                detailEndDate.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                detailType.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                detailInterval.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                detailItemDescription.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                detailTitle.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                detailTitle.setBackgroundColor(getResources().getColor(R.color.colorWhite));
}
    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, dayOfMonth);
        if(datePickerDialog != null) {
            newDate = calendar.getTime();
            String f = simpleDateFormat.format(newDate);
            String g = simpleDateFormat.format(transaction.getDate());
            if(!f.equals(g)) {
                detailDate.setBackgroundColor(getResources().getColor(R.color.colorGreen));
            }
            else
                detailDate.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            datePickerDialog = null;
        }
        if(dateEndPickerDialog != null) {
            newEndDate = calendar.getTime();
            if(transaction.getEndDate() != null) {
            String f = simpleDateFormat.format(newEndDate);
            String g = simpleDateFormat.format(transaction.getEndDate());
            if(!f.equals(g))
                detailEndDate.setBackgroundColor(getResources().getColor(R.color.colorGreen));
            else
                detailEndDate.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            }
            else
                detailEndDate.setBackgroundColor(getResources().getColor(R.color.colorGreen));
            dateEndPickerDialog = null;   
        }
    }
}
*/