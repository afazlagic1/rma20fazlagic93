package ba.unsa.etf.rma.rma20fazlagic93.account;

import android.content.Context;

import ba.unsa.etf.rma.rma20fazlagic93.data.Account;

public interface IAccountInteractor {
    double getMonthLimit();
    double getTotalLimit();
    double getBudget();
    void setMonthLimit(double x, Context context);
    void setTotalLimit(double x, Context context);
    void setBudget(double x, Context context);
    void setMonthLimitAndSetTotalLimit(AccountInteractor.onPostAccountDone callerPost, double monthLimit, double totalLimit, Context context);
    Account getAccountFromDB(Context applicationContext);
}
