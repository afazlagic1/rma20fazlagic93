package ba.unsa.etf.rma.rma20fazlagic93.list;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

import ba.unsa.etf.rma.rma20fazlagic93.R;
import ba.unsa.etf.rma.rma20fazlagic93.data.Type;

public class FilterBySpinnerAdapter extends ArrayAdapter<Type> {

    public FilterBySpinnerAdapter(@NonNull Context context, @NonNull ArrayList<Type> objects) {
        super(context, 0, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    private View initView(int position, View convertView, ViewGroup parent) {
        if(convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.filter_by_spinner_element, parent, false);
        }
        TextView textViewTransactionType = convertView.findViewById(R.id.textViewTransactionType);
        ImageView ikonica = convertView.findViewById(R.id.ikonica);
        Type tip = getItem(position);

        if(tip != null) {
            if(tip.equals(Type.PURCHASE)) {
                ikonica.setImageResource(R.drawable.purchase_);
                textViewTransactionType.setText("Purchase");
            }
            else if(tip.equals(Type.INDIVIDUALINCOME)) {
                ikonica.setImageResource(R.drawable.individual_income);
                textViewTransactionType.setText("Individual income");
            }
            else if(tip.equals(Type.REGULARINCOME)) {
                ikonica.setImageResource(R.drawable.regular_income);
                textViewTransactionType.setText("Regular income");
            }
            else if(tip.equals(Type.INDIVIDUALPAYMENT)) {
                ikonica.setImageResource(R.drawable.individual_payment);
                textViewTransactionType.setText("Individual payment");
            }
            else if(tip.equals(Type.REGULARPAYMENT)) {
                ikonica.setImageResource(R.drawable.regular_payment);
                textViewTransactionType.setText("Regular payment");
            }
        }
        else
            textViewTransactionType.setText("All");

        return convertView;
    }
}
