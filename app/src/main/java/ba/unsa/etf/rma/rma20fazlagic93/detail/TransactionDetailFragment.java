package ba.unsa.etf.rma.rma20fazlagic93.detail;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import ba.unsa.etf.rma.rma20fazlagic93.data.TransactionsModel;
import ba.unsa.etf.rma.rma20fazlagic93.util.ConnectivityBroadcastReceiver;
import ba.unsa.etf.rma.rma20fazlagic93.R;
import ba.unsa.etf.rma.rma20fazlagic93.data.Transaction;
import ba.unsa.etf.rma.rma20fazlagic93.data.Type;
import ba.unsa.etf.rma.rma20fazlagic93.list.FilterBySpinnerAdapter;
import ba.unsa.etf.rma.rma20fazlagic93.list.ITransactionListPresenter;
import ba.unsa.etf.rma.rma20fazlagic93.list.TransactionListPresenter;

public class TransactionDetailFragment extends Fragment implements DatePickerDialog.OnDateSetListener {

    private ImageView detailImage;
    private EditText detailTitle;
    private Spinner detailType;
    private EditText detailAmount;
    private TextView detailDate;
    private TextView detailEndDate;
    private EditText detailInterval;
    private EditText detailItemDescription;
    private Button btnSave;
    private Button btnDelete;
    private EditText izmjenaText;

    private ITransactionListPresenter transactionListPresenter = new TransactionListPresenter(getActivity(), this, getActivity());

    private ArrayList<Type> types2;
    private FilterBySpinnerAdapter filterBySpinnerAdapter;

    private OnDelete onDeleteClick;
    public interface OnDelete {
        void onDelete(Transaction transaction, double amount);
        void onDeleteOrUndoDelete(Transaction transaction, double amount, boolean b);
    }
    private OnSave onSaveClick;
    public interface OnSave {
        void onSave(Transaction transaction, boolean newTransaction, double formerAmount, Type formerType, Date formerDate, Date formerEndDate, int formerInterval);
    }

    private Long s1;
    private Long s2;
    private boolean newTransaction = false;

    private DatePickerDialog datePickerDialog = null;
    private DatePickerDialog dateEndPickerDialog = null;
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMMM, YYYY, dd");

    private Transaction transaction;
    private Type type;
    private String title;
    private String itemDescription;
    private Double amount;
    private Integer transactionInterval;
    private Double monthLimit, totalLimit, budget, totalMonthAmount;
    private Date newDate, newEndDate;
    private Type newType;

    private double formerAmount;
    private Type formerType;
    private Date formerDate;
    private Date formerEndDate;
    private int formerInterval;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail, container, false);

        Toast.makeText(getActivity(), "loading... (please wait)", Toast.LENGTH_LONG).show();

        onDeleteClick = (OnDelete) getActivity();
        onSaveClick = (OnSave) getActivity();

        detailAmount = view.findViewById(R.id.detailAmount);
        detailDate = view.findViewById(R.id.viewDetailDate);
        detailEndDate = view.findViewById(R.id.viewDetailEndDate);
        detailImage = view.findViewById(R.id.detailImage);
        detailInterval = view.findViewById(R.id.detailInterval);
        detailItemDescription = view.findViewById(R.id.detailItemDescription);
        detailTitle = view.findViewById(R.id.detailTitle);
        detailType = view.findViewById(R.id.detailType);
        btnDelete = view.findViewById(R.id.btnDelete);
        btnSave = view.findViewById(R.id.btnSave);
        izmjenaText = view.findViewById(R.id.izmjenaText);

        if(getArguments() != null && getArguments().containsKey("transaction")) {
            if(getArguments().containsKey("totalLimit"))
                totalLimit = getArguments().getDouble("totalLimit");
            else
                totalLimit = 0.;
            if(getArguments().containsKey("monthLimit"))
                monthLimit = getArguments().getDouble("monthLimit");
            else
                monthLimit = 0.;
            if(getArguments().containsKey("totalMonthAmount"))
                totalMonthAmount = getArguments().getDouble("totalMonthAmount");
            else
                totalMonthAmount = 0.;
            if(getArguments().containsKey("budget"))
                budget = getArguments().getDouble("budget");
            else
                budget = 0.;

            int pos = 0;
            transaction = (Transaction) getArguments().getSerializable("transaction");
            amount = transaction.getAmount();
            formerAmount = amount;
            title = transaction.getTitle();
            if(transaction.getTitle().equals("Title"))
                newTransaction = true;
            if(wasDeleted(transaction) && ConnectivityBroadcastReceiver.connection == false) {
                izmjenaText.setText("Offline brisanje");
                btnDelete.setText("Undo");
            }
            else {
                if (ConnectivityBroadcastReceiver.connection == false)
                    if (newTransaction == true)
                        izmjenaText.setText("Offline dodavanje");
                    else
                        izmjenaText.setText("Offline izmjena");
            }
            if(transaction.getEndDate() != null)
                s2 = transaction.getEndDate().getTime();
            else
                s2 = (long) -1;
            if(transaction.getDate() != null)
                s1 = transaction.getDate().getTime();
            else
                s1 = (long) -1;
            newDate = new Date(s1);
            newEndDate = new Date(s2);
            formerDate = new Date(s1);
            formerEndDate = new Date(s2);
            type = transaction.getType();
            newType = type;
            formerType = type;
            if(transaction.getItemDescription() != null)
                itemDescription = transaction.getItemDescription();
            else
                itemDescription = "";
            transactionInterval = transaction.getTransactionInterval();
            formerInterval = transaction.getTransactionInterval();

            if(type != null)
                if(type.equals(Type.INDIVIDUALINCOME)) {
                    detailImage.setImageResource(R.drawable.individual_income);
                    pos = 0;
                }
                else if(type.equals(Type.INDIVIDUALPAYMENT)) {
                    detailImage.setImageResource(R.drawable.individual_payment);
                    pos = 1;
                }
                else if(type.equals(Type.REGULARINCOME)) {
                    detailImage.setImageResource(R.drawable.regular_income);
                    pos = 2;
                }
                else if(type.equals(Type.REGULARPAYMENT)) {
                    detailImage.setImageResource(R.drawable.regular_payment);
                    pos = 3;
                }
                else if(type.equals(Type.PURCHASE)) {
                    detailImage.setImageResource(R.drawable.purchase_);
                    pos = 4;
                }

            detailAmount.setText(String.valueOf(amount));
            detailTitle.setText(title);
            detailInterval.setText(String.valueOf(transactionInterval));
            detailItemDescription.setText(itemDescription);
            initListFilterBy();

            filterBySpinnerAdapter = new FilterBySpinnerAdapter(getActivity(), types2);
            detailType.setAdapter(filterBySpinnerAdapter);
            detailType.setSelection(pos);

            setColors();

            //listeneri
            detailDate.setOnClickListener(dateListener());
            detailEndDate.setOnClickListener(endDateListener());
            btnSave.setOnClickListener(saveListener());
            if(!newTransaction)
                btnDelete.setOnClickListener(deleteListener());
            detailType.setOnItemSelectedListener(typeChangedListener());
            detailTitle.addTextChangedListener(titleChangedListener());
            detailItemDescription.addTextChangedListener(detailItemDescriptionChangedListener());
            detailInterval.addTextChangedListener(detailIntervalChangedListener());
            detailAmount.addTextChangedListener(detailAmountChangedListener());
        }

        return view;
    }

    private boolean wasDeleted(Transaction transaction) {
        Log.d("internall", String.valueOf(transaction.getInternalId()));
        Log.d("notinternall", String.valueOf(transaction.getId()));
        if(transaction.getId() != -1 && transaction.getId() != null && transaction.getId() != 0) {
            for (int i = 0; i < TransactionsModel.transactionsDeleted.size(); i++)
                if (transaction.getId().equals(TransactionsModel.transactionsDeleted.get(i).getId()))
                    return true;
        }
        else {
                    for (int j = 0; j < TransactionsModel.transactionsDeleted.size(); j++)
                        if (transaction.getInternalId() == TransactionsModel.transactionsDeleted.get(j).getInternalId())
                            return true;
                }
        return false;
    }
    private void removeId(Transaction transaction) {
        if(transaction.getId() != -1 && transaction.getId() != null && transaction.getId() != 0) {
            for (int i = 0; i < TransactionsModel.transactionsDeleted.size(); i++)
                if (transaction.getId().equals(TransactionsModel.transactionsDeleted.get(i).getId()))
                    TransactionsModel.transactionsDeleted.remove(TransactionsModel.transactionsDeleted.get(i));
        }
        else {
            for (int j = 0; j < TransactionsModel.transactionsDeleted.size(); j++) {
                Log.d("internalispravaniline", String.valueOf(TransactionsModel.transactionsDeleted.get(j).getInternalId()));
                if (transaction.getInternalId() == TransactionsModel.transactionsDeleted.get(j).getInternalId())
                    TransactionsModel.transactionsDeleted.remove(TransactionsModel.transactionsDeleted.get(j));
            }
        }
    }

    private TextWatcher detailAmountChangedListener() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!Double.valueOf(s.toString()).equals(amount))
                    detailAmount.setBackgroundColor(getResources().getColor(R.color.colorGreen));
                else
                    detailAmount.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };
    }

    private TextWatcher detailIntervalChangedListener() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!s.toString().equals(String.valueOf(transactionInterval)))
                    detailInterval.setBackgroundColor(getResources().getColor(R.color.colorGreen));
                else
                    detailInterval.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };
    }

    private TextWatcher detailItemDescriptionChangedListener() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!s.toString().equals(itemDescription))
                    detailItemDescription.setBackgroundColor(getResources().getColor(R.color.colorGreen));
                else
                    detailItemDescription.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };
    }

    private TextWatcher titleChangedListener() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!s.toString().equals(title))
                    detailTitle.setBackgroundColor(getResources().getColor(R.color.colorGreen));
                else
                    detailTitle.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };
    }

    private AdapterView.OnItemSelectedListener typeChangedListener() {
        return new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(!type.equals(parent.getItemAtPosition(position))) {
                    detailType.setBackgroundColor(getResources().getColor(R.color.colorGreen));
                }
                else
                    detailType.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                newType = (Type) parent.getItemAtPosition(position);
                if(newType.equals(Type.INDIVIDUALINCOME) || newType.equals(Type.REGULARINCOME)) {
                    detailItemDescription.setText("");
                    detailItemDescription.setEnabled(false);
                }
                else
                    detailItemDescription.setEnabled(true);
                if(!(newType.equals(Type.REGULARPAYMENT) || newType.equals(Type.REGULARINCOME))) {
                    detailInterval.setText("0");
                    transactionInterval = 0;
                    detailInterval.setEnabled(false);
                }
                else {
                    detailInterval.setEnabled(true);
                }
                if((newType.equals(Type.REGULARPAYMENT) || newType.equals(Type.REGULARINCOME)) && newEndDate.getTime() == -1)
                    newEndDate = new Date();
                else if((newType.equals(Type.PURCHASE) || newType.equals(Type.INDIVIDUALPAYMENT) || newType.equals(Type.INDIVIDUALINCOME)) && newEndDate.getTime() != -1)
                    newEndDate.setTime(-1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        };
    }

    private View.OnClickListener dateListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog(newDate.getTime());
            }
        };
    }

    private View.OnClickListener endDateListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDateEndPickerDialog(newEndDate.getTime());
            }
        };
    }

    private void showDatePickerDialog(Long m) {
        Calendar calendar = Calendar.getInstance();
        if(!(String.valueOf(m).equals("-1"))) {
            Date date = new Date(m);
            calendar.setTime(date);
            datePickerDialog = new DatePickerDialog(getActivity(), this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.show();
        }
    }
    private void showDateEndPickerDialog(Long m) {
        Calendar calendar = Calendar.getInstance();
        if(!(String.valueOf(m).equals("-1"))) {
            Date date = new Date(m);
            calendar.setTime(date);
            dateEndPickerDialog = new DatePickerDialog(getActivity(), this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
            dateEndPickerDialog.show();
        }
    }

    private View.OnClickListener deleteListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConnectivityBroadcastReceiver.connection == true) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setTitle("Delete alert!");
                    alert.setMessage("Are you sure you want to delete this transaction?");
                    alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            onDeleteClick.onDelete(transaction, amount);
                        }
                    });
                    alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    alert.create().show();
                }
                else {
                    if (btnDelete.getText().toString().equalsIgnoreCase("Delete")) {
                        TransactionsModel.transactionsDeleted.add(transaction);
                        izmjenaText.setText("Offline brisanje");
                        btnDelete.setText("Undo");
                        onDeleteClick.onDeleteOrUndoDelete(transaction, amount, false);
                    } else if (btnDelete.getText().toString().equalsIgnoreCase("Undo")) {
                        removeId(transaction);
                        izmjenaText.setText("Offline izmjena");
                        btnDelete.setText("Delete");
                        onDeleteClick.onDeleteOrUndoDelete(transaction, amount, true);
                    }
                }
            }
        };
    }

    private View.OnClickListener saveListener() {
        return new View.OnClickListener() {
            boolean change = true;
            @Override
            public void onClick(View v) {
                final double newAmount;
                if(!detailAmount.getText().toString().isEmpty())
                    try {
                        newAmount = Double.parseDouble(detailAmount.getText().toString());
                        if( (int) (newAmount + totalLimit) < 0 || (int) (newAmount + budget) < 0 || (int) (newAmount + monthLimit + totalMonthAmount) < 0) {
                            change = false;
                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle("Limit alert!");
                            alert.setMessage("Are you sure you want to save considering the limit?");
                            alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    changeOnSave(newAmount);
                                }
                            });
                            alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                            alert.create().show();
                        }
                        if(change) {
                            changeOnSave(newAmount);
                                }
                            }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
            }
        };
    }

    private void initListFilterBy() {
        types2 = new ArrayList<>();
        types2.add(Type.INDIVIDUALINCOME);
        types2.add(Type.INDIVIDUALPAYMENT);
        types2.add(Type.REGULARINCOME);
        types2.add(Type.REGULARPAYMENT);
        types2.add(Type.PURCHASE);
    }

    private void setColors() {
        detailAmount.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        detailDate.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        detailEndDate.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        detailType.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        detailInterval.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        detailItemDescription.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        detailTitle.setBackgroundColor(getResources().getColor(R.color.colorWhite));
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, dayOfMonth);
        if(datePickerDialog != null) {
            newDate = calendar.getTime();
            String f = simpleDateFormat.format(newDate);
            String g = simpleDateFormat.format(transaction.getDate());
            if(!f.equals(g)) {
                detailDate.setBackgroundColor(getResources().getColor(R.color.colorGreen));
            }
            else
                detailDate.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            datePickerDialog = null;
        }
        if(dateEndPickerDialog != null) {
            newEndDate = calendar.getTime();
            if(transaction.getEndDate() != null) {
                String f = simpleDateFormat.format(newEndDate);
                String g = simpleDateFormat.format(transaction.getEndDate());
                if(!f.equals(g))
                    detailEndDate.setBackgroundColor(getResources().getColor(R.color.colorGreen));
                else
                    detailEndDate.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            }
            else
                detailEndDate.setBackgroundColor(getResources().getColor(R.color.colorGreen));
            dateEndPickerDialog = null;
        }
    }

private void changeOnSave(double newAmount) {
    setColors();
    if(detailTitle.getText().toString().length() <= 15 && detailTitle.getText().toString().length() >=3 && !detailTitle.getText().toString().equals("Title")) {
        if( ( (newType.equals(Type.INDIVIDUALINCOME) || newType.equals(Type.REGULARINCOME)) && newAmount >= 0) || ( (newType.equals(Type.REGULARPAYMENT) || newType.equals(Type.INDIVIDUALPAYMENT) || newType.equals(Type.PURCHASE)) && newAmount <= 0) ) {
            if(newType.equals(Type.INDIVIDUALINCOME) || newType.equals(Type.INDIVIDUALPAYMENT) || newType.equals(Type.PURCHASE)
                    || ( newEndDate != null && (newType.equals(Type.REGULARINCOME) || newType.equals(Type.REGULARPAYMENT)) && newDate.before(newEndDate)) ) {
                if(newType.equals(Type.REGULARPAYMENT) || newType.equals(Type.REGULARINCOME))
                    transaction.setEndDate(newEndDate);
                else
                    transaction.setEndDate(null);
                transaction.setTitle(detailTitle.getText().toString());
                transaction.setAmount(newAmount);
                transaction.setDate(newDate);
                if(newType != null && !(newType.equals(Type.INDIVIDUALINCOME) || newType.equals(Type.REGULARINCOME)))
                    transaction.setItemDescription(detailItemDescription.getText().toString());
                else
                    transaction.setItemDescription(null);
                transaction.setType(newType);
                if(newType != null)
                    if(newType.equals(Type.INDIVIDUALINCOME)) {
                        detailImage.setImageResource(R.drawable.individual_income);
                    }
                    else if(newType.equals(Type.INDIVIDUALPAYMENT)) {
                        detailImage.setImageResource(R.drawable.individual_payment);
                    }
                    else if(newType.equals(Type.REGULARINCOME)) {
                        detailImage.setImageResource(R.drawable.regular_income);
                    }
                    else if(newType.equals(Type.REGULARPAYMENT)) {
                        detailImage.setImageResource(R.drawable.regular_payment);
                    }
                    else if(newType.equals(Type.PURCHASE)) {
                        detailImage.setImageResource(R.drawable.purchase_);
                    }
                if(newType != null && (newType.equals(Type.REGULARPAYMENT) || newType.equals(Type.REGULARINCOME)))
                    if(detailInterval.getText().toString() != "")
                        transaction.setTransactionInterval(Integer.valueOf(detailInterval.getText().toString()));
                    else
                        transaction.setTransactionInterval(0);
                else
                    transaction.setTransactionInterval(0);
                if(newTransaction == true) {
                    transactionListPresenter.addTransaction(transaction, getActivity());
                }
                onSaveClick.onSave(transaction, newTransaction, formerAmount, formerType, formerDate, formerEndDate, formerInterval);
            }
            else {
                Toast.makeText(getActivity(), "Invalid dates!", Toast.LENGTH_LONG).show();
            }
        }
        else {
            Toast.makeText(getActivity(), "Invalid sign of amount for this type of transaction!", Toast.LENGTH_LONG).show();
        }
    }
    else {
        Toast.makeText(getActivity(), "Title invalid!", Toast.LENGTH_LONG).show();
        }
    }
}
