package ba.unsa.etf.rma.rma20fazlagic93.account;

public interface IAccountView {
    void setBudgetView(Double budget);
    void setTotalLimitView(Double totalLimit);
    void setMonthLimitView(Double monthLimit);
}
