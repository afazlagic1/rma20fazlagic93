package ba.unsa.etf.rma.rma20fazlagic93.list;

import java.util.Date;

import ba.unsa.etf.rma.rma20fazlagic93.data.Transaction;
import ba.unsa.etf.rma.rma20fazlagic93.data.Type;

public interface IUpdateTransactionList {
    void updateTransaction(Transaction transaction, double formerAmount, Type formerType, Date formerDate, Date formerEndDate, int formerInterval);
}
