package ba.unsa.etf.rma.rma20fazlagic93.list;

import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;
import java.util.Calendar;

import ba.unsa.etf.rma.rma20fazlagic93.data.Transaction;

public interface ITransactionListView {
    void setTransactions(ArrayList<Transaction> transactions);
    void notifyTransactionListDataChanged();
    Calendar getViewDate();
    int getSelectedSortByItem();
    void setTransactionListAdapter(ArrayList<Transaction> filtriraneTransakcije);
    void setCursor(Cursor cursor);
    Context getContext();
}
