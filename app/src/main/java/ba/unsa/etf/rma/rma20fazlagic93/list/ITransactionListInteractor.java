package ba.unsa.etf.rma.rma20fazlagic93.list;

import android.content.Context;
import android.database.Cursor;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

import ba.unsa.etf.rma.rma20fazlagic93.data.Transaction;
import ba.unsa.etf.rma.rma20fazlagic93.data.Type;

public interface ITransactionListInteractor {
    void remove(int id, Transaction transaction, Context context);
    void add(Transaction t, Context context);
    void getTransactions(String what, String sort, String month, String year);
    void getTransactions(String what, String typeId, String sort, String month, String year);
    void updateTransaction(Transaction transaction, double formerAmount, Type formerType, Date formerDate, Date formerEndDate, int formerInterval, Context context);
    Cursor getTransactionCursor(Context applicationContext, String month, String year);
    Transaction getTransactionFromDB(int internalId, Context context) throws ParseException;
    ArrayList<Transaction> getTransactionsFromDB(Context context) throws ParseException;
    void deleteTransactionsFromDB(Context applicationContext);
    void undoDeleteTransactionDB(Transaction undoDeleteThisTransaction, Context applicationContext);
    ArrayList<Transaction> getDeletedTransactionsFromDB(Context applicationContext) throws ParseException;
    void deleteTransactionsDeletedFromDB(Context applicationContext);
}
