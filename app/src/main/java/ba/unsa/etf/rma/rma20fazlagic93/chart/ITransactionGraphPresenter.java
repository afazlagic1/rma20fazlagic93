package ba.unsa.etf.rma.rma20fazlagic93.chart;

public interface ITransactionGraphPresenter {
    /*double totalMonthPayment(int month, int year);
    double totalMonthIncome(int month, int year);
    double totalAmount(int month, int year);
    double totalDayAmount(int date, int month, int year);
    double totalDayIncome(int date, int month, int year);
    double totalDayPayment(int date, int month, int year);
    double totalWeekAmount(int date, int month, int year);
    double totalWeekPayment(int date, int month, int year);
    double totalWeekIncome(int date, int month, int year);*/
    void putChartMonth();
    void putChartDay();
    void putChartWeek();
}
