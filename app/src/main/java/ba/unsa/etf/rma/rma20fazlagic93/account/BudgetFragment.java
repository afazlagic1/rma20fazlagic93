package ba.unsa.etf.rma.rma20fazlagic93.account;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import ba.unsa.etf.rma.rma20fazlagic93.R;

public class BudgetFragment extends Fragment implements IAccountView {

    private EditText budget;
    private EditText totalLimit;
    private EditText monthLimit;
    private Button btnSave;

    private IAccountPresenter accountPresenter = new AccountPresenter(getActivity(), this);

    public IAccountPresenter getAccountPresenter() {
        return accountPresenter;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_budget, container, false);

        budget= view.findViewById(R.id.budgetV);
        totalLimit = view.findViewById(R.id.totalLimitV);
        monthLimit = view.findViewById(R.id.monthLimitV);
        btnSave = view.findViewById(R.id.btnSaveAccount);

        budget.setText(String.valueOf(getAccountPresenter().getBudget()));
        totalLimit.setText(String.valueOf(getAccountPresenter().getTotalLimit()));
        monthLimit.setText(String.valueOf(getAccountPresenter().getMonthLimit()));

        budget.setEnabled(false);

        btnSave.setOnClickListener(saveListener());
        return view;
    }

    private View.OnClickListener saveListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getAccountPresenter().setMonthLimitAndSetTotalLimit(Double.parseDouble(monthLimit.getText().toString()), Double.parseDouble(totalLimit.getText().toString()), getActivity());
            }
        };
    }

    @Override
    public void setBudgetView(Double budget) {
        this.budget.setText(String.valueOf(budget));
    }

    @Override
    public void setMonthLimitView(Double monthLimit) {
        this.monthLimit.setText(String.valueOf(monthLimit));
    }

    @Override
    public void setTotalLimitView(Double totalLimit) {
        Log.d("totalLimit", String.valueOf(totalLimit));
        this.totalLimit.setText(String.valueOf(totalLimit));
    }
}
