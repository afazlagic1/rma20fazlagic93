package ba.unsa.etf.rma.rma20fazlagic93.list;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

import androidx.fragment.app.FragmentActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import ba.unsa.etf.rma.rma20fazlagic93.util.ConnectivityBroadcastReceiver;
import ba.unsa.etf.rma.rma20fazlagic93.data.Transaction;
import ba.unsa.etf.rma.rma20fazlagic93.data.TransactionsModel;
import ba.unsa.etf.rma.rma20fazlagic93.data.Type;
import ba.unsa.etf.rma.rma20fazlagic93.detail.TransactionDetailFragment;

public class TransactionListPresenter implements ITransactionListPresenter, TransactionListInteractor.onTransactionLoadDone {

    private ITransactionListView view;
    private IUpdateTransactionList view2;
    private ITransactionListInteractor transactionListInteractor = new TransactionListInteractor(this);
    private ArrayList<Transaction> arrayList;
    private Context context;

    public TransactionListPresenter(FragmentActivity activity, TransactionDetailFragment transactionDetailFragment, Context context) {
        this.context = context;
        if(context == null)
            Log.d("konstruktor", "4");
    }

    public TransactionListPresenter() {

    }

    @Override
    public void onDone(ArrayList<Transaction> transactions) {
        if(transactions == null && ConnectivityBroadcastReceiver.connection == false) {
            if(TransactionsModel.lastLoadedTransactions.size() == 0) {
                Calendar calendar = view.getViewDate();
                @SuppressLint("SimpleDateFormat") SimpleDateFormat mjesec = new SimpleDateFormat("MM");
                @SuppressLint("SimpleDateFormat") SimpleDateFormat godina = new SimpleDateFormat("YYYY");
                String year = godina.format(calendar.getTime());
                String month = mjesec.format(calendar.getTime());
                view.setCursor(transactionListInteractor.getTransactionCursor(view.getContext().getApplicationContext(), month, year));
            }
            else {
                view.setTransactionListAdapter(new ArrayList<Transaction>());
                ArrayList<Transaction> mijenjanePlusBrisane = new ArrayList<>();
                mijenjanePlusBrisane.addAll(TransactionsModel.lastLoadedTransactions);
                for(int i = 0; i < TransactionsModel.transactionsDeleted.size(); i++) {
                    boolean prisutna = false;
                    for(int j = 0; j < TransactionsModel.lastLoadedTransactions.size(); j++) {
                        if(TransactionsModel.lastLoadedTransactions.get(j).getId().equals(TransactionsModel.transactionsDeleted.get(i).getId())) {
                            prisutna = true; break;
                        }
                    }
                    if(!prisutna)
                        mijenjanePlusBrisane.add(TransactionsModel.transactionsDeleted.get(i));
                }
                Log.d("transactionsdeletedSize", String.valueOf(mijenjanePlusBrisane.size()));
                view.setTransactions(mijenjanePlusBrisane);
                view.notifyTransactionListDataChanged();
            }
        }
        else {
            Log.d("size", String.valueOf(transactions.size()));
            view.setTransactionListAdapter(new ArrayList<Transaction>());
            sort(transactions);
            view.setTransactions(transactions);
            view.notifyTransactionListDataChanged();
            arrayList = new ArrayList<>();
            arrayList = transactions;
            TransactionsModel.lastLoadedTransactions = new ArrayList<>();
            TransactionsModel.lastLoadedTransactions = transactions;
        }
    }

    private void sort(ArrayList<Transaction> transactions) {
        int selectedItemPosition = view.getSelectedSortByItem();
        if(selectedItemPosition == 0)
            Collections.sort(transactions, new ComparatorPriceAscending());
        else if(selectedItemPosition == 1)
            Collections.sort(transactions, new ComparatorPriceDescending());
        else if(selectedItemPosition == 2)
            Collections.sort(transactions, new ComparatorTitleAscending());
        else if(selectedItemPosition == 3)
            Collections.sort(transactions, new ComparatorTitleDescending());
        else if(selectedItemPosition == 4)
            Collections.sort(transactions, new ComparatorDateAscending());
        else if(selectedItemPosition == 5)
            Collections.sort(transactions, new ComparatorDateAscending());
    }

    TransactionListPresenter(Context context, ITransactionListView view) {
        this.view = view;
        this.context = context;
        if(context == null)
            Log.d("konstruktor", "2");
    }

    TransactionListPresenter(Context context, IUpdateTransactionList view2) {
        this.view2 = view2;
        this.context = context;
        if(context == null)
            Log.d("konstruktor", "3");
    }

    public TransactionListPresenter(Context context) {
        this.context = context;
    }

    public double totalMonthAmount(int month, int year) {
        return TransactionsModel.totalMonthAmount(arrayList, month, year);
    }

    @Override
    public void monthFilter(String typeId) {
     if(typeId == null) {
         String sort = "amount.asc";
         int selectedItemPosition = view.getSelectedSortByItem();
         if(selectedItemPosition == 0)
             sort = "amount.asc";
         else if(selectedItemPosition == 1)
             sort = "amount.desc";
         else if(selectedItemPosition == 2)
             sort = "title.asc";
         else if(selectedItemPosition == 3)
             sort = "title.desc";
         else if(selectedItemPosition == 4)
             sort = "date.asc";
         else if(selectedItemPosition == 5)
             sort = "date.desc";
         Calendar calendar = view.getViewDate();
         @SuppressLint("SimpleDateFormat") SimpleDateFormat mjesec = new SimpleDateFormat("MM");
         @SuppressLint("SimpleDateFormat") SimpleDateFormat godina = new SimpleDateFormat("YYYY");
         String year = godina.format(calendar.getTime());
         String month = mjesec.format(calendar.getTime());
         Log.d("notype",   sort + month + year);
         transactionListInteractor.getTransactions("filter-sort", sort, month, year);
     }
     else {
         String sort = "amount.asc";
         int selectedItemPosition = view.getSelectedSortByItem();
         if(selectedItemPosition == 0)
             sort = "amount.asc";
         else if(selectedItemPosition == 1)
             sort = "amount.desc";
         else if(selectedItemPosition == 2)
             sort = "title.asc";
         else if(selectedItemPosition == 3)
             sort = "title.desc";
         else if(selectedItemPosition == 4)
             sort = "date.asc";
         else if(selectedItemPosition == 5)
             sort = "date.desc";
         Calendar calendar = view.getViewDate();
         @SuppressLint("SimpleDateFormat") SimpleDateFormat mjesec = new SimpleDateFormat("MM");
         @SuppressLint("SimpleDateFormat") SimpleDateFormat godina = new SimpleDateFormat("YYYY");
         String year = godina.format(calendar.getTime());
         String month = mjesec.format(calendar.getTime());
         transactionListInteractor.getTransactions("filter-sort", typeId, sort, month, year);
     }
    }

    @Override
    public void getSelectedType(int position, Type tip) {
        String typeID = null;
        if(position != 0 && tip != null) {
            if(tip.equals(Type.REGULARPAYMENT))
                typeID = "1";
            else if(tip.equals(Type.REGULARINCOME))
                typeID = "2";
            else if(tip.equals(Type.PURCHASE))
                typeID = "3";
            else if(tip.equals(Type.INDIVIDUALINCOME))
                typeID = "4";
            else if(tip.equals(Type.INDIVIDUALPAYMENT))
                typeID = "5";
                monthFilter(typeID);
        }
        else {
            monthFilter(null);
        }
    }

    @Override
    public void removeTransaction(Transaction t, Context context1) {
        transactionListInteractor.remove(t.getId(), t, context1.getApplicationContext());
    }

    @Override
    public void addTransaction(Transaction transaction, FragmentActivity activity) {
        transactionListInteractor.add(transaction, activity.getApplicationContext());
    }
    public void addTransaction(Transaction transaction, Context context) {
        transactionListInteractor.add(transaction, context.getApplicationContext());
    }

    @Override
    public void updateTransaction(Transaction transaction, double formerAmount, Type formerType, Date formerDate, Date formerEndDate, int formerInterval, Context context1) {
        transactionListInteractor.updateTransaction(transaction, formerAmount, formerType, formerDate, formerEndDate, formerInterval, context1.getApplicationContext());
    }

    @Override
    public void getTransactionsCursor(Context context1) {
        Calendar calendar = view.getViewDate();
        @SuppressLint("SimpleDateFormat") SimpleDateFormat mjesec = new SimpleDateFormat("MM");
        @SuppressLint("SimpleDateFormat") SimpleDateFormat godina = new SimpleDateFormat("YYYY");
        String year = godina.format(calendar.getTime());
        String month = mjesec.format(calendar.getTime());
        view.setCursor(transactionListInteractor.getTransactionCursor(context1.getApplicationContext(), month, year));
    }

    @Override
    public Transaction getTransactionFromDB(int internalId, Context context) throws ParseException {
        return transactionListInteractor.getTransactionFromDB(internalId, context);
    }

    @Override
    public void deleteTransactionsFromDB(Context applicationContext) {
        transactionListInteractor.deleteTransactionsFromDB(applicationContext.getApplicationContext());
    }

    @Override
    public void undoDeleteTransactionDB(Transaction undoDeleteThisTransaction, Context context) {
        transactionListInteractor.undoDeleteTransactionDB(undoDeleteThisTransaction, context.getApplicationContext());
    }

    @Override
    public ArrayList<Transaction> getDeletedTransactionsFromDB(Context applicationContext) throws ParseException {
        return transactionListInteractor.getDeletedTransactionsFromDB(applicationContext.getApplicationContext());
    }

    @Override
    public void deleteTransactionsDeletedFromDB(Context applicationContext) {
        transactionListInteractor.deleteTransactionsDeletedFromDB(applicationContext.getApplicationContext());
    }

    @Override
    public ArrayList<Transaction> getTransactionsFromDB(Context context) throws ParseException {
        return transactionListInteractor.getTransactionsFromDB(context.getApplicationContext());
    }

    private class ComparatorPriceAscending implements Comparator<Transaction> {
        @Override
        public int compare(Transaction o1, Transaction o2) {
            if(o1.getAmount() < o2.getAmount())
                return -1;
            else if(o1.getAmount() > o2.getAmount())
                return 1;
            return 0;
        }
    }
    private class ComparatorPriceDescending implements Comparator<Transaction> {
        @Override
        public int compare(Transaction o1, Transaction o2) {
            if(o1.getAmount() > o2.getAmount())
                return -1;
            else if(o1.getAmount() < o2.getAmount())
                return 1;
            return 0;
        }
    }
    private class ComparatorTitleAscending implements Comparator<Transaction> {
        @Override
        public int compare(Transaction o1, Transaction o2) {
            return o1.getTitle().compareTo(o2.getTitle());
        }
    }
    private class ComparatorTitleDescending implements Comparator<Transaction> {
        @Override
        public int compare(Transaction o1, Transaction o2) {
            return o2.getTitle().compareTo(o1.getTitle());
        }
    }
    private class ComparatorDateAscending implements Comparator<Transaction> {
        @Override
        public int compare(Transaction o1, Transaction o2) {
            return o1.getDate().compareTo(o2.getDate());
        }
    }
    private class ComparatorDateDescending implements Comparator<Transaction> {
        @Override
        public int compare(Transaction o1, Transaction o2) {
            return o2.getDate().compareTo(o1.getDate());
        }
    }

}
