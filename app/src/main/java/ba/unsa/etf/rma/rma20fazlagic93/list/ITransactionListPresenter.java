package ba.unsa.etf.rma.rma20fazlagic93.list;

import android.content.Context;

import androidx.fragment.app.FragmentActivity;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

import ba.unsa.etf.rma.rma20fazlagic93.data.Transaction;
import ba.unsa.etf.rma.rma20fazlagic93.data.Type;

public interface ITransactionListPresenter {
    //void refreshTransactions();
    //ITransactionListInteractor getInteractor();
    double totalMonthAmount(int month, int year);
    //void monthFilter(ArrayList<Transaction> transactions);
    void monthFilter(String typeId);
    void getSelectedType(int position, Type tip);
    void removeTransaction(Transaction transaction, Context context);
    void addTransaction(Transaction transaction, FragmentActivity activity);
    void updateTransaction(Transaction transaction, double formerAmount, Type formerType, Date formerDate, Date formerEndDate, int formerInterval, Context context1);
    void getTransactionsCursor(Context context1);
    Transaction getTransactionFromDB(int internalId, Context context) throws ParseException;
    void deleteTransactionsFromDB(Context applicationContext);
    void undoDeleteTransactionDB(Transaction undoDeleteThisTransaction, Context context);
    ArrayList<Transaction> getDeletedTransactionsFromDB(Context applicationContext) throws ParseException;
    void deleteTransactionsDeletedFromDB(Context applicationContext);
    ArrayList<Transaction> getTransactionsFromDB(Context context) throws ParseException;
}
