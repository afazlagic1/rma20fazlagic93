package ba.unsa.etf.rma.rma20fazlagic93.util;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class TransactionDBOpenHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "RMADataBase.db";
    public static final int DATABASE_VERSION = 2;

    public TransactionDBOpenHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }
    public TransactionDBOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static final String ACCOUNT_TABLE = "accounts";
    public static final String ACCOUNT_ID = "_id";
    public static final String ACCOUNT_BUDGET = "budget";
    public static final String ACCOUNT_TOTALLIMIT = "totalLimit";
    public static final String ACCOUNT_MONTHLIMIT = "monthLimit";
    private static final String ACCOUNT_TABLE_CREATE =
            "CREATE TABLE IF NOT EXISTS " + ACCOUNT_TABLE + " (" + ACCOUNT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + ACCOUNT_BUDGET + " REAL, "
                    + ACCOUNT_TOTALLIMIT + " REAL, "
                    + ACCOUNT_MONTHLIMIT + " REAL);";
    private static final String ACCOUNT_DROP = "DROP TABLE IF EXISTS " + ACCOUNT_TABLE;

    public static final String TRANSACTION_TABLE = "transactions";
    public static final String TRANSACTION_ID = "id";
    public static final String TRANSACTION_INTERNAL_ID = "_id";
    public static final String TRANSACTION_TITLE = "title";
    public static final String TRANSACTION_DATE = "date";
    public static final String TRANSACTION_ENDDATE = "endDate";
    public static final String TRANSACTION_AMOUNT = "amount";
    public static final String TRANSACTION_TYPE = "type";
    public static final String TRANSACTION_ITEMDESCRIPTION = "itemDescription";
    public static final String TRANSACTION_TRANSACTION_INTERVAL = "transactionInterval";
    public static final String TRANSACTION_ACCOUNT_ID = "accountId";
    private static final String TRANSACTION_TABLE_CREATE =
            "CREATE TABLE IF NOT EXISTS " + TRANSACTION_TABLE + " ("  + TRANSACTION_INTERNAL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + TRANSACTION_ID + " INTEGER, "
            + TRANSACTION_TITLE + " TEXT NOT NULL, "
            + TRANSACTION_DATE + " TEXT NOT NULL, "
            + TRANSACTION_ENDDATE + " TEXT, "
            + TRANSACTION_AMOUNT + " REAL, "
            + TRANSACTION_TYPE + " TEXT, "
            + TRANSACTION_ITEMDESCRIPTION + " TEXT, "
            + TRANSACTION_TRANSACTION_INTERVAL + " INTEGER, "
            + TRANSACTION_ACCOUNT_ID + " INTEGER NOT NULL, "
            + "FOREIGN KEY (" + TRANSACTION_ACCOUNT_ID + ") REFERENCES " + ACCOUNT_TABLE + "("+ ACCOUNT_ID +"));";
    private static final String TRANSACTION_DROP = "DROP TABLE IF EXISTS " + TRANSACTION_TABLE;

public static final String DELETED_TRANSACTION_TABLE = "deletedtransactions";
private static final String DELETED_TRANSACTION_TABLE_CREATE =
        "CREATE TABLE IF NOT EXISTS " + DELETED_TRANSACTION_TABLE + " ("  + TRANSACTION_INTERNAL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + TRANSACTION_ID + " INTEGER, "
                + TRANSACTION_TITLE + " TEXT NOT NULL, "
                + TRANSACTION_DATE + " TEXT NOT NULL, "
                + TRANSACTION_ENDDATE + " TEXT, "
                + TRANSACTION_AMOUNT + " REAL, "
                + TRANSACTION_TYPE + " TEXT, "
                + TRANSACTION_ITEMDESCRIPTION + " TEXT, "
                + TRANSACTION_TRANSACTION_INTERVAL + " INTEGER, "
                + TRANSACTION_ACCOUNT_ID + " INTEGER NOT NULL, "
                + "FOREIGN KEY (" + TRANSACTION_ACCOUNT_ID + ") REFERENCES " + ACCOUNT_TABLE + "("+ ACCOUNT_ID +"));";
    private static final String DELETED_TRANSACTION_DROP = "DROP TABLE IF EXISTS " + TRANSACTION_TABLE;

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(ACCOUNT_TABLE_CREATE);
        db.execSQL(TRANSACTION_TABLE_CREATE);
        db.execSQL(DELETED_TRANSACTION_TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DELETED_TRANSACTION_DROP);
        db.execSQL(TRANSACTION_DROP);
        db.execSQL(ACCOUNT_DROP);
        onCreate(db);
    }
}
