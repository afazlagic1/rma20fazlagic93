package ba.unsa.etf.rma.rma20fazlagic93.data;

import android.util.Log;

import java.util.ArrayList;
import java.util.Calendar;

public class TransactionsModel {
    public static Double budgetWas;
    public static Double monthLimitWas;
    public static Double totalLimitWas;
    public static ArrayList<Transaction> lastLoadedTransactionsHolder = new ArrayList<>();
    public static ArrayList<Transaction> lastLoadedTransactions = new ArrayList<>();
    public static ArrayList<Transaction> transactionArrayList = new ArrayList<>();
    public static ArrayList<Transaction> transactionsDeleted = new ArrayList<>();
    public static double totalMonthAmount(ArrayList<Transaction> transactions,int month, int year) {
        if(transactions == null)
            transactions = transactionArrayList;
        else
            transactionArrayList = transactions;
        Calendar c1 = Calendar.getInstance();
        Calendar c2 = Calendar.getInstance();
        double t = 0;
        for(int i=0; i < transactions.size(); i++) {
            c1.setTime(transactions.get(i).getDate());
            if((transactions.get(i).getType().equals(Type.REGULARINCOME) || transactions.get(i).getType().equals(Type.REGULARPAYMENT))) {
               if(transactions.get(i).getEndDate() == null && ((c1.get(Calendar.MONTH) <= month && c1.get(Calendar.YEAR) == year) || c1.get(Calendar.YEAR) < year ) )
                   t = t + transactions.get(i).getAmount();
               else if(transactions.get(i).getEndDate() != null) {
                   c2.setTime(transactions.get(i).getEndDate());
                   if(c1.get(Calendar.YEAR) == year && c2.get(Calendar.YEAR) == year) {
                       if(c1.get(Calendar.MONTH) <= month) {
                           while(true) {
                               if(c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DATE) <= c2.get(Calendar.DATE) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == month) {
                                   t = t + transactions.get(i).getAmount();
                               }
                               else if(c1.get(Calendar.MONTH) < c2.get(Calendar.MONTH) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == month) {
                                   t = t + transactions.get(i).getAmount();
                               }
                               else if(c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DATE) > c2.get(Calendar.DATE) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
                                   break;
                               else if(c1.get(Calendar.MONTH) > c2.get(Calendar.MONTH) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
                                   break;
                               else if(c1.get(Calendar.YEAR) > c2.get(Calendar.YEAR))
                                   break;
                               if(transactions.get(i).getTransactionInterval() == 0)
                                   break;
                               c1.add(Calendar.DATE, transactions.get(i).getTransactionInterval());
                           }
                       }
                   }
                   else if(c1.get(Calendar.YEAR) == year && c2.get(Calendar.YEAR) > year) {
                       if(c1.get(Calendar.MONTH) <= month) {
                           while(true) {
                               if(c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DATE) <= c2.get(Calendar.DATE) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == month) {
                                   t = t + transactions.get(i).getAmount();
                               }
                               else if(c1.get(Calendar.MONTH) < c2.get(Calendar.MONTH) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == month) {
                                   t = t + transactions.get(i).getAmount();
                               }
                               else if(c1.get(Calendar.YEAR) < c2.get(Calendar.YEAR) && c1.get(Calendar.YEAR) == year && c1.get(Calendar.MONTH) == month) {
                                   t = t + transactions.get(i).getAmount();
                               }
                               else if(c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DATE) > c2.get(Calendar.DATE) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
                                   break;
                               else if(c1.get(Calendar.MONTH) > c2.get(Calendar.MONTH) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
                                   break;
                               else if(c1.get(Calendar.YEAR) > c2.get(Calendar.YEAR))
                                   break;
                               else if(c1.get(Calendar.YEAR) > year)
                                   break;
                               if(transactions.get(i).getTransactionInterval() == 0)
                                   break;
                               c1.add(Calendar.DATE, transactions.get(i).getTransactionInterval());
                           }
                       }
                   }
                   else if(c1.get(Calendar.YEAR) < year && c2.get(Calendar.YEAR) == year) {
                       if(c2.get(Calendar.MONTH) >= month) {
                           while(true) {
                               if(c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DATE) <= c2.get(Calendar.DATE) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == month) {
                                   t = t + transactions.get(i).getAmount();
                               }
                               else if(c1.get(Calendar.MONTH) < c2.get(Calendar.MONTH) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == month) {
                                   t = t + transactions.get(i).getAmount();
                               }
                               else if(c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DATE) > c2.get(Calendar.DATE) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
                                   break;
                               else if(c1.get(Calendar.MONTH) > c2.get(Calendar.MONTH) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
                                   break;
                               else if(c1.get(Calendar.YEAR) > c2.get(Calendar.YEAR))
                                   break;
                               if(transactions.get(i).getTransactionInterval() == 0)
                                   break;
                               c1.add(Calendar.DATE, transactions.get(i).getTransactionInterval());
                           }
                       }
                   }
                   else if(c1.get(Calendar.YEAR) < year && c2.get(Calendar.YEAR) > year) {
                        while(true) {
                            if(c1.get(Calendar.MONTH) == month && c1.get(Calendar.YEAR) == year)
                                t = t + transactions.get(i).getAmount();
                            else if(c1.get(Calendar.YEAR) > year)
                                break;
                            else if(c1.get(Calendar.YEAR) == year && c1.get(Calendar.MONTH) > month)
                                break;
                            if(transactions.get(i).getTransactionInterval() == 0)
                                break;
                            c1.add(Calendar.DATE, transactions.get(i).getTransactionInterval());
                        }
                   }
               }
            }
            else if(transactions.get(i).getType().equals(Type.INDIVIDUALPAYMENT) || transactions.get(i).getType().equals(Type.INDIVIDUALINCOME) || transactions.get(i).getType().equals(Type.PURCHASE)) {
                if(c1.get(Calendar.MONTH) == month && c1.get(Calendar.YEAR) == year)
                    t = t + transactions.get(i).getAmount();
            }
        }
        return t;
    }
    public static double totalMonthPayment(ArrayList<Transaction> transactions, int month, int year) {
        Log.d("month", String.valueOf(month));
        Calendar c1 = Calendar.getInstance();
        Calendar c2 = Calendar.getInstance();
        double t = 0;
        for(int i = 0; i < transactions.size(); i++) {
            if(transactions.get(i).getType().equals(Type.INDIVIDUALPAYMENT) || transactions.get(i).getType().equals(Type.REGULARPAYMENT) || transactions.get(i).getType().equals(Type.PURCHASE)) {
                c1.setTime(transactions.get(i).getDate());
                if((transactions.get(i).getType().equals(Type.REGULARINCOME) || transactions.get(i).getType().equals(Type.REGULARPAYMENT))) {
                    if(transactions.get(i).getEndDate() == null && ((c1.get(Calendar.MONTH) <= month && c1.get(Calendar.YEAR) == year) || c1.get(Calendar.YEAR) < year ) )
                        t = t + transactions.get(i).getAmount();
                    else if(transactions.get(i).getEndDate() != null) {
                        c2.setTime(transactions.get(i).getEndDate());
                        if(c1.get(Calendar.YEAR) == year && c2.get(Calendar.YEAR) == year) {
                            if(c1.get(Calendar.MONTH) <= month) {
                                while(true) {
                                    if(c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DATE) <= c2.get(Calendar.DATE) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == month) {
                                        t = t + transactions.get(i).getAmount();
                                    }
                                    else if(c1.get(Calendar.MONTH) < c2.get(Calendar.MONTH) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == month) {
                                        t = t + transactions.get(i).getAmount();
                                    }
                                    else if(c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DATE) > c2.get(Calendar.DATE) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
                                        break;
                                    else if(c1.get(Calendar.MONTH) > c2.get(Calendar.MONTH) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
                                        break;
                                    else if(c1.get(Calendar.YEAR) > c2.get(Calendar.YEAR))
                                        break;
                                    if(transactions.get(i).getTransactionInterval() == 0)
                                        break;
                                    c1.add(Calendar.DATE, transactions.get(i).getTransactionInterval());
                                }
                            }
                        }
                        else if(c1.get(Calendar.YEAR) == year && c2.get(Calendar.YEAR) > year) {
                            if(c1.get(Calendar.MONTH) <= month) {
                                while(true) {
                                    if(c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DATE) <= c2.get(Calendar.DATE) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == month) {
                                        t = t + transactions.get(i).getAmount();
                                    }
                                    else if(c1.get(Calendar.MONTH) < c2.get(Calendar.MONTH) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == month) {
                                        t = t + transactions.get(i).getAmount();
                                    }
                                    else if(c1.get(Calendar.YEAR) < c2.get(Calendar.YEAR) && c1.get(Calendar.YEAR) == year && c1.get(Calendar.MONTH) == month) {
                                        t = t + transactions.get(i).getAmount();
                                    }
                                    else if(c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DATE) > c2.get(Calendar.DATE) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
                                        break;
                                    else if(c1.get(Calendar.MONTH) > c2.get(Calendar.MONTH) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
                                        break;
                                    else if(c1.get(Calendar.YEAR) > c2.get(Calendar.YEAR))
                                        break;
                                    else if(c1.get(Calendar.YEAR) > year)
                                        break;
                                    if(transactions.get(i).getTransactionInterval() == 0)
                                        break;
                                    c1.add(Calendar.DATE, transactions.get(i).getTransactionInterval());
                                }
                            }
                        }
                        else if(c1.get(Calendar.YEAR) < year && c2.get(Calendar.YEAR) == year) {
                            if(c2.get(Calendar.MONTH) >= month) {
                                while(true) {
                                    if(c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DATE) <= c2.get(Calendar.DATE) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == month) {
                                        t = t + transactions.get(i).getAmount();
                                    }
                                    else if(c1.get(Calendar.MONTH) < c2.get(Calendar.MONTH) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == month) {
                                        t = t + transactions.get(i).getAmount();
                                    }
                                    else if(c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DATE) > c2.get(Calendar.DATE) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
                                        break;
                                    else if(c1.get(Calendar.MONTH) > c2.get(Calendar.MONTH) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
                                        break;
                                    else if(c1.get(Calendar.YEAR) > c2.get(Calendar.YEAR))
                                        break;
                                    if(transactions.get(i).getTransactionInterval() == 0)
                                        break;
                                    c1.add(Calendar.DATE, transactions.get(i).getTransactionInterval());
                                }
                            }
                        }
                        else if(c1.get(Calendar.YEAR) < year && c2.get(Calendar.YEAR) > year) {
                            while(true) {
                                if(c1.get(Calendar.MONTH) == month && c1.get(Calendar.YEAR) == year)
                                    t = t + transactions.get(i).getAmount();
                                else if(c1.get(Calendar.YEAR) > year)
                                    break;
                                else if(c1.get(Calendar.YEAR) == year && c1.get(Calendar.MONTH) > month)
                                    break;
                                if(transactions.get(i).getTransactionInterval() == 0)
                                    break;
                                c1.add(Calendar.DATE, transactions.get(i).getTransactionInterval());
                            }
                        }
                    }
                }
                else if(transactions.get(i).getType().equals(Type.INDIVIDUALPAYMENT) || transactions.get(i).getType().equals(Type.INDIVIDUALINCOME) || transactions.get(i).getType().equals(Type.PURCHASE)) {
                    if(c1.get(Calendar.MONTH) == month && c1.get(Calendar.YEAR) == year)
                        t = t + transactions.get(i).getAmount();
                }
            }
        }
        return t;
    }
    public static double totalMonthIncome(ArrayList<Transaction> transactions, int month, int year) {
        Calendar c1 = Calendar.getInstance();
        Calendar c2 = Calendar.getInstance();
        double t = 0;
        for(int i = 0; i < transactions.size(); i++) {
            if(transactions.get(i).getType().equals(Type.INDIVIDUALINCOME) || transactions.get(i).getType().equals(Type.REGULARINCOME)) {
                c1.setTime(transactions.get(i).getDate());
                if((transactions.get(i).getType().equals(Type.REGULARINCOME) || transactions.get(i).getType().equals(Type.REGULARPAYMENT))) {
                    if(transactions.get(i).getEndDate() == null && ((c1.get(Calendar.MONTH) <= month && c1.get(Calendar.YEAR) == year) || c1.get(Calendar.YEAR) < year ) )
                        t = t + transactions.get(i).getAmount();
                    else if(transactions.get(i).getEndDate() != null) {
                        c2.setTime(transactions.get(i).getEndDate());
                        if(c1.get(Calendar.YEAR) == year && c2.get(Calendar.YEAR) == year) {
                            if(c1.get(Calendar.MONTH) <= month) {
                                while(true) {
                                    if(c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DATE) <= c2.get(Calendar.DATE) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == month) {
                                        t = t + transactions.get(i).getAmount();
                                    }
                                    else if(c1.get(Calendar.MONTH) < c2.get(Calendar.MONTH) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == month) {
                                        t = t + transactions.get(i).getAmount();
                                    }
                                    else if(c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DATE) > c2.get(Calendar.DATE) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
                                        break;
                                    else if(c1.get(Calendar.MONTH) > c2.get(Calendar.MONTH) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
                                        break;
                                    else if(c1.get(Calendar.YEAR) > c2.get(Calendar.YEAR))
                                        break;
                                    if(transactions.get(i).getTransactionInterval() == 0)
                                        break;
                                    c1.add(Calendar.DATE, transactions.get(i).getTransactionInterval());
                                }
                            }
                        }
                        else if(c1.get(Calendar.YEAR) == year && c2.get(Calendar.YEAR) > year) {
                            if(c1.get(Calendar.MONTH) <= month) {
                                while(true) {
                                    if(c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DATE) <= c2.get(Calendar.DATE) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == month) {
                                        t = t + transactions.get(i).getAmount();
                                    }
                                    else if(c1.get(Calendar.MONTH) < c2.get(Calendar.MONTH) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == month) {
                                        t = t + transactions.get(i).getAmount();
                                    }
                                    else if(c1.get(Calendar.YEAR) < c2.get(Calendar.YEAR) && c1.get(Calendar.YEAR) == year && c1.get(Calendar.MONTH) == month) {
                                        t = t + transactions.get(i).getAmount();
                                    }
                                    else if(c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DATE) > c2.get(Calendar.DATE) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
                                        break;
                                    else if(c1.get(Calendar.MONTH) > c2.get(Calendar.MONTH) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
                                        break;
                                    else if(c1.get(Calendar.YEAR) > c2.get(Calendar.YEAR))
                                        break;
                                    else if(c1.get(Calendar.YEAR) > year)
                                        break;
                                    if(transactions.get(i).getTransactionInterval() == 0)
                                        break;
                                    c1.add(Calendar.DATE, transactions.get(i).getTransactionInterval());
                                }
                            }
                        }
                        else if(c1.get(Calendar.YEAR) < year && c2.get(Calendar.YEAR) == year) {
                            if(c2.get(Calendar.MONTH) >= month) {
                                while(true) {
                                    if(c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DATE) <= c2.get(Calendar.DATE) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == month) {
                                        t = t + transactions.get(i).getAmount();
                                    }
                                    else if(c1.get(Calendar.MONTH) < c2.get(Calendar.MONTH) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == month) {
                                        t = t + transactions.get(i).getAmount();
                                    }
                                    else if(c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DATE) > c2.get(Calendar.DATE) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
                                        break;
                                    else if(c1.get(Calendar.MONTH) > c2.get(Calendar.MONTH) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
                                        break;
                                    else if(c1.get(Calendar.YEAR) > c2.get(Calendar.YEAR))
                                        break;
                                    if(transactions.get(i).getTransactionInterval() == 0)
                                        break;
                                    c1.add(Calendar.DATE, transactions.get(i).getTransactionInterval());
                                }
                            }
                        }
                        else if(c1.get(Calendar.YEAR) < year && c2.get(Calendar.YEAR) > year) {
                            while(true) {
                                if(c1.get(Calendar.MONTH) == month && c1.get(Calendar.YEAR) == year)
                                    t = t + transactions.get(i).getAmount();
                                else if(c1.get(Calendar.YEAR) > year)
                                    break;
                                else if(c1.get(Calendar.YEAR) == year && c1.get(Calendar.MONTH) > month)
                                    break;
                                if(transactions.get(i).getTransactionInterval() == 0)
                                    break;
                                c1.add(Calendar.DATE, transactions.get(i).getTransactionInterval());
                            }
                        }
                    }
                }
                else if(transactions.get(i).getType().equals(Type.INDIVIDUALPAYMENT) || transactions.get(i).getType().equals(Type.INDIVIDUALINCOME) || transactions.get(i).getType().equals(Type.PURCHASE)) {
                    if(c1.get(Calendar.MONTH) == month && c1.get(Calendar.YEAR) == year)
                        t = t + transactions.get(i).getAmount();
                }
            }
        }
        return t;
    }

    public static double totalDayAmount(ArrayList<Transaction> transactions, int date, int month, int year) {
        Calendar c1 = Calendar.getInstance();
        Calendar c2 = Calendar.getInstance();
        double t = 0;
        for(int i=0; i < transactions.size(); i++) {
            c1.setTime(transactions.get(i).getDate());
            if((transactions.get(i).getType().equals(Type.REGULARINCOME) || transactions.get(i).getType().equals(Type.REGULARPAYMENT))) {
                if(c1.get(Calendar.DATE) == date && transactions.get(i).getEndDate() == null && ((c1.get(Calendar.MONTH) == month && c1.get(Calendar.YEAR) == year)) )
                    t = t + transactions.get(i).getAmount();
                else if(transactions.get(i).getEndDate() != null) {
                    c2.setTime(transactions.get(i).getEndDate());
                    if(c1.get(Calendar.YEAR) == year && c2.get(Calendar.YEAR) == year) {
                        if(c1.get(Calendar.MONTH) <= month) {
                            while(true) {
                                if(c1.get(Calendar.DATE) == date && c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DATE) <= c2.get(Calendar.DATE) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == month) {
                                    t = t + transactions.get(i).getAmount();
                                    break;
                                }
                                else if(c1.get(Calendar.DATE) == date && c1.get(Calendar.MONTH) < c2.get(Calendar.MONTH) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == month) {
                                    t = t + transactions.get(i).getAmount();
                                    break;
                                }
                                else if(c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DATE) > c2.get(Calendar.DATE) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
                                    break;
                                else if(c1.get(Calendar.MONTH) > c2.get(Calendar.MONTH) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
                                    break;
                                else if(c1.get(Calendar.YEAR) > c2.get(Calendar.YEAR))
                                    break;
                                if(transactions.get(i).getTransactionInterval() == 0)
                                    break;
                                c1.add(Calendar.DATE, transactions.get(i).getTransactionInterval());
                            }
                        }
                    }
                    else if(c1.get(Calendar.YEAR) == year && c2.get(Calendar.YEAR) > year) {
                        if(c1.get(Calendar.MONTH) <= month) {
                            while(true) {
                                if(c1.get(Calendar.DATE) == date && c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DATE) <= c2.get(Calendar.DATE) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == month) {
                                    t = t + transactions.get(i).getAmount();
                                    break;
                                }
                                else if(c1.get(Calendar.DATE) == date && c1.get(Calendar.MONTH) < c2.get(Calendar.MONTH) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == month) {
                                    t = t + transactions.get(i).getAmount();
                                    break;
                                }
                                else if(c1.get(Calendar.DATE) == date && c1.get(Calendar.YEAR) < c2.get(Calendar.YEAR) && c1.get(Calendar.YEAR) == year && c1.get(Calendar.MONTH) == month) {
                                    t = t + transactions.get(i).getAmount();
                                    break;
                                }
                                else if(c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DATE) > c2.get(Calendar.DATE) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
                                    break;
                                else if(c1.get(Calendar.MONTH) > c2.get(Calendar.MONTH) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
                                    break;
                                else if(c1.get(Calendar.YEAR) > c2.get(Calendar.YEAR))
                                    break;
                                else if(c1.get(Calendar.YEAR) > year)
                                    break;
                                if(transactions.get(i).getTransactionInterval() == 0)
                                    break;
                                c1.add(Calendar.DATE, transactions.get(i).getTransactionInterval());
                            }
                        }
                    }
                    else if(c1.get(Calendar.YEAR) < year && c2.get(Calendar.YEAR) == year) {
                        if(c2.get(Calendar.MONTH) >= month) {
                            while(true) {
                                if(c1.get(Calendar.DATE) == date && c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DATE) <= c2.get(Calendar.DATE) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == month) {
                                    t = t + transactions.get(i).getAmount();
                                    break;
                                }
                                else if(c1.get(Calendar.DATE) == date && c1.get(Calendar.MONTH) < c2.get(Calendar.MONTH) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == month) {
                                    t = t + transactions.get(i).getAmount();
                                    break;
                                }
                                else if(c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DATE) > c2.get(Calendar.DATE) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
                                    break;
                                else if(c1.get(Calendar.MONTH) > c2.get(Calendar.MONTH) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
                                    break;
                                else if(c1.get(Calendar.YEAR) > c2.get(Calendar.YEAR))
                                    break;
                                if(transactions.get(i).getTransactionInterval() == 0)
                                    break;
                                c1.add(Calendar.DATE, transactions.get(i).getTransactionInterval());
                            }
                        }
                    }
                    else if(c1.get(Calendar.YEAR) < year && c2.get(Calendar.YEAR) > year) {
                        while(true) {
                            if(c1.get(Calendar.DATE) == date && c1.get(Calendar.MONTH) == month && c1.get(Calendar.YEAR) == year) {
                                t = t + transactions.get(i).getAmount();
                                break;
                            }
                            else if(c1.get(Calendar.YEAR) > year)
                                break;
                            else if(c1.get(Calendar.YEAR) == year && c1.get(Calendar.MONTH) > month)
                                break;
                            if(transactions.get(i).getTransactionInterval() == 0)
                                break;
                            c1.add(Calendar.DATE, transactions.get(i).getTransactionInterval());
                        }
                    }
                }
            }
            else if(transactions.get(i).getType().equals(Type.INDIVIDUALPAYMENT) || transactions.get(i).getType().equals(Type.INDIVIDUALINCOME) || transactions.get(i).getType().equals(Type.PURCHASE)) {
                if(c1.get(Calendar.MONTH) == month && c1.get(Calendar.YEAR) == year && c1.get(Calendar.DATE) == date)
                    t = t + transactions.get(i).getAmount();
            }
        }
        return t;
    }
    public static double totalDayPayment(ArrayList<Transaction> transactions,int date, int month, int year) {
        Calendar c1 = Calendar.getInstance();
        Calendar c2 = Calendar.getInstance();
        double t = 0;
        for(int i=0; i < transactions.size(); i++) {
            if(transactions.get(i).getType().equals(Type.INDIVIDUALPAYMENT) || transactions.get(i).getType().equals(Type.REGULARPAYMENT) || transactions.get(i).getType().equals(Type.PURCHASE)) {
            c1.setTime(transactions.get(i).getDate());
            if((transactions.get(i).getType().equals(Type.REGULARINCOME) || transactions.get(i).getType().equals(Type.REGULARPAYMENT))) {
                if(c1.get(Calendar.DATE) == date && transactions.get(i).getEndDate() == null && ((c1.get(Calendar.MONTH) == month && c1.get(Calendar.YEAR) == year)) )
                    t = t + transactions.get(i).getAmount();
                else if(transactions.get(i).getEndDate() != null) {
                    c2.setTime(transactions.get(i).getEndDate());
                    if(c1.get(Calendar.YEAR) == year && c2.get(Calendar.YEAR) == year) {
                        if(c1.get(Calendar.MONTH) <= month) {
                            while(true) {
                                if(c1.get(Calendar.DATE) == date && c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DATE) <= c2.get(Calendar.DATE) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == month) {
                                    t = t + transactions.get(i).getAmount();
                                    break;
                                }
                                else if(c1.get(Calendar.DATE) == date && c1.get(Calendar.MONTH) < c2.get(Calendar.MONTH) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == month) {
                                    t = t + transactions.get(i).getAmount();
                                    break;
                                }
                                else if(c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DATE) > c2.get(Calendar.DATE) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
                                    break;
                                else if(c1.get(Calendar.MONTH) > c2.get(Calendar.MONTH) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
                                    break;
                                else if(c1.get(Calendar.YEAR) > c2.get(Calendar.YEAR))
                                    break;
                                if(transactions.get(i).getTransactionInterval() == 0)
                                    break;
                                c1.add(Calendar.DATE, transactions.get(i).getTransactionInterval());
                            }
                        }
                    }
                    else if(c1.get(Calendar.YEAR) == year && c2.get(Calendar.YEAR) > year) {
                        if(c1.get(Calendar.MONTH) <= month) {
                            while(true) {
                                if(c1.get(Calendar.DATE) == date && c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DATE) <= c2.get(Calendar.DATE) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == month) {
                                    t = t + transactions.get(i).getAmount();
                                    break;
                                }
                                else if(c1.get(Calendar.DATE) == date && c1.get(Calendar.MONTH) < c2.get(Calendar.MONTH) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == month) {
                                    t = t + transactions.get(i).getAmount();
                                    break;
                                }
                                else if(c1.get(Calendar.DATE) == date && c1.get(Calendar.YEAR) < c2.get(Calendar.YEAR) && c1.get(Calendar.YEAR) == year && c1.get(Calendar.MONTH) == month) {
                                    t = t + transactions.get(i).getAmount();
                                    break;
                                }
                                else if(c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DATE) > c2.get(Calendar.DATE) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
                                    break;
                                else if(c1.get(Calendar.MONTH) > c2.get(Calendar.MONTH) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
                                    break;
                                else if(c1.get(Calendar.YEAR) > c2.get(Calendar.YEAR))
                                    break;
                                else if(c1.get(Calendar.YEAR) > year)
                                    break;
                                if(transactions.get(i).getTransactionInterval() == 0)
                                    break;
                                c1.add(Calendar.DATE, transactions.get(i).getTransactionInterval());
                            }
                        }
                    }
                    else if(c1.get(Calendar.YEAR) < year && c2.get(Calendar.YEAR) == year) {
                        if(c2.get(Calendar.MONTH) >= month) {
                            while(true) {
                                if(c1.get(Calendar.DATE) == date && c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DATE) <= c2.get(Calendar.DATE) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == month) {
                                    t = t + transactions.get(i).getAmount();
                                    break;
                                }
                                else if(c1.get(Calendar.DATE) == date && c1.get(Calendar.MONTH) < c2.get(Calendar.MONTH) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == month) {
                                    t = t + transactions.get(i).getAmount();
                                    break;
                                }
                                else if(c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DATE) > c2.get(Calendar.DATE) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
                                    break;
                                else if(c1.get(Calendar.MONTH) > c2.get(Calendar.MONTH) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
                                    break;
                                else if(c1.get(Calendar.YEAR) > c2.get(Calendar.YEAR))
                                    break;
                                if(transactions.get(i).getTransactionInterval() == 0)
                                    break;
                                c1.add(Calendar.DATE, transactions.get(i).getTransactionInterval());
                            }
                        }
                    }
                    else if(c1.get(Calendar.YEAR) < year && c2.get(Calendar.YEAR) > year) {
                        while(true) {
                            if(c1.get(Calendar.DATE) == date && c1.get(Calendar.MONTH) == month && c1.get(Calendar.YEAR) == year) {
                                t = t + transactions.get(i).getAmount();
                                break;
                            }
                            else if(c1.get(Calendar.YEAR) > year)
                                break;
                            else if(c1.get(Calendar.YEAR) == year && c1.get(Calendar.MONTH) > month)
                                break;
                            if(transactions.get(i).getTransactionInterval() == 0)
                                break;
                            c1.add(Calendar.DATE, transactions.get(i).getTransactionInterval());
                        }
                    }
                }
            }
            else if(transactions.get(i).getType().equals(Type.INDIVIDUALPAYMENT) || transactions.get(i).getType().equals(Type.INDIVIDUALINCOME) || transactions.get(i).getType().equals(Type.PURCHASE)) {
                if(c1.get(Calendar.MONTH) == month && c1.get(Calendar.YEAR) == year && c1.get(Calendar.DATE) == date)
                    t = t + transactions.get(i).getAmount();
                }
            }
        }
        return t;
    }
    public static double totalDayIncome(ArrayList<Transaction> transactions, int date, int month, int year) {
        Calendar c1 = Calendar.getInstance();
        Calendar c2 = Calendar.getInstance();
        double t = 0;
        for(int i=0; i < transactions.size(); i++) {
            if(transactions.get(i).getType().equals(Type.INDIVIDUALINCOME) || transactions.get(i).getType().equals(Type.REGULARINCOME)) {
                c1.setTime(transactions.get(i).getDate());
                if((transactions.get(i).getType().equals(Type.REGULARINCOME) || transactions.get(i).getType().equals(Type.REGULARPAYMENT))) {
                    if(c1.get(Calendar.DATE) == date && transactions.get(i).getEndDate() == null && ((c1.get(Calendar.MONTH) == month && c1.get(Calendar.YEAR) == year)) )
                        t = t + transactions.get(i).getAmount();
                    else if(transactions.get(i).getEndDate() != null) {
                        c2.setTime(transactions.get(i).getEndDate());
                        if(c1.get(Calendar.YEAR) == year && c2.get(Calendar.YEAR) == year) {
                            if(c1.get(Calendar.MONTH) <= month) {
                                while(true) {
                                    if(c1.get(Calendar.DATE) == date && c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DATE) <= c2.get(Calendar.DATE) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == month) {
                                        t = t + transactions.get(i).getAmount();
                                        break;
                                    }
                                    else if(c1.get(Calendar.DATE) == date && c1.get(Calendar.MONTH) < c2.get(Calendar.MONTH) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == month) {
                                        t = t + transactions.get(i).getAmount();
                                        break;
                                    }
                                    else if(c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DATE) > c2.get(Calendar.DATE) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
                                        break;
                                    else if(c1.get(Calendar.MONTH) > c2.get(Calendar.MONTH) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
                                        break;
                                    else if(c1.get(Calendar.YEAR) > c2.get(Calendar.YEAR))
                                        break;
                                    if(transactions.get(i).getTransactionInterval() == 0)
                                        break;
                                    c1.add(Calendar.DATE, transactions.get(i).getTransactionInterval());
                                }
                            }
                        }
                        else if(c1.get(Calendar.YEAR) == year && c2.get(Calendar.YEAR) > year) {
                            if(c1.get(Calendar.MONTH) <= month) {
                                while(true) {
                                    if(c1.get(Calendar.DATE) == date && c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DATE) <= c2.get(Calendar.DATE) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == month) {
                                        t = t + transactions.get(i).getAmount();
                                        break;
                                    }
                                    else if(c1.get(Calendar.DATE) == date && c1.get(Calendar.MONTH) < c2.get(Calendar.MONTH) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == month) {
                                        t = t + transactions.get(i).getAmount();
                                        break;
                                    }
                                    else if(c1.get(Calendar.DATE) == date && c1.get(Calendar.YEAR) < c2.get(Calendar.YEAR) && c1.get(Calendar.YEAR) == year && c1.get(Calendar.MONTH) == month) {
                                        t = t + transactions.get(i).getAmount();
                                        break;
                                    }
                                    else if(c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DATE) > c2.get(Calendar.DATE) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
                                        break;
                                    else if(c1.get(Calendar.MONTH) > c2.get(Calendar.MONTH) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
                                        break;
                                    else if(c1.get(Calendar.YEAR) > c2.get(Calendar.YEAR))
                                        break;
                                    else if(c1.get(Calendar.YEAR) > year)
                                        break;
                                    if(transactions.get(i).getTransactionInterval() == 0)
                                        break;
                                    c1.add(Calendar.DATE, transactions.get(i).getTransactionInterval());
                                }
                            }
                        }
                        else if(c1.get(Calendar.YEAR) < year && c2.get(Calendar.YEAR) == year) {
                            if(c2.get(Calendar.MONTH) >= month) {
                                while(true) {
                                    if(c1.get(Calendar.DATE) == date && c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DATE) <= c2.get(Calendar.DATE) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == month) {
                                        t = t + transactions.get(i).getAmount();
                                        break;
                                    }
                                    else if(c1.get(Calendar.DATE) == date && c1.get(Calendar.MONTH) < c2.get(Calendar.MONTH) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == month) {
                                        t = t + transactions.get(i).getAmount();
                                        break;
                                    }
                                    else if(c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DATE) > c2.get(Calendar.DATE) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
                                        break;
                                    else if(c1.get(Calendar.MONTH) > c2.get(Calendar.MONTH) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
                                        break;
                                    else if(c1.get(Calendar.YEAR) > c2.get(Calendar.YEAR))
                                        break;
                                    if(transactions.get(i).getTransactionInterval() == 0)
                                        break;
                                    c1.add(Calendar.DATE, transactions.get(i).getTransactionInterval());
                                }
                            }
                        }
                        else if(c1.get(Calendar.YEAR) < year && c2.get(Calendar.YEAR) > year) {
                            while(true) {
                                if(c1.get(Calendar.DATE) == date && c1.get(Calendar.MONTH) == month && c1.get(Calendar.YEAR) == year) {
                                    t = t + transactions.get(i).getAmount();
                                    break;
                                }
                                else if(c1.get(Calendar.YEAR) > year)
                                    break;
                                else if(c1.get(Calendar.YEAR) == year && c1.get(Calendar.MONTH) > month)
                                    break;
                                if(transactions.get(i).getTransactionInterval() == 0)
                                    break;
                                c1.add(Calendar.DATE, transactions.get(i).getTransactionInterval());
                            }
                        }
                    }
                }
                else if(transactions.get(i).getType().equals(Type.INDIVIDUALPAYMENT) || transactions.get(i).getType().equals(Type.INDIVIDUALINCOME) || transactions.get(i).getType().equals(Type.PURCHASE)) {
                    if(c1.get(Calendar.MONTH) == month && c1.get(Calendar.YEAR) == year && c1.get(Calendar.DATE) == date)
                        t = t + transactions.get(i).getAmount();
                }
            }
        }
        return t;
    }

    public static double totalWeekAmount(ArrayList<Transaction> transactions, int week, int month, int year) {
        Calendar c1 = Calendar.getInstance();
        Calendar c2 = Calendar.getInstance();
        double t = 0;
        for(int i=0; i < transactions.size(); i++) {
        c1.setTime(transactions.get(i).getDate());
        if((transactions.get(i).getType().equals(Type.REGULARINCOME) || transactions.get(i).getType().equals(Type.REGULARPAYMENT))) {
            if(c1.get(Calendar.WEEK_OF_MONTH) == week && transactions.get(i).getEndDate() == null && ((c1.get(Calendar.MONTH) == month && c1.get(Calendar.YEAR) == year)) )
                t = t + transactions.get(i).getAmount();
            else if(transactions.get(i).getEndDate() != null) {
                c2.setTime(transactions.get(i).getEndDate());
                if(c1.get(Calendar.YEAR) == year && c2.get(Calendar.YEAR) == year) {
                    if(c1.get(Calendar.MONTH) <= month) {
                        while(true) {
                            if(c1.get(Calendar.WEEK_OF_MONTH) == week && c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DATE) <= c2.get(Calendar.DATE) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == month) {
                                t = t + transactions.get(i).getAmount();
                            }
                            else if(c1.get(Calendar.WEEK_OF_MONTH) == week && c1.get(Calendar.MONTH) < c2.get(Calendar.MONTH) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == month) {
                                t = t + transactions.get(i).getAmount();
                            }
                            else if(c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DATE) > c2.get(Calendar.DATE) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
                                break;
                            else if(c1.get(Calendar.MONTH) > c2.get(Calendar.MONTH) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
                                break;
                            else if(c1.get(Calendar.YEAR) > c2.get(Calendar.YEAR))
                                break;
                            if(transactions.get(i).getTransactionInterval() == 0)
                                break;
                            c1.add(Calendar.DATE, transactions.get(i).getTransactionInterval());
                        }
                    }
                }
                else if(c1.get(Calendar.YEAR) == year && c2.get(Calendar.YEAR) > year) {
                    if(c1.get(Calendar.MONTH) <= month) {
                        while(true) {
                            if(c1.get(Calendar.WEEK_OF_MONTH) == week && c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DATE) <= c2.get(Calendar.DATE) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == month) {
                                t = t + transactions.get(i).getAmount();
                            }
                            else if(c1.get(Calendar.WEEK_OF_MONTH) == week && c1.get(Calendar.MONTH) < c2.get(Calendar.MONTH) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == month) {
                                t = t + transactions.get(i).getAmount();
                            }
                            else if(c1.get(Calendar.WEEK_OF_MONTH) == week && c1.get(Calendar.YEAR) < c2.get(Calendar.YEAR) && c1.get(Calendar.YEAR) == year && c1.get(Calendar.MONTH) == month) {
                                t = t + transactions.get(i).getAmount();
                            }
                            else if(c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DATE) > c2.get(Calendar.DATE) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
                                break;
                            else if(c1.get(Calendar.MONTH) > c2.get(Calendar.MONTH) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
                                break;
                            else if(c1.get(Calendar.YEAR) > c2.get(Calendar.YEAR))
                                break;
                            else if(c1.get(Calendar.YEAR) > year)
                                break;
                            if(transactions.get(i).getTransactionInterval() == 0)
                                break;
                            c1.add(Calendar.DATE, transactions.get(i).getTransactionInterval());
                        }
                    }
                }
                else if(c1.get(Calendar.YEAR) < year && c2.get(Calendar.YEAR) == year) {
                    if(c2.get(Calendar.MONTH) >= month) {
                        while(true) {
                            if(c1.get(Calendar.WEEK_OF_MONTH) == week && c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DATE) <= c2.get(Calendar.DATE) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == month) {
                                t = t + transactions.get(i).getAmount();
                            }
                            else if(c1.get(Calendar.WEEK_OF_MONTH) == week && c1.get(Calendar.MONTH) < c2.get(Calendar.MONTH) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == month) {
                                t = t + transactions.get(i).getAmount();
                            }
                            else if(c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DATE) > c2.get(Calendar.DATE) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
                                break;
                            else if(c1.get(Calendar.MONTH) > c2.get(Calendar.MONTH) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
                                break;
                            else if(c1.get(Calendar.YEAR) > c2.get(Calendar.YEAR))
                                break;
                            if(transactions.get(i).getTransactionInterval() == 0)
                                break;
                            c1.add(Calendar.DATE, transactions.get(i).getTransactionInterval());
                        }
                    }
                }
                else if(c1.get(Calendar.YEAR) < year && c2.get(Calendar.YEAR) > year) {
                    while(true) {
                        if(c1.get(Calendar.WEEK_OF_MONTH) == week && c1.get(Calendar.MONTH) == month && c1.get(Calendar.YEAR) == year) {
                            t = t + transactions.get(i).getAmount();
                        }
                        else if(c1.get(Calendar.YEAR) > year)
                            break;
                        else if(c1.get(Calendar.YEAR) == year && c1.get(Calendar.MONTH) > month)
                            break;
                        if(transactions.get(i).getTransactionInterval() == 0)
                            break;
                        c1.add(Calendar.DATE, transactions.get(i).getTransactionInterval());
                    }
                }
            }
        }
        else if(transactions.get(i).getType().equals(Type.INDIVIDUALPAYMENT) || transactions.get(i).getType().equals(Type.INDIVIDUALINCOME) || transactions.get(i).getType().equals(Type.PURCHASE)) {
            if(c1.get(Calendar.MONTH) == month && c1.get(Calendar.YEAR) == year && c1.get(Calendar.WEEK_OF_MONTH) == week)
                t = t + transactions.get(i).getAmount();
            }
        }
        return t;
    }

    public static double totalWeekPayment(ArrayList<Transaction> transactions, int week,int month,int year) {
        Calendar c1 = Calendar.getInstance();
        Calendar c2 = Calendar.getInstance();
        double t = 0;
        for(int i=0; i < transactions.size(); i++) {
            if(transactions.get(i).getType().equals(Type.INDIVIDUALPAYMENT) || transactions.get(i).getType().equals(Type.REGULARPAYMENT) || transactions.get(i).getType().equals(Type.PURCHASE)) {
            c1.setTime(transactions.get(i).getDate());
            if((transactions.get(i).getType().equals(Type.REGULARINCOME) || transactions.get(i).getType().equals(Type.REGULARPAYMENT))) {
                if(c1.get(Calendar.WEEK_OF_MONTH) == week && transactions.get(i).getEndDate() == null && ((c1.get(Calendar.MONTH) == month && c1.get(Calendar.YEAR) == year)) )
                    t = t + transactions.get(i).getAmount();
                else if(transactions.get(i).getEndDate() != null) {
                    c2.setTime(transactions.get(i).getEndDate());
                    if(c1.get(Calendar.YEAR) == year && c2.get(Calendar.YEAR) == year) {
                        if(c1.get(Calendar.MONTH) <= month) {
                            while(true) {
                                if(c1.get(Calendar.WEEK_OF_MONTH) == week && c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DATE) <= c2.get(Calendar.DATE) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == month) {
                                    t = t + transactions.get(i).getAmount();
                                }
                                else if(c1.get(Calendar.WEEK_OF_MONTH) == week && c1.get(Calendar.MONTH) < c2.get(Calendar.MONTH) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == month) {
                                    t = t + transactions.get(i).getAmount();
                                }
                                else if(c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DATE) > c2.get(Calendar.DATE) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
                                    break;
                                else if(c1.get(Calendar.MONTH) > c2.get(Calendar.MONTH) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
                                    break;
                                else if(c1.get(Calendar.YEAR) > c2.get(Calendar.YEAR))
                                    break;
                                if(transactions.get(i).getTransactionInterval() == 0)
                                    break;
                                c1.add(Calendar.DATE, transactions.get(i).getTransactionInterval());
                            }
                        }
                    }
                    else if(c1.get(Calendar.YEAR) == year && c2.get(Calendar.YEAR) > year) {
                        if(c1.get(Calendar.MONTH) <= month) {
                            while(true) {
                                if(c1.get(Calendar.WEEK_OF_MONTH) == week && c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DATE) <= c2.get(Calendar.DATE) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == month) {
                                    t = t + transactions.get(i).getAmount();
                                }
                                else if(c1.get(Calendar.WEEK_OF_MONTH) == week && c1.get(Calendar.MONTH) < c2.get(Calendar.MONTH) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == month) {
                                    t = t + transactions.get(i).getAmount();
                                }
                                else if(c1.get(Calendar.WEEK_OF_MONTH) == week && c1.get(Calendar.YEAR) < c2.get(Calendar.YEAR) && c1.get(Calendar.YEAR) == year && c1.get(Calendar.MONTH) == month) {
                                    t = t + transactions.get(i).getAmount();
                                }
                                else if(c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DATE) > c2.get(Calendar.DATE) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
                                    break;
                                else if(c1.get(Calendar.MONTH) > c2.get(Calendar.MONTH) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
                                    break;
                                else if(c1.get(Calendar.YEAR) > c2.get(Calendar.YEAR))
                                    break;
                                else if(c1.get(Calendar.YEAR) > year)
                                    break;
                                if(transactions.get(i).getTransactionInterval() == 0)
                                    break;
                                c1.add(Calendar.DATE, transactions.get(i).getTransactionInterval());
                            }
                        }
                    }
                    else if(c1.get(Calendar.YEAR) < year && c2.get(Calendar.YEAR) == year) {
                        if(c2.get(Calendar.MONTH) >= month) {
                            while(true) {
                                if(c1.get(Calendar.WEEK_OF_MONTH) == week && c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DATE) <= c2.get(Calendar.DATE) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == month) {
                                    t = t + transactions.get(i).getAmount();
                                }
                                else if(c1.get(Calendar.WEEK_OF_MONTH) == week && c1.get(Calendar.MONTH) < c2.get(Calendar.MONTH) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == month) {
                                    t = t + transactions.get(i).getAmount();
                                }
                                else if(c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DATE) > c2.get(Calendar.DATE) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
                                    break;
                                else if(c1.get(Calendar.MONTH) > c2.get(Calendar.MONTH) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
                                    break;
                                else if(c1.get(Calendar.YEAR) > c2.get(Calendar.YEAR))
                                    break;
                                if(transactions.get(i).getTransactionInterval() == 0)
                                    break;
                                c1.add(Calendar.DATE, transactions.get(i).getTransactionInterval());
                            }
                        }
                    }
                    else if(c1.get(Calendar.YEAR) < year && c2.get(Calendar.YEAR) > year) {
                        while(true) {
                            if(c1.get(Calendar.WEEK_OF_MONTH) == week && c1.get(Calendar.MONTH) == month && c1.get(Calendar.YEAR) == year) {
                                t = t + transactions.get(i).getAmount();
                            }
                            else if(c1.get(Calendar.YEAR) > year)
                                break;
                            else if(c1.get(Calendar.YEAR) == year && c1.get(Calendar.MONTH) > month)
                                break;
                            if(transactions.get(i).getTransactionInterval() == 0)
                                break;
                            c1.add(Calendar.DATE, transactions.get(i).getTransactionInterval());
                        }
                    }
                }
            }
            else if(transactions.get(i).getType().equals(Type.INDIVIDUALPAYMENT) || transactions.get(i).getType().equals(Type.INDIVIDUALINCOME) || transactions.get(i).getType().equals(Type.PURCHASE)) {
                if(c1.get(Calendar.MONTH) == month && c1.get(Calendar.YEAR) == year && c1.get(Calendar.WEEK_OF_MONTH) == week)
                    t = t + transactions.get(i).getAmount();
            }
        }
    }
        return t;
    }

    public static double totalWeekIncome(ArrayList<Transaction> transactions,int week, int month, int year) {
        Calendar c1 = Calendar.getInstance();
        Calendar c2 = Calendar.getInstance();
        double t = 0;
        for(int i=0; i < transactions.size(); i++) {
            if(transactions.get(i).getType().equals(Type.INDIVIDUALINCOME) || transactions.get(i).getType().equals(Type.REGULARINCOME)) {
            c1.setTime(transactions.get(i).getDate());
            if((transactions.get(i).getType().equals(Type.REGULARINCOME) || transactions.get(i).getType().equals(Type.REGULARPAYMENT))) {
                if(c1.get(Calendar.WEEK_OF_MONTH) == week && transactions.get(i).getEndDate() == null && ((c1.get(Calendar.MONTH) == month && c1.get(Calendar.YEAR) == year)) )
                    t = t + transactions.get(i).getAmount();
                else if(transactions.get(i).getEndDate() != null) {
                    c2.setTime(transactions.get(i).getEndDate());
                    if(c1.get(Calendar.YEAR) == year && c2.get(Calendar.YEAR) == year) {
                        if(c1.get(Calendar.MONTH) <= month) {
                            while(true) {
                                if(c1.get(Calendar.WEEK_OF_MONTH) == week && c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DATE) <= c2.get(Calendar.DATE) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == month) {
                                    t = t + transactions.get(i).getAmount();
                                }
                                else if(c1.get(Calendar.WEEK_OF_MONTH) == week && c1.get(Calendar.MONTH) < c2.get(Calendar.MONTH) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == month) {
                                    t = t + transactions.get(i).getAmount();
                                }
                                else if(c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DATE) > c2.get(Calendar.DATE) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
                                    break;
                                else if(c1.get(Calendar.MONTH) > c2.get(Calendar.MONTH) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
                                    break;
                                else if(c1.get(Calendar.YEAR) > c2.get(Calendar.YEAR))
                                    break;
                                if(transactions.get(i).getTransactionInterval() == 0)
                                    break;
                                c1.add(Calendar.DATE, transactions.get(i).getTransactionInterval());
                            }
                        }
                    }
                    else if(c1.get(Calendar.YEAR) == year && c2.get(Calendar.YEAR) > year) {
                        if(c1.get(Calendar.MONTH) <= month) {
                            while(true) {
                                if(c1.get(Calendar.WEEK_OF_MONTH) == week && c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DATE) <= c2.get(Calendar.DATE) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == month) {
                                    t = t + transactions.get(i).getAmount();
                                }
                                else if(c1.get(Calendar.WEEK_OF_MONTH) == week && c1.get(Calendar.MONTH) < c2.get(Calendar.MONTH) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == month) {
                                    t = t + transactions.get(i).getAmount();
                                }
                                else if(c1.get(Calendar.WEEK_OF_MONTH) == week && c1.get(Calendar.YEAR) < c2.get(Calendar.YEAR) && c1.get(Calendar.YEAR) == year && c1.get(Calendar.MONTH) == month) {
                                    t = t + transactions.get(i).getAmount();
                                }
                                else if(c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DATE) > c2.get(Calendar.DATE) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
                                    break;
                                else if(c1.get(Calendar.MONTH) > c2.get(Calendar.MONTH) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
                                    break;
                                else if(c1.get(Calendar.YEAR) > c2.get(Calendar.YEAR))
                                    break;
                                else if(c1.get(Calendar.YEAR) > year)
                                    break;
                                if(transactions.get(i).getTransactionInterval() == 0)
                                    break;
                                c1.add(Calendar.DATE, transactions.get(i).getTransactionInterval());
                            }
                        }
                    }
                    else if(c1.get(Calendar.YEAR) < year && c2.get(Calendar.YEAR) == year) {
                        if(c2.get(Calendar.MONTH) >= month) {
                            while(true) {
                                if(c1.get(Calendar.WEEK_OF_MONTH) == week && c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DATE) <= c2.get(Calendar.DATE) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == month) {
                                    t = t + transactions.get(i).getAmount();
                                }
                                else if(c1.get(Calendar.WEEK_OF_MONTH) == week && c1.get(Calendar.MONTH) < c2.get(Calendar.MONTH) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == month) {
                                    t = t + transactions.get(i).getAmount();
                                }
                                else if(c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DATE) > c2.get(Calendar.DATE) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
                                    break;
                                else if(c1.get(Calendar.MONTH) > c2.get(Calendar.MONTH) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
                                    break;
                                else if(c1.get(Calendar.YEAR) > c2.get(Calendar.YEAR))
                                    break;
                                if(transactions.get(i).getTransactionInterval() == 0)
                                    break;
                                c1.add(Calendar.DATE, transactions.get(i).getTransactionInterval());
                            }
                        }
                    }
                    else if(c1.get(Calendar.YEAR) < year && c2.get(Calendar.YEAR) > year) {
                        while(true) {
                            if(c1.get(Calendar.WEEK_OF_MONTH) == week && c1.get(Calendar.MONTH) == month && c1.get(Calendar.YEAR) == year) {
                                t = t + transactions.get(i).getAmount();
                            }
                            else if(c1.get(Calendar.YEAR) > year)
                                break;
                            else if(c1.get(Calendar.YEAR) == year && c1.get(Calendar.MONTH) > month)
                                break;
                            if(transactions.get(i).getTransactionInterval() == 0)
                                break;
                            c1.add(Calendar.DATE, transactions.get(i).getTransactionInterval());
                        }
                    }
                }
            }
            else if(transactions.get(i).getType().equals(Type.INDIVIDUALPAYMENT) || transactions.get(i).getType().equals(Type.INDIVIDUALINCOME) || transactions.get(i).getType().equals(Type.PURCHASE)) {
                if(c1.get(Calendar.MONTH) == month && c1.get(Calendar.YEAR) == year && c1.get(Calendar.WEEK_OF_MONTH) == week)
                    t = t + transactions.get(i).getAmount();
                }
            }
        }
        return t;
    }
}