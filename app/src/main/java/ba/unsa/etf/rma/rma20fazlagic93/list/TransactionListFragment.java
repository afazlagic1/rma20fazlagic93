package ba.unsa.etf.rma.rma20fazlagic93.list;

import android.content.Context;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import ba.unsa.etf.rma.rma20fazlagic93.data.Account;
import ba.unsa.etf.rma.rma20fazlagic93.data.TransactionsModel;
import ba.unsa.etf.rma.rma20fazlagic93.util.ConnectivityBroadcastReceiver;
import ba.unsa.etf.rma.rma20fazlagic93.R;
import ba.unsa.etf.rma.rma20fazlagic93.util.TransactionDBOpenHelper;
import ba.unsa.etf.rma.rma20fazlagic93.account.AccountPresenter;
import ba.unsa.etf.rma.rma20fazlagic93.account.IAccountPresenter;
import ba.unsa.etf.rma.rma20fazlagic93.account.IAccountView;
import ba.unsa.etf.rma.rma20fazlagic93.data.Transaction;
import ba.unsa.etf.rma.rma20fazlagic93.data.Type;

public class TransactionListFragment extends Fragment implements ITransactionListView, IAccountView {

    private Transaction clickedTransaction = null;
    private int clickedPosition = -1;
    private int oznakaPocetka;

    private ListView firstListView;
    private TextView globalAmount;
    private TextView limit;
    private TextView textViewDate;
    private Spinner filterBy;
    private Spinner sortBy;
    private ImageButton btnLeft;
    private ImageButton btnRight;
    private Button btnAdd;

    private ArrayList<Type> types;
    private FilterBySpinnerAdapter typesAdapter;

    private Calendar calendar = Calendar.getInstance();
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMMM, YYYY");

    private ITransactionListPresenter transactionListPresenter = new TransactionListPresenter(getActivity(), this);
    private TransactionListAdapter transactionListAdapter;
    private TransactionListCursorAdapter transactionListCursorAdapter;

    private IAccountPresenter accountPresenter = new AccountPresenter(getActivity(), this);

    private OnClick onClick;
    private AdapterView.OnItemClickListener listCursorItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Cursor cursor = (Cursor) parent.getItemAtPosition(position);
            if(cursor != null) {
                Transaction t = null;
                try {
                    Log.d("positionIs", String.valueOf(position));
                    t = getTransactionListPresenter().getTransactionFromDB(cursor.getInt(cursor.getColumnIndex(TransactionDBOpenHelper.TRANSACTION_INTERNAL_ID)), getActivity());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                onClick.onItemClick(t, 0);
            }
        }
    };

    public interface OnClick {
        public void onItemClick(Transaction transaction, double totalMonthAmount);
    }

    private OnAddClick onAddClick;
    public interface OnAddClick {
        public void onAddClickMethod(Transaction transaction, double totalMonthAmount);
    }

    public IAccountPresenter getAccountPresenter() {
        return accountPresenter;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);

        Toast.makeText(getActivity(), "loading... (please wait)", Toast.LENGTH_LONG).show();

        transactionListCursorAdapter = new TransactionListCursorAdapter(getActivity(), R.layout.transaction_element, null, false);
        firstListView = view.findViewById(R.id.firstListView);
        globalAmount = view.findViewById(R.id.globalAmount);
        limit = view.findViewById(R.id.limit);
        textViewDate = view.findViewById(R.id.textViewDate);
        filterBy = view.findViewById(R.id.filterBy);
        sortBy = view.findViewById(R.id.sortBy);
        btnLeft = view.findViewById(R.id.btnLeft);
        btnRight = view.findViewById(R.id.btnRight);
        btnAdd = view.findViewById(R.id.btnAdd);
        if(ConnectivityBroadcastReceiver.connection == true)
            firstListView.setAdapter(transactionListAdapter);
        else {
            firstListView.setAdapter(transactionListCursorAdapter);
            firstListView.setOnItemClickListener(listCursorItemClickListener);
            getTransactionListPresenter().getTransactionsCursor(getActivity());
            Log.d("budgetWas", String.valueOf(TransactionsModel.budgetWas));
            Log.d("monthLimitWas", String.valueOf(TransactionsModel.monthLimitWas));
            globalAmount.setText("Global amount: " + String.valueOf(TransactionsModel.budgetWas));
            limit.setText("Limit: " + String.valueOf(TransactionsModel.monthLimitWas));
        }

        if(!ConnectivityBroadcastReceiver.connection && TransactionsModel.budgetWas == null && TransactionsModel.totalLimitWas == null && TransactionsModel.monthLimitWas == null) {
            Account account = getAccountPresenter().getAccountFromDB(getActivity());
            globalAmount.setText("Global amount: " + String.valueOf(account.getBudget()));
            limit.setText("Limit: " + String.valueOf(account.getMonthLimit()));
        }

        onClick = (OnClick) getActivity();
        onAddClick = (OnAddClick) getActivity();

        oznakaPocetka = -1;

        if(getArguments() != null) {
            if(getArguments().containsKey("deleteThisTransaction"))
                getTransactionListPresenter().removeTransaction((Transaction) getArguments().getSerializable("deleteThisTransaction"), getActivity());
            if(getArguments().containsKey("amountToDelete")) {
                Log.d("budget + amountToDelete:", String.valueOf(getAccountPresenter().getBudget()) + "" + String.valueOf(getArguments().getDouble("amountToDelete")));
            }
            if(getArguments().containsKey("UndoDeleteThisTransaction"))
                getTransactionListPresenter().undoDeleteTransactionDB((Transaction) getArguments().getSerializable("UndoDeleteThisTransaction"), getActivity());
        }

        limit.setText(String.valueOf( "Limit: " + getAccountPresenter().getMonthLimit()));
        globalAmount.setText("Global amount: " + String.valueOf(getAccountPresenter().getBudget()));

        Date dateFilterBy = new Date();
        String s = simpleDateFormat.format(dateFilterBy);
        textViewDate.setText(s);
        calendar.setTime(dateFilterBy);

        initListFilterBy();
        typesAdapter = new FilterBySpinnerAdapter(getActivity(), types);
        filterBy.setAdapter(typesAdapter);

        ArrayAdapter<CharSequence> sortByAdapter = ArrayAdapter.createFromResource(getActivity(), R.array.sort, android.R.layout.simple_spinner_item);
        sortByAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sortBy.setAdapter(sortByAdapter);

        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            btnAdd.setEnabled(false);
            btnAdd.setVisibility(0);
        }

        //listeneri
        btnLeft.setOnClickListener(btnLeftOnClickListener());
        btnRight.setOnClickListener(btnRightOnClickListener());
        filterBy.setOnItemSelectedListener(filterByOnItemSelectedListener());
        sortBy.setOnItemSelectedListener(sortByOnItemSelectedListener());
        if(btnAdd.isEnabled())
            btnAdd.setOnClickListener(addListener());

        return view;
    }

    @Override
    public void onResume() {
        accountPresenter = new AccountPresenter(getActivity(), this);
        super.onResume();
    }

    public ITransactionListPresenter getTransactionListPresenter() {
        return transactionListPresenter;
    }

    private View.OnClickListener addListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Transaction transaction = new Transaction(0, new Date(), 0.0, "Title", Type.INDIVIDUALINCOME, null, 0, null, 0);
                onAddClick.onAddClickMethod(transaction, transactionListPresenter.totalMonthAmount(calendar.get(Calendar.MONTH), calendar.get(Calendar.YEAR)));
            }
        };
    }

    private AdapterView.OnItemClickListener listItemClickListener() {
        return new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Transaction transaction = transactionListAdapter.getTransaction(position);
                if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
                if(clickedTransaction == null) {
                    firstListView.getChildAt(position).setBackgroundColor(R.color.colorGreen);
                    clickedTransaction = transaction;
                    clickedPosition = position;
                    onClick.onItemClick(transaction, getTransactionListPresenter().totalMonthAmount(calendar.get(Calendar.MONTH), calendar.get(Calendar.YEAR)));
                }
                else {
                    if(clickedTransaction.equals(transaction)) {
                        firstListView.getChildAt(position).setBackgroundColor(getResources().getColor(R.color.colorLightYellow));
                        firstListView.setBackgroundColor(getResources().getColor(R.color.colorLightYellow));
                    clickedTransaction = null;
                    clickedPosition = -1;
                    Transaction newTransaction = new Transaction(0, new Date(), 0.0, "Title", Type.INDIVIDUALINCOME, null, 0, null, 0);
                    onClick.onItemClick(newTransaction, getTransactionListPresenter().totalMonthAmount(calendar.get(Calendar.MONTH), calendar.get(Calendar.YEAR)));
                    }
                    else {
                        firstListView.setBackgroundColor(getResources().getColor(R.color.colorLightYellow));
                        for(int i=0; i < firstListView.getChildCount(); i++)
                            if(i != position)
                                firstListView.getChildAt(i).setBackgroundColor(getResources().getColor(R.color.colorLightYellow));
                        firstListView.getChildAt(position).setBackgroundColor(R.color.colorGreen);
                        clickedTransaction = transaction;
                        clickedPosition = position;
                        onClick.onItemClick(transaction, getTransactionListPresenter().totalMonthAmount(calendar.get(Calendar.MONTH), calendar.get(Calendar.YEAR)));
                    }
                }
                else {
                    firstListView.setBackgroundColor(getResources().getColor(R.color.colorLightYellow));
                    clickedTransaction = null;
                    clickedPosition = -1;
                    onClick.onItemClick(transaction, getTransactionListPresenter().totalMonthAmount(calendar.get(Calendar.MONTH), calendar.get(Calendar.YEAR)));
                }
            }
        };
    }

    private AdapterView.OnItemSelectedListener sortByOnItemSelectedListener() {
        return new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Type selectedItem = (Type) filterBy.getSelectedItem();
                int selectedItemPosition = filterBy.getSelectedItemPosition();
                getSelectedType(selectedItemPosition, selectedItem);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        };
    }

    private AdapterView.OnItemSelectedListener filterByOnItemSelectedListener() {
        return new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Type tip = (Type) parent.getItemAtPosition(position);
                if(position >= 0 && position <= 5)
                    getSelectedType(position, tip);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        };
    }

    private View.OnClickListener btnRightOnClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calendar.add(Calendar.MONTH, 1);
                if(!ConnectivityBroadcastReceiver.connection)
                    getTransactionListPresenter().getTransactionsCursor(getActivity());
                textViewDate.setText(simpleDateFormat.format(calendar.getTime()));
                Type selectedItem = (Type) filterBy.getSelectedItem();
                int selectedItemPosition = filterBy.getSelectedItemPosition();
                getSelectedType(selectedItemPosition, selectedItem);
            }
        };
    }

    private View.OnClickListener btnLeftOnClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calendar.add(Calendar.MONTH, -1);
                if(!ConnectivityBroadcastReceiver.connection)
                    getTransactionListPresenter().getTransactionsCursor(getActivity());
                textViewDate.setText(simpleDateFormat.format(calendar.getTime()));
                Type selectedItem = (Type) filterBy.getSelectedItem();
                int selectedItemPosition = filterBy.getSelectedItemPosition();
                getSelectedType(selectedItemPosition, selectedItem);
            }
        };
    }

    private void initListFilterBy() {
        types = new ArrayList<>();
        types.add(null); //oznacava ALL kategoriju
        types.add(Type.INDIVIDUALINCOME);
        types.add(Type.INDIVIDUALPAYMENT);
        types.add(Type.REGULARINCOME);
        types.add(Type.REGULARPAYMENT);
        types.add(Type.PURCHASE);
    }

    private void getSelectedType(int position, Type tip) {
        getTransactionListPresenter().getSelectedType(position, tip);
    }

    @Override
    public void setTransactions(ArrayList<Transaction> transactions) {
            for(int i = 0; i < transactions.size(); i++)
                if(transactions.get(i).getTitle().equals("Title"))
                    transactions.remove(i);
        firstListView.setAdapter(transactionListAdapter);
        firstListView.setOnItemClickListener(listItemClickListener());
        transactionListAdapter.setTransactions(transactions);
    }

    @Override
    public void notifyTransactionListDataChanged() {
        transactionListAdapter.notifyDataSetChanged();
    }

    @Override
    public Calendar getViewDate() {
        return calendar;
    }

    @Override
    public int getSelectedSortByItem() {
        return sortBy.getSelectedItemPosition();
    }

    @Override
    public void setTransactionListAdapter(ArrayList<Transaction> filtriraneTransakcije) {
        transactionListAdapter = new TransactionListAdapter(getActivity(), R.layout.transaction_element, filtriraneTransakcije); //ili context?
        firstListView.setAdapter(transactionListAdapter);
    }

    @Override
    public void setCursor(Cursor cursor) {
        firstListView.setAdapter(transactionListCursorAdapter);
        firstListView.setOnItemClickListener(listCursorItemClickListener);
        transactionListCursorAdapter.changeCursor(cursor);
    }

    @Override
    public void setBudgetView(Double budget) {
        globalAmount.setText("Global amount: " + String.valueOf(budget));
    }

    @Override
    public void setTotalLimitView(Double totalLimit) {
        //
    }

    @Override
    public void setMonthLimitView(Double monthLimit) {
        limit.setText("Limit: " + String.valueOf(monthLimit));
    }

    @Override
    public Context getContext() {
        return getActivity();
    }
}
