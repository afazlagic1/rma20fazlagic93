package ba.unsa.etf.rma.rma20fazlagic93.account;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import ba.unsa.etf.rma.rma20fazlagic93.data.Account;
import ba.unsa.etf.rma.rma20fazlagic93.data.TransactionsModel;
import ba.unsa.etf.rma.rma20fazlagic93.util.ConnectivityBroadcastReceiver;
import ba.unsa.etf.rma.rma20fazlagic93.util.TransactionDBOpenHelper;

public class AccountInteractor implements IAccountInteractor {
    protected Integer id = -1;
    protected Double budget = 0.;
    protected Double totalLimit = 0.;
    protected Double monthLimit = 0.;
    protected String path = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com/account/";
    private onGetAccountInfoDone caller;
    private onPostAccountDone callerPost;

    public AccountInteractor(onGetAccountInfoDone caller) {
        if(id == -1)
            new GetAccountTask().execute("");
        this.caller = caller;
    }

    @Override
    public double getMonthLimit() {
        return monthLimit;
    }

    @Override
    public double getTotalLimit() {
        return totalLimit;
    }

    @Override
    public double getBudget() {
        if(ConnectivityBroadcastReceiver.connection == true)
            new GetAccountTask().execute("");
        else if(caller != null) {

        }
        if(TransactionsModel.budgetWas != null)
            return TransactionsModel.budgetWas;
        return 0.;
    }

    @Override
    public void setMonthLimit(double x, Context context) {
    }

    @Override
    public void setTotalLimit(double x, Context context) {
    }

    @Override
    public void setBudget(double x, final Context context) {
        Runnable accountBudgetPostThread = new Runnable() {
            @Override
            public void run() {
                HttpURLConnection con = null;
                try {
                    String response = null;
                    URL url1 = new URL(path + "c78da961-b241-4175-84c3-34c2b6b88f2f");
                    con = (HttpURLConnection) url1.openConnection();
                    con.setReadTimeout(2000);
                    con.setConnectTimeout(4000);
                    con.setRequestMethod("POST");
                    con.setDoInput(true);
                    con.setDoOutput(true);
                    con.setRequestProperty("Content-Type", "application/json");

                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("budget", budget);

                    DataOutputStream dataOutputStream = new DataOutputStream(con.getOutputStream());
                    dataOutputStream.writeBytes(jsonObject.toString());
                    dataOutputStream.flush();
                    dataOutputStream.close();

                    con.connect();

                    Log.d("uspjesan post za budget", String.valueOf(con.getResponseCode()));

                    if (con.getResponseCode() == 200) {

                    }

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
        this.budget = x;
        TransactionsModel.budgetWas = x;
        Thread thread = new Thread(accountBudgetPostThread);
        thread.start();
    }
    @Override
    public void setMonthLimitAndSetTotalLimit(final onPostAccountDone callerPost, final double monthLimit, final double totalLimit, final Context context) {
        TransactionsModel.totalLimitWas = totalLimit;
        TransactionsModel.monthLimitWas = monthLimit;
        Runnable accountThread = new Runnable() {
            @Override
            public void run() {
                HttpURLConnection con = null;
                try {
                    String response = null;
                    URL url1 = new URL(path + "c78da961-b241-4175-84c3-34c2b6b88f2f");
                    con = (HttpURLConnection) url1.openConnection();
                    con.setReadTimeout(2000);
                    con.setConnectTimeout(4000);
                    con.setRequestMethod("POST");
                    con.setDoInput(true);
                    con.setDoOutput(true);
                    con.setRequestProperty("Content-Type", "application/json");

                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("monthLimit", monthLimit);
                    jsonObject.put("totalLimit", totalLimit);

                    DataOutputStream dataOutputStream = new DataOutputStream(con.getOutputStream());
                    dataOutputStream.writeBytes(jsonObject.toString());
                    dataOutputStream.flush();
                    dataOutputStream.close();

                    con.connect();

                    Log.d("uspjesan post accounta", String.valueOf(con.getResponseCode()));

                    if(con.getResponseCode() == 200) {
                        callerPost.onPostInfoChanged(monthLimit, totalLimit);
                        caller.onGetMonthLimitDone(monthLimit);
                        caller.onGetTotalLimitDone(totalLimit);
                    }

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                    callerPost.onPostInfoChanged(TransactionsModel.monthLimitWas, TransactionsModel.totalLimitWas);
                    caller.onGetMonthLimitDone(TransactionsModel.monthLimitWas);
                    caller.onGetTotalLimitDone(TransactionsModel.totalLimitWas);
                    ContentResolver cr = context.getApplicationContext().getContentResolver();
                    Uri accountURI = Uri.parse("content://rma.provider.account/elements");
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(TransactionDBOpenHelper.ACCOUNT_MONTHLIMIT, TransactionsModel.monthLimitWas);
                    contentValues.put(TransactionDBOpenHelper.ACCOUNT_TOTALLIMIT, TransactionsModel.totalLimitWas);
                    contentValues.put(TransactionDBOpenHelper.ACCOUNT_BUDGET, TransactionsModel.budgetWas);
                    cr.insert(accountURI, contentValues);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
        this.monthLimit = monthLimit;
        this.totalLimit = totalLimit;
        this.callerPost = callerPost;
        Thread thread = new Thread(accountThread);
        thread.start();
    }

    @Override
    public Account getAccountFromDB(Context applicationContext) {
        ContentResolver contentResolver = applicationContext.getApplicationContext().getContentResolver();
        String[] kolone = new String[]{
                TransactionDBOpenHelper.ACCOUNT_ID,
                TransactionDBOpenHelper.ACCOUNT_BUDGET,
                TransactionDBOpenHelper.ACCOUNT_TOTALLIMIT,
                TransactionDBOpenHelper.ACCOUNT_MONTHLIMIT
        };
        Uri adresa = Uri.parse("content://rma.provider.account/elements");
        String where = null;
        String whereArgs[] = null;
        String order = null;
        Cursor cursor = contentResolver.query(adresa,kolone,where,whereArgs,order);
        Account account = new Account();
        if(cursor != null && cursor.getCount() != 0) {
            cursor.moveToLast();
            account.setTotalLimit(cursor.getDouble(cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.ACCOUNT_TOTALLIMIT)));
            account.setMonthLimit(cursor.getDouble(cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.ACCOUNT_MONTHLIMIT)));
            account.setBudget(cursor.getDouble(cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.ACCOUNT_BUDGET)));
        }
        cursor.close();
        return account;
    }

    protected interface onGetAccountInfoDone {
        void onGetBudgetDone(Double info);
        void onGetMonthLimitDone(Double info);
        void onGetTotalLimitDone(Double info);
    }

    protected interface onPostAccountDone {
        void onPostInfoChanged(Double monthLimit, Double totalLimit);
    }

    private class GetAccountTask extends AsyncTask<String, Integer, Void> {
        @Override
        protected Void doInBackground(String... strings) {
            Log.d("doInBackAccount", "");
            String query = null;
            try {
                query = URLEncoder.encode(strings[0], "utf-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            String url1 = path + "c78da961-b241-4175-84c3-34c2b6b88f2f";
            try {
                URL url = new URL(url1);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                String rezultat = convertStreamToString(in);
                JSONObject jo = new JSONObject(rezultat);
                if(jo.isNull("id"))
                    id = 0;
                else
                    id = jo.getInt("id");
                if(jo.isNull("budget"))
                    budget = 0.;
                else
                    budget = jo.getDouble("budget");
                if(jo.isNull("totalLimit"))
                    totalLimit = 0.;
                else
                    totalLimit = jo.getDouble("totalLimit");
                if(jo.isNull("monthLimit"))
                    monthLimit = 0.;
                else
                    monthLimit = jo.getDouble("monthLimit");

                if(urlConnection.getResponseCode() == 200) {
                    TransactionsModel.budgetWas = budget;
                    TransactionsModel.monthLimitWas = monthLimit;
                    TransactionsModel.totalLimitWas = totalLimit;
                }
            }
            catch (MalformedURLException e) {
                e.printStackTrace();
                Log.d("MalformedURLException", "e");
            } catch (IOException e) { //ovdje dodati izmjenu u bazi
                e.printStackTrace();
                Log.d("IOException", "account");
               /* if(caller != null &&  TransactionsModel.budgetWas != null && TransactionsModel.totalLimitWas != null && TransactionsModel.monthLimitWas != null) {
                    caller.onGetBudgetDone(TransactionsModel.budgetWas);
                    caller.onGetMonthLimitDone(TransactionsModel.monthLimitWas);
                    caller.onGetTotalLimitDone(TransactionsModel.totalLimitWas);
                }*/
            } catch (JSONException e) {
                e.printStackTrace();
                Log.d("JSONException", "e");
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(caller != null &&  budget != null && monthLimit != null && totalLimit != null) {
                caller.onGetBudgetDone(budget);
                caller.onGetMonthLimitDone(monthLimit);
                caller.onGetTotalLimitDone(totalLimit);
            }
        }
    }

    private String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new
                InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
        } finally {
            try {
                is.close();
            } catch (IOException e) {
            }
        }
        return sb.toString();
    }
}