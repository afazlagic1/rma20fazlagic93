package ba.unsa.etf.rma.rma20fazlagic93.chart;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import ba.unsa.etf.rma.rma20fazlagic93.data.Transaction;
import ba.unsa.etf.rma.rma20fazlagic93.data.Type;

public class ChartDataInteractor implements IChartDataInteractor {
    private ArrayList<Double> payment;
    private ArrayList<Double> income;
    private ArrayList<Double> amountArray;
    private ArrayList<Transaction> transactionArrayList;
    private String path = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com/account/";

    private Transaction transactionNow;

    private OnDoneTotalWeek callerWeek;

    private OnDoneTotalMonth callerMonth;

    private OnDoneTotalDay callerDay;

    public ChartDataInteractor(OnDoneTotalMonth onDoneTotalMonth) {
        this.callerMonth = onDoneTotalMonth;
    }
    public ChartDataInteractor(OnDoneTotalDay onDoneTotalDay) {
        this.callerDay = onDoneTotalDay;
    }
    public ChartDataInteractor(OnDoneTotalWeek onDoneTotalWeek) {
        this.callerWeek = onDoneTotalWeek;
    }

    @Override
    public void set(String s) {
        if(s.equals("month"))
            new TotalMonthTask().execute("month");
        else if(s.equals("week"))
            new TotalMonthTask().execute("week");
        else if(s.equals("day"))
            new TotalMonthTask().execute("day");
    }

    public interface OnDoneTotalMonth {
        void onDoneTotalMonth(ArrayList<Transaction> payment, ArrayList<Transaction> income, ArrayList<Transaction> amount);
    }
    public interface OnDoneTotalDay {
        void onDoneTotaDay(ArrayList<Transaction> transakcije);
    }
    public interface OnDoneTotalWeek {
        void onDoneTotalWeek(ArrayList<Transaction> transakcije);
    }

    private class TotalMonthTask extends AsyncTask<String, Integer, Void> {
        String query = null;
        @Override
        protected Void doInBackground(String... strings) {
            payment = new ArrayList<>();
            income = new ArrayList<>();
            amountArray = new ArrayList<>();
            transactionArrayList = new ArrayList<>();
            try {
                query = URLEncoder.encode(strings[0], "utf-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            if (!query.equals("filter-sort")) {
                int k = 0;
                while (true) {
                    String url1 = path + "c78da961-b241-4175-84c3-34c2b6b88f2f" + "/transactions" + "?page=" + k;
                    try {
                        URL url = new URL(url1);
                        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                        InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                        String rezultat = convertStreamToString(in);
                        JSONObject jo = new JSONObject(rezultat);
                        JSONArray transactions = jo.getJSONArray("transactions");

                        if (transactions.length() == 0) break;

                        for (int i = 0; i < transactions.length(); i++) {
                            JSONObject transaction = transactions.getJSONObject(i);
                            Integer id = transaction.getInt("id");
                            String date;
                            Date date1;
                            if (!transaction.isNull("date")) {
                                date = transaction.getString("date");
                                SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                                date1 = inputFormat.parse(date);
                            } else {
                                continue;
                            }
                            String title = transaction.getString("title");
                            if(!(title.length() > 3 && title.length() < 15))
                                continue;
                            Double amount = transaction.getDouble("amount");
                            Integer transactionTypeId;
                            if (!transaction.isNull("TransactionTypeId"))
                                transactionTypeId = transaction.getInt("TransactionTypeId");
                            else
                                continue;
                            String itemDescription;
                            if (transaction.isNull("itemDescription")) {
                                if(transactionTypeId != 2 && transactionTypeId != 4)
                                    itemDescription = "";
                                else
                                    itemDescription = null;
                            } else {
                                itemDescription = transaction.getString("itemDescription");
                                if(transactionTypeId == 2 && itemDescription != "")
                                    continue;
                                else if(transactionTypeId == 4 && itemDescription != "")
                                    continue;
                            }
                            Integer transactionInterval;
                            if (transaction.isNull("transactionInterval")) {
                                transactionInterval = 0;
                                if(transactionTypeId == 1)
                                    continue;
                                else if(transactionTypeId == 2)
                                    continue;
                            } else {
                                transactionInterval = transaction.getInt("transactionInterval");
                                if(transactionTypeId != 1 && transactionTypeId != 2 && transactionInterval != 0)
                                    continue;
                            }
                            String endDate;
                            Date date2 = null;
                            if (!transaction.isNull("endDate")) {
                                endDate = transaction.getString("endDate");
                                SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                                date2 = inputFormat.parse(endDate);
                                if(transactionTypeId != 1 && transactionTypeId != 2)
                                    continue;
                            } else {
                                date2 = null;
                                if(transactionTypeId == 1)
                                    continue;
                                if(transactionTypeId == 2)
                                    continue;
                            }
                            String createdAt = transaction.getString("createdAt");
                            String updatedAt = transaction.getString("updatedAt");
                            Integer accountId = transaction.getInt("AccountId");
                            transactionNow = new Transaction(id, date1, amount, title, null, itemDescription, transactionInterval, date2, accountId);
                            if(transactionNow.getTitle().length() > 3 && transactionNow.getTitle().length() < 15)
                                transactionArrayList.add(transactionNow);
                            addType(transactionTypeId);
                        }
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                        Log.d("MalformedURLException", "e");
                        break;
                    } catch (IOException e) {
                        e.printStackTrace();
                        Log.d("IOException", "e");
                        break;
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d("JSONException", "e");
                        break;
                    } catch (ParseException e) {
                        Log.i("1", "ne valja date formatter");
                        e.printStackTrace();
                        break;
                    }
                    k++;
                }
            }
          /*  double totalPayment = 0;
            double totalIncome = 0;
            double totalAmount = 0;
            String query = null;
            try {
                query = URLEncoder.encode(strings[0], "utf-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            if (query.equals("")) {
                Calendar calendar = Calendar.getInstance();
                int k = 0;
                String month, year;
                month = "1";
                year = String.valueOf(calendar.get(Calendar.YEAR));
                while (true) {
                    String url1;
                    url1 = path + "c78da961-b241-4175-84c3-34c2b6b88f2f" + "/transactions/filter" + "?page=" + k + "&month=" + month + "&year=" + year;
                    Log.d("grafPut", url1);
                    try {
                        URL url = new URL(url1);
                        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                        InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                        String rezultat = convertStreamToString(in);
                        JSONObject jo = new JSONObject(rezultat);
                        JSONArray transactions = jo.getJSONArray("transactions");
                        if (transactions.length() == 0 && Integer.valueOf(month) < 12) {
                            k = 0;
                            int x = Integer.parseInt(month) + 1;
                            month = String.valueOf(x);
                            payment.add(totalPayment);
                            income.add(totalIncome);
                            amountArray.add(totalAmount);
                            totalAmount = 0;
                            totalIncome = 0;
                            totalPayment = 0;
                            continue;
                        }
                        else if(transactions.length() == 0) {
                            payment.add(totalPayment);
                            income.add(totalIncome);
                            amountArray.add(totalAmount);
                            break;
                        }
                        for (int i = 0; i < transactions.length(); i++) {
                            JSONObject transaction = transactions.getJSONObject(i);
                            Integer id = transaction.getInt("id");
                            String date;
                            Date date1;
                            if (!transaction.isNull("date")) {
                                date = transaction.getString("date");
                                SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                                date1 = inputFormat.parse(date);
                            } else {
                                date1 = new Date();
                            }
                            String title = transaction.getString("title");
                            Double amount = transaction.getDouble("amount");
                            String itemDescription;
                            if (transaction.isNull("itemDescription")) {
                                itemDescription = null;
                            } else {
                                itemDescription = transaction.getString("itemDescription");
                            }
                            Integer transactionInterval;
                            if (transaction.isNull("transactionInterval")) {
                                transactionInterval = 0;
                            } else {
                                transactionInterval = transaction.getInt("transactionInterval");
                            }
                            String endDate;
                            Date date2 = null;
                            if (!transaction.isNull("endDate")) {
                                endDate = transaction.getString("endDate");
                                SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                                date2 = inputFormat.parse(endDate);
                            } else {
                                date2 = new Date();
                            }
                            String createdAt = transaction.getString("createdAt");
                            String updatedAt = transaction.getString("updatedAt");
                            Integer accountId = transaction.getInt("AccountId");
                            Integer transactionTypeId;
                            if (!transaction.isNull("TransactionTypeId"))
                                transactionTypeId = transaction.getInt("TransactionTypeId");
                            else
                                continue;
                            transactionNow = new Transaction(id, date1, amount, title, null, itemDescription, transactionInterval, date2, accountId);
                            if(!(transactionNow.getTitle().length() >= 3 && transactionNow.getTitle().length() <= 15))
                                continue;
                            addType(transactionTypeId);
                            if(transactionNow.getType().equals(Type.PURCHASE) || transactionNow.getType().equals(Type.INDIVIDUALPAYMENT))
                                totalPayment = totalPayment + transactionNow.getAmount();
                            else if(transactionNow.getType().equals(Type.INDIVIDUALINCOME))
                                totalIncome = totalIncome + transactionNow.getAmount();
                            totalAmount = totalAmount + totalPayment - totalIncome;
                        }
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                        Log.d("MalformedURLException", "e");
                        break;
                    } catch (IOException e) {
                        e.printStackTrace();
                        Log.d("IOException", "e");
                        break;
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d("JSONException", "e");
                        break;
                    } catch (ParseException e) {
                        Log.i("1", "ne valja date formatter");
                        e.printStackTrace();
                        break;
                    }
                    k++;
                }
                addRegularTransactions(Integer.parseInt(month), Integer.valueOf(year), null);
            }
*/
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(query.equals("month"))
                callerMonth.onDoneTotalMonth(transactionArrayList, transactionArrayList, transactionArrayList);
            else if(query.equals("week"))
                callerWeek.onDoneTotalWeek(transactionArrayList);
            else if(query.equals("day"))
                callerDay.onDoneTotaDay(transactionArrayList);
        }
    }

    private void addRegularTransactions(Integer month, Integer year, Integer typeID) {
        int k = 0;
        int m = 1;
        if(typeID != null)
            m = typeID;
        while (true) {
            String url1 = path + "c78da961-b241-4175-84c3-34c2b6b88f2f" + "/transactions" + "/filter" + "?page=" + k + "&typeId=" + m;
            Log.d("putRegularne", url1);
            try {
                URL url = new URL(url1);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                String rezultat = convertStreamToString(in);
                JSONObject jo = new JSONObject(rezultat);
                JSONArray transactions = jo.getJSONArray("transactions");

                if(typeID == null) {
                    if (transactions.length() == 0 && m == 2) break;
                    else if (transactions.length() == 0 && m == 1) {
                        m = 2;
                        k = 0;
                        continue;
                    }
                }
                else
                if(transactions.length() == 0) break;


                for (int i = 0; i < transactions.length(); i++) {
                    JSONObject transaction = transactions.getJSONObject(i);
                    Integer transactionTypeId;
                    if (!transaction.isNull("transactionTypeId"))
                        transactionTypeId = transaction.getInt("TransactionTypeId");
                    else
                        continue;
                    Integer id = transaction.getInt("id");
                    String date;
                    Date date1;
                    if (!transaction.isNull("date")) {
                        date = transaction.getString("date");
                        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                        date1 = inputFormat.parse(date);
                    } else {
                        date1 = new Date();
                    }
                    String endDate;
                    Date date2;
                    if (!transaction.isNull("endDate")) {
                        endDate = transaction.getString("endDate");
                        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                        date2 = inputFormat.parse(endDate);
                    } else {
                        date2 = new Date();
                    }
                    Calendar c1 = Calendar.getInstance();
                    c1.setTime(date1);
                    Calendar c2 = Calendar.getInstance();
                    c2.setTime(date2);
                    Calendar helpCalendar = Calendar.getInstance();
                    helpCalendar.set(year, month, 1);
                    Calendar showedCalendar = Calendar.getInstance();
                    showedCalendar.set(year, month, helpCalendar.getActualMaximum(Calendar.DATE));
                    Double amount = transaction.getDouble("amount");

                    Integer transactionInterval;
                    if (transaction.isNull("transactionInterval")) {
                        transactionInterval = 0;
                    } else {
                        transactionInterval = transaction.getInt("transactionInterval");
                    }

                    while(c1.before(c2) && c1.before(showedCalendar)) {
                        if(c1.get(Calendar.MONTH) == month - 1)
                            if(transactionTypeId == 1) {
                                payment.add(month - 1, payment.get(month - 1) - amount);
                                payment.remove(month);
                                amountArray.add(month - 1, amountArray.get(month - 1) - amount);
                                amountArray.remove(month);
                            }
                            else if(transactionTypeId == 2) {
                                income.add(month - 1, income.get(month - 1) + amount);
                                income.remove(month);
                                amountArray.add(month - 1, amountArray.get(month - 1) + amount);
                                amountArray.remove(month);
                            }
                        if(transactionInterval == 0)
                            break;
                        c1.add(Calendar.DATE, transactionInterval);
                    }
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
                Log.d("MalformedURLException", "e");
                break;
            } catch (IOException e) {
                e.printStackTrace();
                Log.d("IOException", "e");
                break;
            } catch (JSONException e) {
                e.printStackTrace();
                Log.d("JSONException", "e");
                break;
            } catch (ParseException e) {
                Log.i("1", "ne valja date formatter");
                e.printStackTrace();
                break;
            }
            k++;
        }
    }

    private void addType(Integer... params) {
        Integer typeID = params[0];
        String url1 = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com/transactionTypes";
        try {
            URL url = new URL(url1);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String rezultat = convertStreamToString(in);
            JSONObject jo = new JSONObject(rezultat);
            Integer count = jo.getInt("count");
            JSONArray types = jo.getJSONArray("rows");
            for (int i = 0; i < count; i++) {
                JSONObject type = types.getJSONObject(i);
                if (typeID.equals(type.getInt("id"))) {
                    if (type.getString("name").equals("Regular payment")) {
                        transactionNow.setType(Type.REGULARPAYMENT);
                        transactionNow.setAmount(transactionNow.getAmount() * (-1));
                    } else if (type.getString("name").equals("Individual payment")) {
                        transactionNow.setType(Type.INDIVIDUALPAYMENT);
                        transactionNow.setAmount(transactionNow.getAmount() * (-1));
                    } else if (type.getString("name").equals("Individual income"))
                        transactionNow.setType(Type.INDIVIDUALINCOME);
                    else if (type.getString("name").equals("Regular income"))
                        transactionNow.setType(Type.REGULARINCOME);
                    else if (type.getString("name").equals("Purchase")) {
                        transactionNow.setType(Type.PURCHASE);
                        transactionNow.setAmount(transactionNow.getAmount() * (-1));
                    }
                }
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
        public String convertStreamToString(InputStream is) {
            BufferedReader reader = new BufferedReader(new
                    InputStreamReader(is));
            StringBuilder sb = new StringBuilder();
            String line = null;
            try {
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
            } catch (IOException e) {
            } finally {
                try {
                    is.close();
                } catch (IOException e) {
                }
            }
            return sb.toString();
    }
}
